import aiohttp
import asyncio
from datetime import datetime, timedelta
import json


# Função assíncrona para fazer requisições de uma página específica
async def fetch_page(session, page_number, data_inicial, data_final):
    url = "https://pncp.gov.br/api/consulta/v1/contratos"
    params = {
        "dataInicial": data_inicial,
        "dataFinal": data_final,
        "cnpjOrgao": "33892175000100",
        "pagina": page_number,
        "tamanhoPagina": 10,
    }

    max_retries = 3
    for attempt in range(max_retries):
        try:
            async with session.get(url, params=params) as response:
                if response.status == 200:
                    return await response.json()
                else:
                    print(f"Erro na página {page_number}: {response.status}")
                    if attempt < max_retries - 1:
                        print(
                            f"Tentando novamente... "
                            f"(Tentativa {attempt + 2} de {max_retries})"
                        )
                    await asyncio.sleep(1)  # Wait for 1 second before retrying
        except aiohttp.ClientError as e:
            print(f"Erro de conexão na página {page_number}: {str(e)}")
            if attempt < max_retries - 1:
                print(
                    f"Tentando novamente... (Tentativa {attempt + 2} de {max_retries})"
                )
            await asyncio.sleep(1)  # Wait for 1 second before retrying

    print(f"Falha ao obter a página {page_number} após {max_retries} tentativas.")
    return None


async def main():
    data_inicial = "20220102"

    data_atual = datetime.today().strftime("%Y%m%d")

    all_data = []

    async with aiohttp.ClientSession() as session:
        while True:
            ano_inicial = int(data_inicial[:4])
            data_final = f"{ano_inicial + 1}0101"

            if data_final > data_atual:
                data_final = data_atual

            print(f"Processando intervalo de {data_inicial} a {data_final}")

            url = "https://pncp.gov.br/api/consulta/v1/contratos"
            params = {
                "dataInicial": data_inicial,
                "dataFinal": data_final,
                "cnpjOrgao": "33892175000100",
                "pagina": 1,
                "tamanhoPagina": 10,
            }

            async with session.get(url, params=params) as response:
                if response.status == 200:
                    data = await response.json()
                    total_paginas = data["totalPaginas"]
                    print(
                        f"Intervalo: {data_inicial} a {data_final} - "
                        f"Total de páginas: {total_paginas}"
                    )

                    tasks = [
                        fetch_page(session, page, data_inicial, data_final)
                        for page in range(1, total_paginas + 1)
                    ]
                    results = await asyncio.gather(*tasks)

                    for result in results:
                        if result:
                            all_data.extend(result["data"])

                    print(
                        f"Intervalo: {data_inicial} a {data_final} - "
                        f"Total de registros obtidos: {len(all_data)}"
                    )
                else:
                    print(
                        f"Erro na requisição inicial para o intervalo "
                        f"{data_inicial} a {data_final}: {response.status}"
                    )
                    break

            nova_data_inicial = datetime.strptime(data_final, "%Y%m%d") + timedelta(
                days=1
            )
            data_inicial = nova_data_inicial.strftime("%Y%m%d")

            if data_inicial > data_atual:
                break

    print(f"Total de registros coletados: {len(all_data)}")

    salvar_em_json(all_data, "data-mapping/notebooks/ipea_pncp/dados_coletados.json")


def salvar_em_json(data, nome_arquivo):
    with open(nome_arquivo, "w", encoding="utf-8") as f:
        json.dump(data, f, ensure_ascii=False, indent=4)
    print(f"Dados salvos com sucesso no arquivo: {nome_arquivo}")


asyncio.run(main())
