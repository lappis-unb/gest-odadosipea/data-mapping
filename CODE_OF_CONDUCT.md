# Código de Conduta

## Nossos Padrões

Exemplos de comportamentos inaceitáveis incluem:

- Uso de linguagem ou imagens sexualizadas, ou atenção ou avanços indesejados de natureza sexual.
- Comentários insultuosos ou depreciativos, e ataques pessoais ou políticos.
- Assédio público ou privado.
- Publicação de informações privadas de outras pessoas.
- Outra conduta que possa ser razoavelmente considerada inapropriada em um ambiente profissional.

## Escopo

Este Código de Conduta se aplica dentro de todos os espaços da comunidade e também se aplica quando um indivíduo está representando oficialmente a comunidade em espaços públicos. 

## Aplicação

Comportamentos abusivos, de assédio ou inaceitáveis podem ser reportados aos líderes da comunidade responsáveis pela aplicação. Todas as reclamações serão revisadas e investigadas de maneira pontual e justa.

Todos os líderes da comunidade são obrigados a respeitar a privacidade e a segurança de quem reportar qualquer incidente.

## Atribuição

Este Código de Conduta é adaptado do [Contributor Covenant](https://www.contributor-covenant.org), versão 2.1, disponível em https://www.contributor-covenant.org/version/2/1/code_of_conduct.html.

Para respostas a perguntas frequentes sobre este código de conduta, veja https://www.contributor-covenant.org/faq.
