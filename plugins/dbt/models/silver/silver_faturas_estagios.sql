{{
  config(
    materialized='table'
  )
}}


WITH contratos_unicos AS (
    -- Seleciona apenas os registros onde o fornecedor_cnpj_cpf_idgener é único
    SELECT * 
    FROM public_raw.contratos
    WHERE fornecedor_cnpj_cpf_idgener IN     
        (SELECT fornecedor_cnpj_cpf_idgener 
         FROM public_raw.contratos
         GROUP BY fornecedor_cnpj_cpf_idgener 
         HAVING COUNT(fornecedor_cnpj_cpf_idgener) = 1)
),

-- Primeiro join com base em fornecedor_cnpj_cpf_idgener
primeiro_join AS (
    SELECT 
        e.*,  -- Todas as colunas da tabela `estagios_atualizada`
        c.id AS contrato_id    
    FROM public_raw.estagios e
    JOIN contratos_unicos c
    ON e.ne_ccor_favorecido = c.fornecedor_cnpj_cpf_idgener
),

processos_unicos AS (
    -- Seleciona apenas os registros onde o processo é único
    SELECT * 
    FROM public_raw.contratos
    WHERE processo IN     
        (SELECT processo 
         FROM public_raw.contratos
         GROUP BY processo 
         HAVING COUNT(processo) = 1)    
    AND id NOT IN (SELECT contrato_id FROM primeiro_join)
),

-- Segundo join com base em processo, com registros restantes
segundo_join AS (
    SELECT 
        e.*,  -- Todas as colunas da tabela `estagios_atualizada`
        c.id AS contrato_id  
    FROM public_raw.estagios e
    JOIN processos_unicos c
    ON e.ne_num_processo = c.processo
),

numeros_unicos AS (
    -- Seleciona apenas os registros onde o numero é único
    SELECT * 
    FROM public_raw.contratos
    WHERE LENGTH(numero) = 12
    AND id NOT IN (SELECT contrato_id FROM segundo_join)
),

-- -- Terceiro join com base em numero, com registros restantes
terceiro_join AS (
    SELECT 
        e.*,  -- Todas as colunas da tabela `estagios_atualizada`
        c.id AS contrato_id   
    FROM public_raw.estagios e
    JOIN numeros_unicos c
    ON RIGHT(e.ne_ccor, 12) = c.numero
),

-- Combina os resultados dos 3 joins
resultado AS(
  SELECT * FROM primeiro_join
  UNION DISTINCT
  SELECT * FROM segundo_join
  UNION DISTINCT
  SELECT * FROM terceiro_join
),

contratos_empenhos AS (
    SELECT DISTINCT contrato_id, numero_empenho
    FROM public_raw.faturas
    WHERE contrato_id NOT IN (SELECT DISTINCT contrato_id FROM resultado)
),

quarto_join AS (
    SELECT 
        e.*,  -- Todas as colunas da tabela `estagios_atualizada`
        c.contrato_id 
    FROM public_raw.estagios e
    JOIN contratos_empenhos c
    ON RIGHT(e.ne_ccor, 12) = c.numero_empenho
),

resultado_2 AS (
    SELECT * FROM resultado
    UNION DISTINCT
    SELECT * FROM quarto_join
),


-- seleciona os contratos restantes
contratos_restantes AS(
  select * FROM public_raw.contratos where id NOT IN(select contrato_id FROM resultado_2)
),


-- faz o join a partir da informação complementar
quinto_join AS (
    SELECT 
        ec.*,  -- Todas as colunas da tabela `segundo_join`
        c.id AS contrato_id
    FROM public_raw.estagios ec
    JOIN contratos_restantes c
    ON ec.ne_informacao_complementar like CONCAT('%',c.contratante_orgao_unidade_gestora_codigo, TO_CHAR(c.codigo_modalidade, 'FM00'), REGEXP_REPLACE(c.numero, '[\./-]', '', 'g'),'%') AND c.codigo_modalidade = '5' OR
    ec.ne_informacao_complementar like CONCAT('%',c.contratante_orgao_unidade_gestora_codigo, TO_CHAR(c.codigo_modalidade, 'FM00'), REGEXP_REPLACE(c.licitacao_numero, '[\./-]', '', 'g'),'%') AND c.codigo_modalidade = '7'
),

-- une os ids dos contratos pra ver quais contratos tiveram alguma correspondencia
union_estagios_contratoID AS(
  SELECT * from resultado_2
  UNION DISTINCT
  SELECT * from quinto_join
),




empenhos_faturas as (
SELECT DISTINCT
    contrato_id,
    valor,
    'fatura' AS estagio_despesa, -- aqui está sendo definido o nome da coluna (estagio_despesa) em que será preenchido com o valor "fatura"
    numero_empenho AS ne_ccor,
    TO_CHAR(emissao, 'MON/YYYY') as mes
  FROM public_raw.faturas
),

estagios_desp as(
SELECT 
  contrato_id,
  despesas_empenhadas_controle_empenho_movim_liquido_moeda_origem AS valor,
  'empenhado' AS estagio_despesa,
  RIGHT(estagios.ne_ccor, 12),
  mes_lancamento AS mes
FROM union_estagios_contratoID AS estagios 
WHERE 
  RIGHT(estagios.ne_ccor, 12) IN (SELECT ne_ccor FROM empenhos_faturas) 
  AND despesas_empenhadas_controle_empenho_movim_liquido_moeda_origem IS NOT NULL
  AND despesas_empenhadas_controle_empenho_movim_liquido_moeda_origem <> 0

UNION ALL

SELECT 
  contrato_id,
  despesas_liquidadas_controle_empenho_movim_liquido_moeda_origem AS valor,
  'liquidado' AS estagio_despesa,
  RIGHT(estagios.ne_ccor, 12), -- retirar essa parte do right, utilizar uma coluna que já tenha esse valor correto
  mes_lancamento AS mes
FROM union_estagios_contratoID AS estagios
WHERE 
  RIGHT(estagios.ne_ccor, 12) IN (SELECT ne_ccor FROM empenhos_faturas) 
  AND despesas_liquidadas_controle_empenho_movim_liquido_moeda_origem IS NOT NULL
  AND despesas_liquidadas_controle_empenho_movim_liquido_moeda_origem <> 0

UNION ALL

SELECT 
  contrato_id,
  despesas_pagas_controle_empenho_movim_liquido_moeda_origem AS valor,
  'pago' AS estagio_despesa,
  RIGHT(estagios.ne_ccor, 12),
  mes_lancamento AS mes
FROM union_estagios_contratoID AS estagios
WHERE 
  RIGHT(estagios.ne_ccor, 12) IN (SELECT ne_ccor FROM empenhos_faturas) 
  AND despesas_pagas_controle_empenho_movim_liquido_moeda_origem IS NOT NULL
  AND despesas_pagas_controle_empenho_movim_liquido_moeda_origem <> 0
),
  
union_faturas_estagios as (
select * from empenhos_faturas
UNION ALL
select * from estagios_desp
)

select * from union_faturas_estagios 