import psycopg2
from psycopg2.extras import execute_values
from collections.abc import MutableMapping
import json
import pandas as pd
import io


class PostgresDB:
    def __init__(self, conn_str):
        self.conn_str = conn_str

    def flatten(self, d, parent_key="", sep="_"):
        """Função para achatar dicionários aninhados."""
        items = []
        for k, v in d.items():
            new_key = f"{parent_key}{sep}{k}" if parent_key else k
            if isinstance(v, MutableMapping):
                items.extend(self.flatten(v, new_key, sep=sep).items())
            elif isinstance(v, list):
                # Se for uma lista, converte para JSON string
                items.append((new_key, json.dumps(v)))
            else:
                items.append((new_key, v))
        return dict(items)

    def create_table_if_not_exists(
        self, sample_data, table_name, primary_key=None, schema="raw"
    ):
        """Cria a tabela dinamicamente com base nos dados."""
        conn = psycopg2.connect(self.conn_str)
        cursor = conn.cursor()

        cursor.execute(f"CREATE SCHEMA IF NOT EXISTS {schema};")
        conn.commit()

        flattened_sample = self.flatten(sample_data)

        column_definitions = [f"{column} TEXT" for column in flattened_sample.keys()]

        # Permite que `primary_key` possa ser string ou lista de strings
        if primary_key:
            pk_list = [primary_key] if isinstance(primary_key, str) else primary_key
            if isinstance(pk_list, list) and all(
                key in flattened_sample for key in pk_list
            ):
                primary_keys_str = ", ".join(pk_list)
                column_definitions.append(f"PRIMARY KEY ({primary_keys_str})")
        else:
            print(
                f"Aviso: Nenhuma chave primária definida para a tabela {table_name}."
            )  # Apenas log de aviso

        create_table_query = f"""
        CREATE TABLE IF NOT EXISTS {schema}.{table_name} (
            {', '.join(column_definitions)}
        );
        """

        print(f"SQL de criação da tabela: {create_table_query}")  # Log para depuração

        try:
            cursor.execute(create_table_query)
            conn.commit()
            print(f"Tabela {schema}.{table_name} criada ou já existente.")
        except Exception as e:
            print(f"Erro ao criar a tabela {schema}.{table_name}: {e}")
        finally:
            cursor.close()
            conn.close()

    def insert_data(
        self, data, table_name, conflict_fields=None, primary_key=None, schema="raw"
    ):
        """
        Insere dados no banco de dados, garantindo que a tabela seja criada.
        Se ocorrer um conflito, aplica a política especificada em `conflict_fields`.

        Args:
            data (list): Lista de dicionários contendo os dados a serem inseridos.
            table_name (str): Nome da tabela de destino.
            conflict_fields (list): Lista de campos usados para resolver conflitos.
            primary_key (str): Nome da chave primária da tabela (opcional).
            schema (str): Nome do schema do banco (padrão: "raw").
        """
        if not data:
            print(f"Nenhum dado para inserir na tabela {table_name}.")
            return

        # Garante que a tabela seja criada com a chave primária correta
        self.create_table_if_not_exists(data[0], table_name, primary_key=primary_key)

        flattened_data = [self.flatten(item) for item in data]
        columns = flattened_data[0].keys()
        values = [tuple(item[col] for col in columns) for item in flattened_data]

        sql = f"""
        INSERT INTO {schema}.{table_name} ({', '.join(columns)})
        VALUES %s
        """

        # Adiciona a cláusula ON CONFLICT, se necessário
        if conflict_fields:
            conflict_fields_str = ", ".join(conflict_fields)
            update_str = ", ".join([f"{col} = EXCLUDED.{col}" for col in columns])
            sql += f"""
            ON CONFLICT ({conflict_fields_str})
            DO UPDATE SET {update_str}
            """

        conn = psycopg2.connect(self.conn_str)
        cursor = conn.cursor()

        try:
            execute_values(cursor, sql, values)
            conn.commit()
            print(
                f"Inseridos {len(values)} registros na tabela {table_name} com sucesso."
            )
        except Exception as e:
            print(f"Erro ao inserir dados na tabela {table_name}: {e}")
        finally:
            cursor.close()
            conn.close()

    def get_contratos_ids(self):
        """Extrai todos os IDs de contratos da tabela contratos."""
        query = "SELECT id FROM raw.contratos"

        conn = psycopg2.connect(self.conn_str)
        cursor = conn.cursor()
        cursor.execute(query)

        contratos_ids = [row[0] for row in cursor.fetchall()]

        cursor.close()
        conn.close()

        return contratos_ids

    def drop_table_if_exists(self, table_name, schema="raw"):
        """Remove a tabela se ela existir."""
        conn = psycopg2.connect(self.conn_str)
        cursor = conn.cursor()
        drop_table_query = f"DROP TABLE IF EXISTS {schema}.{table_name};"
        try:
            cursor.execute(drop_table_query)
            conn.commit()
            print(f"Tabela {schema}.{table_name} removida com sucesso.")
        except Exception as e:
            print(f"Erro ao remover a tabela {schema}.{table_name}: {e}")
        finally:
            cursor.close()
            conn.close()

    def insert_csv_data(self, csv_data, table_name, schema="raw"):
        """
        Insere dados de um CSV no banco de dados, garantindo que a tabela seja criada.
        Se a tabela existir, ela será removida antes da inserção dos novos dados.

        Args:
            csv_data (str): Dados do CSV como string.
            table_name (str): Nome da tabela de destino.
            schema (str): Nome do schema do banco (padrão: "raw").
        """
        # Converte o CSV para DataFrame
        df = pd.read_csv(io.StringIO(csv_data))

        # Converte o DataFrame para lista de dicionários
        data = df.to_dict(orient="records")

        # Remove a tabela existente
        self.drop_table_if_exists(table_name, schema)

        # Insere os novos dados
        self.insert_data(data, table_name, primary_key=None, schema=schema)
