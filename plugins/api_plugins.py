import requests
import time


class ContratosAPI:
    BASE_URL = "https://contratos.comprasnet.gov.br/api"

    def __init__(self):
        self.headers = {
            "accept": "application/json",
        }

    def _make_request(self, endpoint):
        """Função genérica para fazer uma requisição GET à API."""
        url = f"{self.BASE_URL}{endpoint}"
        max_retries = 3
        for attempt in range(max_retries):
            try:
                print(f"Tentativa {attempt + 1}: Fazendo requisição para: {url}")
                response = requests.get(url, headers=self.headers, timeout=20)
                response.raise_for_status()
                return response.json()
            except requests.exceptions.RequestException as e:
                print(f"Erro ao fazer a requisição: {e}")
                time.sleep(5)  # Aguarda 5 segundos antes de tentar novamente
                if attempt == max_retries - 1:
                    raise Exception(f"Falha após {max_retries} tentativas: {e}")

    def get_contratos_by_ug(self, ug_code):
        """Obter todos os contratos ativos de uma UG específica."""
        endpoint = f"/contrato/ug/{ug_code}"
        return self._make_request(endpoint)

    def get_faturas_by_contrato_id(self, contrato_id):
        """Obter todas as faturas de um contrato específico."""
        endpoint = f"/contrato/{contrato_id}/faturas"
        return self._make_request(endpoint)

    def get_empenhos_by_contrato_id(self, contrato_id):
        """Obter todos os empenhos de um contrato específico."""
        endpoint = f"/contrato/{contrato_id}/empenhos"
        return self._make_request(endpoint)

    def get_cronograma_by_contrato_id(self, contrato_id):
        """Obter todas os cronogramas de um contrato específico."""
        endpoint = f"/contrato/{contrato_id}/cronograma"
        return self._make_request(endpoint)


class EstruturaAPI:
    BASE_URL = "https://estruturaorganizacional.dados.gov.br/doc"

    def __init__(self):
        self.headers = {
            "accept": "application/json",
        }

    def _make_request(self, endpoint, params=None):
        """Função genérica para fazer uma requisição GET à API."""
        url = f"{self.BASE_URL}{endpoint}"
        response = requests.get(url, headers=self.headers, params=params)
        if response.status_code == 200:
            return response.json()
        else:
            raise Exception(
                f"Failed to get data. Status code: {response.status_code}, URL: {url}"
            )

    def get_estrutura_organizacional_resumida(
        self, codigoPoder=None, codigoEsfera=None, codigoUnidade=None
    ):
        """Consultar Estrutura Organizacional Resumida."""
        endpoint = "/estrutura-organizacional/resumida"
        params = {}
        if codigoPoder:
            params["codigoPoder"] = codigoPoder
        if codigoEsfera:
            params["codigoEsfera"] = codigoEsfera
        if codigoUnidade:
            params["codigoUnidade"] = codigoUnidade

        return self._make_request(endpoint, params)
