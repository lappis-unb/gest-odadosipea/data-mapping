## Descrição do Problema ou Sugestão

Descreva aqui o problema encontrado ou a funcionalidade sugerida. Seja claro e objetivo.

## Passos para Reproduzir (para bugs)

1. [Descreva o primeiro passo]
2. [Descreva o segundo passo]
3. [E assim por diante]

## Comportamento Esperado

Descreva o comportamento esperado.

## Informações Adicionais

Inclua capturas de tela, logs ou qualquer outra informação relevante.
