# Configuração do Ambiente de Extração de Dados

Este repositório contém os arquivos necessários para configurar um ambiente para extração de dados usando contêineres Docker.

## Componentes

A configuração inclui:

- Apache Airflow para gerenciamento de fluxo de trabalho.
- JupyterLab para análise interativa de dados.
- Superset para geração de dashboards.

## Primeiros Passos

### Pré-requisitos

Para usar este ambiente, você precisa ter o Docker e o Docker Compose instalados no seu sistema. Se você não tem o Docker instalado, siga o [guia de instalação oficial do Docker](https://docs.docker.com/get-docker/).


### Configurando o ambiente

Para configurar e iniciar o ambiente:

1. Clone este repositório para a sua máquina local.
2. Navegue até o diretório que contém o arquivo `docker-compose.yml`.
3. Execute o seguinte comando para iniciar todos os serviços em modo desanexado:

    ```sh
    docker compose up --build -d
    ```

4. Após iniciar os serviços, os contêineres estarão em execução com as seguintes URLs de acesso:

- **Apache Airflow**: [http://localhost:8080/](http://localhost:8080/)
- **JupyterLab**: [http://localhost:8888/](http://localhost:8888/)
- **Superset**: [http://localhost:8088/](http://localhost:8088/)

### Parando o ambiente

Para parar o ambiente e remover os contêineres, execute:

```sh
docker compose down
```

## Configurando o DBeaver

O DBeaver é uma ferramenta de gerenciamento de banco de dados que oferece uma interface intuitiva para administrar suas conexões e consultas. Siga os passos abaixo para configurá-lo:

1. **Instalação Local do DBeaver:**
   - Baixe e instale o DBeaver: Acesse o site oficial do DBeaver e siga as instruções para o seu sistema operacional.
   - Abra o DBeaver.

2. **Conexão com o Banco de Dados de Teste:**
    - Host: "postgres"
    - Port: "5432"
    - Database: "data_warehouse"
    - Username: "airflow"
    - Password: "airflow"

2. **Conexão com o Banco de Dados de Homologação/Analytics**
    - Host: "10.0.0.73"
    - Port: "5432"
    - Database: "analytics"
    - Username: "analytics"
    - Password: solicitar para a equipe dev
    - Importante: A conexão com este banco só pode ser realizada após a conexão com a VPN. Solicite acesso à VPN com o time de Infraestrutura.


## Processo de Contribuição

1. **Branch de Desenvolvimento:**
    - As contribuições devem ser feitas a partir da branch dev.
    - Para isso, crie uma branch a partir de dev e abra o Merge Request (MR) para a mesma. O pipeline do projeto irá fazer o merge automaticamente para a branch main, que é a branch de homologaçã

2. **Padrão de Commit:**
    - feat(ci): adiciona nova configuração no pipeline
    - fix(auth): corrige erro de autenticação
    - chore(docs): atualiza documentação

3. **Padrão de Nomeação de Branch**
    - feat/nome-da-feature: Para novas funcionalidades.
    - fix/corrige-algo: Para correções de bugs.
    - chore/nome-do-chore: Para tarefas gerais.

4. **Pré-Commit com Ruff e Black**
    - O repositório está configurado com ferramentas de pré-commit Ruff e Black para garantir a qualidade do código. Certifique-se de que o código esteja formatado corretamente antes de enviar um commit
