# fmt: off
# ruff: noqa

from airflow import DAG
from airflow.datasets import Dataset
from airflow.models import Variable
from airflow.operators.bash_operator import BashOperator
from airflow.operators.empty import EmptyOperator
from airflow.utils.dates import days_ago

from datetime import timedelta

default_args = {
    'owner': 'DBT-Genrated',
    'retries': 5,
    'retry_delay': timedelta(minutes=10),
    'pool': 'dbt_pool'
}

with DAG(
    "run_model__silver_faturas_estagios",
    default_args=default_args,
    schedule='@daily',
    start_date=days_ago(1),
    tags=["dbt", "model"],
    max_active_runs=1,
    on_success_callback=None,
) as dag:

    end_task = EmptyOperator(
        task_id="end",
        outlets=[Dataset("silver_faturas_estagios_model")],
    )

    silver_faturas_estagios_task = BashOperator(
        task_id='run_silver_faturas_estagios',
        bash_command='rm -r /tmp/dbt_run_silver_faturas_estagios || true \
&& cp -r /opt/airflow/dags-config/repo/plugins/dbt /tmp/dbt_run_silver_faturas_estagios \
&& cd /tmp/dbt_run_silver_faturas_estagios \
&& dbt deps && dbt run --select silver_faturas_estagios \
&& rm -r /tmp/dbt_run_silver_faturas_estagios',
        env={
            'DBT_POSTGRES_HOST': Variable.get("ipea_dw_pg_host"),
            'DBT_POSTGRES_USER': Variable.get("ipea_dw_pg_user"),
            'DBT_POSTGRES_PASSWORD': Variable.get("ipea_dw_pg_password"),
            'DBT_POSTGRES_ENVIRONMENT': Variable.get("ipea_dw_pg_environment"),
            'DBT_POSTGRES_PORT': Variable.get("ipea_dw_pg_port"),
            'DBT_POSTGRES_DATABASE': Variable.get("ipea_dw_pg_db"),
        },
        append_env=True
    )

    not_null_silver_faturas_estagios_contrato_id_task = BashOperator(
        task_id='test_not_null_silver_faturas_estagios_contrato_id',
        bash_command='rm -r /tmp/dbt_test_not_null_silver_faturas_estagios_contrato_id || true \
&& cp -r /opt/airflow/dags-config/repo/plugins/dbt /tmp/dbt_test_not_null_silver_faturas_estagios_contrato_id \
&& cd /tmp/dbt_test_not_null_silver_faturas_estagios_contrato_id \
&& dbt deps && dbt test --select not_null_silver_faturas_estagios_contrato_id \
&& rm -r /tmp/dbt_test_not_null_silver_faturas_estagios_contrato_id',
        env={
            'DBT_POSTGRES_HOST': Variable.get("ipea_dw_pg_host"),
            'DBT_POSTGRES_USER': Variable.get("ipea_dw_pg_user"),
            'DBT_POSTGRES_PASSWORD': Variable.get("ipea_dw_pg_password"),
            'DBT_POSTGRES_ENVIRONMENT': Variable.get("ipea_dw_pg_environment"),
            'DBT_POSTGRES_PORT': Variable.get("ipea_dw_pg_port"),
            'DBT_POSTGRES_DATABASE': Variable.get("ipea_dw_pg_db"),
        },
        append_env=True
    )

    not_null_silver_faturas_estagios_valor_task = BashOperator(
        task_id='test_not_null_silver_faturas_estagios_valor',
        bash_command='rm -r /tmp/dbt_test_not_null_silver_faturas_estagios_valor || true \
&& cp -r /opt/airflow/dags-config/repo/plugins/dbt /tmp/dbt_test_not_null_silver_faturas_estagios_valor \
&& cd /tmp/dbt_test_not_null_silver_faturas_estagios_valor \
&& dbt deps && dbt test --select not_null_silver_faturas_estagios_valor \
&& rm -r /tmp/dbt_test_not_null_silver_faturas_estagios_valor',
        env={
            'DBT_POSTGRES_HOST': Variable.get("ipea_dw_pg_host"),
            'DBT_POSTGRES_USER': Variable.get("ipea_dw_pg_user"),
            'DBT_POSTGRES_PASSWORD': Variable.get("ipea_dw_pg_password"),
            'DBT_POSTGRES_ENVIRONMENT': Variable.get("ipea_dw_pg_environment"),
            'DBT_POSTGRES_PORT': Variable.get("ipea_dw_pg_port"),
            'DBT_POSTGRES_DATABASE': Variable.get("ipea_dw_pg_db"),
        },
        append_env=True
    )

    not_null_silver_faturas_estagios_estagio_despesa_task = BashOperator(
        task_id='test_not_null_silver_faturas_estagios_estagio_despesa',
        bash_command='rm -r /tmp/dbt_test_not_null_silver_faturas_estagios_estagio_despesa || true \
&& cp -r /opt/airflow/dags-config/repo/plugins/dbt /tmp/dbt_test_not_null_silver_faturas_estagios_estagio_despesa \
&& cd /tmp/dbt_test_not_null_silver_faturas_estagios_estagio_despesa \
&& dbt deps && dbt test --select not_null_silver_faturas_estagios_estagio_despesa \
&& rm -r /tmp/dbt_test_not_null_silver_faturas_estagios_estagio_despesa',
        env={
            'DBT_POSTGRES_HOST': Variable.get("ipea_dw_pg_host"),
            'DBT_POSTGRES_USER': Variable.get("ipea_dw_pg_user"),
            'DBT_POSTGRES_PASSWORD': Variable.get("ipea_dw_pg_password"),
            'DBT_POSTGRES_ENVIRONMENT': Variable.get("ipea_dw_pg_environment"),
            'DBT_POSTGRES_PORT': Variable.get("ipea_dw_pg_port"),
            'DBT_POSTGRES_DATABASE': Variable.get("ipea_dw_pg_db"),
        },
        append_env=True
    )

    accepted_values_silver_faturas_estagios_estagio_despesa__fatura__empenhado__liquidado__pago_task = BashOperator(
        task_id='test_accepted_values_silver_faturas_estagios_estagio_despesa__fatura__empenhado__liquidado__pago',
        bash_command='rm -r /tmp/dbt_test_accepted_values_silver_faturas_estagios_estagio_despesa__fatura__empenhado__liquidado__pago || true \
&& cp -r /opt/airflow/dags-config/repo/plugins/dbt /tmp/dbt_test_accepted_values_silver_faturas_estagios_estagio_despesa__fatura__empenhado__liquidado__pago \
&& cd /tmp/dbt_test_accepted_values_silver_faturas_estagios_estagio_despesa__fatura__empenhado__liquidado__pago \
&& dbt deps && dbt test --select accepted_values_silver_faturas_estagios_estagio_despesa__fatura__empenhado__liquidado__pago \
&& rm -r /tmp/dbt_test_accepted_values_silver_faturas_estagios_estagio_despesa__fatura__empenhado__liquidado__pago',
        env={
            'DBT_POSTGRES_HOST': Variable.get("ipea_dw_pg_host"),
            'DBT_POSTGRES_USER': Variable.get("ipea_dw_pg_user"),
            'DBT_POSTGRES_PASSWORD': Variable.get("ipea_dw_pg_password"),
            'DBT_POSTGRES_ENVIRONMENT': Variable.get("ipea_dw_pg_environment"),
            'DBT_POSTGRES_PORT': Variable.get("ipea_dw_pg_port"),
            'DBT_POSTGRES_DATABASE': Variable.get("ipea_dw_pg_db"),
        },
        append_env=True
    )

    not_null_silver_faturas_estagios_ne_ccor_task = BashOperator(
        task_id='test_not_null_silver_faturas_estagios_ne_ccor',
        bash_command='rm -r /tmp/dbt_test_not_null_silver_faturas_estagios_ne_ccor || true \
&& cp -r /opt/airflow/dags-config/repo/plugins/dbt /tmp/dbt_test_not_null_silver_faturas_estagios_ne_ccor \
&& cd /tmp/dbt_test_not_null_silver_faturas_estagios_ne_ccor \
&& dbt deps && dbt test --select not_null_silver_faturas_estagios_ne_ccor \
&& rm -r /tmp/dbt_test_not_null_silver_faturas_estagios_ne_ccor',
        env={
            'DBT_POSTGRES_HOST': Variable.get("ipea_dw_pg_host"),
            'DBT_POSTGRES_USER': Variable.get("ipea_dw_pg_user"),
            'DBT_POSTGRES_PASSWORD': Variable.get("ipea_dw_pg_password"),
            'DBT_POSTGRES_ENVIRONMENT': Variable.get("ipea_dw_pg_environment"),
            'DBT_POSTGRES_PORT': Variable.get("ipea_dw_pg_port"),
            'DBT_POSTGRES_DATABASE': Variable.get("ipea_dw_pg_db"),
        },
        append_env=True
    )

    not_null_silver_faturas_estagios_mes_task = BashOperator(
        task_id='test_not_null_silver_faturas_estagios_mes',
        bash_command='rm -r /tmp/dbt_test_not_null_silver_faturas_estagios_mes || true \
&& cp -r /opt/airflow/dags-config/repo/plugins/dbt /tmp/dbt_test_not_null_silver_faturas_estagios_mes \
&& cd /tmp/dbt_test_not_null_silver_faturas_estagios_mes \
&& dbt deps && dbt test --select not_null_silver_faturas_estagios_mes \
&& rm -r /tmp/dbt_test_not_null_silver_faturas_estagios_mes',
        env={
            'DBT_POSTGRES_HOST': Variable.get("ipea_dw_pg_host"),
            'DBT_POSTGRES_USER': Variable.get("ipea_dw_pg_user"),
            'DBT_POSTGRES_PASSWORD': Variable.get("ipea_dw_pg_password"),
            'DBT_POSTGRES_ENVIRONMENT': Variable.get("ipea_dw_pg_environment"),
            'DBT_POSTGRES_PORT': Variable.get("ipea_dw_pg_port"),
            'DBT_POSTGRES_DATABASE': Variable.get("ipea_dw_pg_db"),
        },
        append_env=True
    )

    silver_faturas_estagios_task >> not_null_silver_faturas_estagios_contrato_id_task >> end_task
    silver_faturas_estagios_task >> not_null_silver_faturas_estagios_valor_task >> end_task
    silver_faturas_estagios_task >> not_null_silver_faturas_estagios_estagio_despesa_task >> end_task
    silver_faturas_estagios_task >> accepted_values_silver_faturas_estagios_estagio_despesa__fatura__empenhado__liquidado__pago_task >> end_task
    silver_faturas_estagios_task >> not_null_silver_faturas_estagios_ne_ccor_task >> end_task
    silver_faturas_estagios_task >> not_null_silver_faturas_estagios_mes_task >> end_task
