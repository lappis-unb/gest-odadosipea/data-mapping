# fmt: off
# ruff: noqa

from airflow import DAG
from airflow.datasets import Dataset
from airflow.models import Variable
from airflow.operators.bash_operator import BashOperator
from airflow.operators.empty import EmptyOperator
from airflow.utils.dates import days_ago

from datetime import timedelta

default_args = {
    'owner': 'DBT-Genrated',
    'retries': 5,
    'retry_delay': timedelta(minutes=10),
    'pool': 'dbt_pool'
}

with DAG(
    "run_model__empenhos_tesouro",
    default_args=default_args,
    schedule='@daily',
    start_date=days_ago(1),
    tags=["dbt", "model"],
    max_active_runs=1,
    on_success_callback=None,
) as dag:

    end_task = EmptyOperator(
        task_id="end",
        outlets=[Dataset("empenhos_tesouro_model")],
    )

    empenhos_tesouro_task = BashOperator(
        task_id='run_empenhos_tesouro',
        bash_command='rm -r /tmp/dbt_run_empenhos_tesouro || true \
&& cp -r /opt/airflow/dags-config/repo/plugins/dbt /tmp/dbt_run_empenhos_tesouro \
&& cd /tmp/dbt_run_empenhos_tesouro \
&& dbt deps && dbt run --select empenhos_tesouro \
&& rm -r /tmp/dbt_run_empenhos_tesouro',
        env={
            'DBT_POSTGRES_HOST': Variable.get("ipea_dw_pg_host"),
            'DBT_POSTGRES_USER': Variable.get("ipea_dw_pg_user"),
            'DBT_POSTGRES_PASSWORD': Variable.get("ipea_dw_pg_password"),
            'DBT_POSTGRES_ENVIRONMENT': Variable.get("ipea_dw_pg_environment"),
            'DBT_POSTGRES_PORT': Variable.get("ipea_dw_pg_port"),
            'DBT_POSTGRES_DATABASE': Variable.get("ipea_dw_pg_db"),
        },
        append_env=True
    )

    not_null_empenhos_tesouro_ne_ccor_task = BashOperator(
        task_id='test_not_null_empenhos_tesouro_ne_ccor',
        bash_command='rm -r /tmp/dbt_test_not_null_empenhos_tesouro_ne_ccor || true \
&& cp -r /opt/airflow/dags-config/repo/plugins/dbt /tmp/dbt_test_not_null_empenhos_tesouro_ne_ccor \
&& cd /tmp/dbt_test_not_null_empenhos_tesouro_ne_ccor \
&& dbt deps && dbt test --select not_null_empenhos_tesouro_ne_ccor \
&& rm -r /tmp/dbt_test_not_null_empenhos_tesouro_ne_ccor',
        env={
            'DBT_POSTGRES_HOST': Variable.get("ipea_dw_pg_host"),
            'DBT_POSTGRES_USER': Variable.get("ipea_dw_pg_user"),
            'DBT_POSTGRES_PASSWORD': Variable.get("ipea_dw_pg_password"),
            'DBT_POSTGRES_ENVIRONMENT': Variable.get("ipea_dw_pg_environment"),
            'DBT_POSTGRES_PORT': Variable.get("ipea_dw_pg_port"),
            'DBT_POSTGRES_DATABASE': Variable.get("ipea_dw_pg_db"),
        },
        append_env=True
    )

    not_null_empenhos_tesouro_ne_num_processo_task = BashOperator(
        task_id='test_not_null_empenhos_tesouro_ne_num_processo',
        bash_command='rm -r /tmp/dbt_test_not_null_empenhos_tesouro_ne_num_processo || true \
&& cp -r /opt/airflow/dags-config/repo/plugins/dbt /tmp/dbt_test_not_null_empenhos_tesouro_ne_num_processo \
&& cd /tmp/dbt_test_not_null_empenhos_tesouro_ne_num_processo \
&& dbt deps && dbt test --select not_null_empenhos_tesouro_ne_num_processo \
&& rm -r /tmp/dbt_test_not_null_empenhos_tesouro_ne_num_processo',
        env={
            'DBT_POSTGRES_HOST': Variable.get("ipea_dw_pg_host"),
            'DBT_POSTGRES_USER': Variable.get("ipea_dw_pg_user"),
            'DBT_POSTGRES_PASSWORD': Variable.get("ipea_dw_pg_password"),
            'DBT_POSTGRES_ENVIRONMENT': Variable.get("ipea_dw_pg_environment"),
            'DBT_POSTGRES_PORT': Variable.get("ipea_dw_pg_port"),
            'DBT_POSTGRES_DATABASE': Variable.get("ipea_dw_pg_db"),
        },
        append_env=True
    )

    not_null_empenhos_tesouro_ne_ccor_descricao_task = BashOperator(
        task_id='test_not_null_empenhos_tesouro_ne_ccor_descricao',
        bash_command='rm -r /tmp/dbt_test_not_null_empenhos_tesouro_ne_ccor_descricao || true \
&& cp -r /opt/airflow/dags-config/repo/plugins/dbt /tmp/dbt_test_not_null_empenhos_tesouro_ne_ccor_descricao \
&& cd /tmp/dbt_test_not_null_empenhos_tesouro_ne_ccor_descricao \
&& dbt deps && dbt test --select not_null_empenhos_tesouro_ne_ccor_descricao \
&& rm -r /tmp/dbt_test_not_null_empenhos_tesouro_ne_ccor_descricao',
        env={
            'DBT_POSTGRES_HOST': Variable.get("ipea_dw_pg_host"),
            'DBT_POSTGRES_USER': Variable.get("ipea_dw_pg_user"),
            'DBT_POSTGRES_PASSWORD': Variable.get("ipea_dw_pg_password"),
            'DBT_POSTGRES_ENVIRONMENT': Variable.get("ipea_dw_pg_environment"),
            'DBT_POSTGRES_PORT': Variable.get("ipea_dw_pg_port"),
            'DBT_POSTGRES_DATABASE': Variable.get("ipea_dw_pg_db"),
        },
        append_env=True
    )

    not_null_empenhos_tesouro_doc_observacao_task = BashOperator(
        task_id='test_not_null_empenhos_tesouro_doc_observacao',
        bash_command='rm -r /tmp/dbt_test_not_null_empenhos_tesouro_doc_observacao || true \
&& cp -r /opt/airflow/dags-config/repo/plugins/dbt /tmp/dbt_test_not_null_empenhos_tesouro_doc_observacao \
&& cd /tmp/dbt_test_not_null_empenhos_tesouro_doc_observacao \
&& dbt deps && dbt test --select not_null_empenhos_tesouro_doc_observacao \
&& rm -r /tmp/dbt_test_not_null_empenhos_tesouro_doc_observacao',
        env={
            'DBT_POSTGRES_HOST': Variable.get("ipea_dw_pg_host"),
            'DBT_POSTGRES_USER': Variable.get("ipea_dw_pg_user"),
            'DBT_POSTGRES_PASSWORD': Variable.get("ipea_dw_pg_password"),
            'DBT_POSTGRES_ENVIRONMENT': Variable.get("ipea_dw_pg_environment"),
            'DBT_POSTGRES_PORT': Variable.get("ipea_dw_pg_port"),
            'DBT_POSTGRES_DATABASE': Variable.get("ipea_dw_pg_db"),
        },
        append_env=True
    )

    not_null_empenhos_tesouro_natureza_despesa_1_task = BashOperator(
        task_id='test_not_null_empenhos_tesouro_natureza_despesa_1',
        bash_command='rm -r /tmp/dbt_test_not_null_empenhos_tesouro_natureza_despesa_1 || true \
&& cp -r /opt/airflow/dags-config/repo/plugins/dbt /tmp/dbt_test_not_null_empenhos_tesouro_natureza_despesa_1 \
&& cd /tmp/dbt_test_not_null_empenhos_tesouro_natureza_despesa_1 \
&& dbt deps && dbt test --select not_null_empenhos_tesouro_natureza_despesa_1 \
&& rm -r /tmp/dbt_test_not_null_empenhos_tesouro_natureza_despesa_1',
        env={
            'DBT_POSTGRES_HOST': Variable.get("ipea_dw_pg_host"),
            'DBT_POSTGRES_USER': Variable.get("ipea_dw_pg_user"),
            'DBT_POSTGRES_PASSWORD': Variable.get("ipea_dw_pg_password"),
            'DBT_POSTGRES_ENVIRONMENT': Variable.get("ipea_dw_pg_environment"),
            'DBT_POSTGRES_PORT': Variable.get("ipea_dw_pg_port"),
            'DBT_POSTGRES_DATABASE': Variable.get("ipea_dw_pg_db"),
        },
        append_env=True
    )

    not_null_empenhos_tesouro_natureza_despesa_detalhada_task = BashOperator(
        task_id='test_not_null_empenhos_tesouro_natureza_despesa_detalhada',
        bash_command='rm -r /tmp/dbt_test_not_null_empenhos_tesouro_natureza_despesa_detalhada || true \
&& cp -r /opt/airflow/dags-config/repo/plugins/dbt /tmp/dbt_test_not_null_empenhos_tesouro_natureza_despesa_detalhada \
&& cd /tmp/dbt_test_not_null_empenhos_tesouro_natureza_despesa_detalhada \
&& dbt deps && dbt test --select not_null_empenhos_tesouro_natureza_despesa_detalhada \
&& rm -r /tmp/dbt_test_not_null_empenhos_tesouro_natureza_despesa_detalhada',
        env={
            'DBT_POSTGRES_HOST': Variable.get("ipea_dw_pg_host"),
            'DBT_POSTGRES_USER': Variable.get("ipea_dw_pg_user"),
            'DBT_POSTGRES_PASSWORD': Variable.get("ipea_dw_pg_password"),
            'DBT_POSTGRES_ENVIRONMENT': Variable.get("ipea_dw_pg_environment"),
            'DBT_POSTGRES_PORT': Variable.get("ipea_dw_pg_port"),
            'DBT_POSTGRES_DATABASE': Variable.get("ipea_dw_pg_db"),
        },
        append_env=True
    )

    not_null_empenhos_tesouro_natureza_despesa_detalhada_1_task = BashOperator(
        task_id='test_not_null_empenhos_tesouro_natureza_despesa_detalhada_1',
        bash_command='rm -r /tmp/dbt_test_not_null_empenhos_tesouro_natureza_despesa_detalhada_1 || true \
&& cp -r /opt/airflow/dags-config/repo/plugins/dbt /tmp/dbt_test_not_null_empenhos_tesouro_natureza_despesa_detalhada_1 \
&& cd /tmp/dbt_test_not_null_empenhos_tesouro_natureza_despesa_detalhada_1 \
&& dbt deps && dbt test --select not_null_empenhos_tesouro_natureza_despesa_detalhada_1 \
&& rm -r /tmp/dbt_test_not_null_empenhos_tesouro_natureza_despesa_detalhada_1',
        env={
            'DBT_POSTGRES_HOST': Variable.get("ipea_dw_pg_host"),
            'DBT_POSTGRES_USER': Variable.get("ipea_dw_pg_user"),
            'DBT_POSTGRES_PASSWORD': Variable.get("ipea_dw_pg_password"),
            'DBT_POSTGRES_ENVIRONMENT': Variable.get("ipea_dw_pg_environment"),
            'DBT_POSTGRES_PORT': Variable.get("ipea_dw_pg_port"),
            'DBT_POSTGRES_DATABASE': Variable.get("ipea_dw_pg_db"),
        },
        append_env=True
    )

    not_null_empenhos_tesouro_ne_ccor_favorecido_task = BashOperator(
        task_id='test_not_null_empenhos_tesouro_ne_ccor_favorecido',
        bash_command='rm -r /tmp/dbt_test_not_null_empenhos_tesouro_ne_ccor_favorecido || true \
&& cp -r /opt/airflow/dags-config/repo/plugins/dbt /tmp/dbt_test_not_null_empenhos_tesouro_ne_ccor_favorecido \
&& cd /tmp/dbt_test_not_null_empenhos_tesouro_ne_ccor_favorecido \
&& dbt deps && dbt test --select not_null_empenhos_tesouro_ne_ccor_favorecido \
&& rm -r /tmp/dbt_test_not_null_empenhos_tesouro_ne_ccor_favorecido',
        env={
            'DBT_POSTGRES_HOST': Variable.get("ipea_dw_pg_host"),
            'DBT_POSTGRES_USER': Variable.get("ipea_dw_pg_user"),
            'DBT_POSTGRES_PASSWORD': Variable.get("ipea_dw_pg_password"),
            'DBT_POSTGRES_ENVIRONMENT': Variable.get("ipea_dw_pg_environment"),
            'DBT_POSTGRES_PORT': Variable.get("ipea_dw_pg_port"),
            'DBT_POSTGRES_DATABASE': Variable.get("ipea_dw_pg_db"),
        },
        append_env=True
    )

    not_null_empenhos_tesouro_ne_ccor_favorecido_1_task = BashOperator(
        task_id='test_not_null_empenhos_tesouro_ne_ccor_favorecido_1',
        bash_command='rm -r /tmp/dbt_test_not_null_empenhos_tesouro_ne_ccor_favorecido_1 || true \
&& cp -r /opt/airflow/dags-config/repo/plugins/dbt /tmp/dbt_test_not_null_empenhos_tesouro_ne_ccor_favorecido_1 \
&& cd /tmp/dbt_test_not_null_empenhos_tesouro_ne_ccor_favorecido_1 \
&& dbt deps && dbt test --select not_null_empenhos_tesouro_ne_ccor_favorecido_1 \
&& rm -r /tmp/dbt_test_not_null_empenhos_tesouro_ne_ccor_favorecido_1',
        env={
            'DBT_POSTGRES_HOST': Variable.get("ipea_dw_pg_host"),
            'DBT_POSTGRES_USER': Variable.get("ipea_dw_pg_user"),
            'DBT_POSTGRES_PASSWORD': Variable.get("ipea_dw_pg_password"),
            'DBT_POSTGRES_ENVIRONMENT': Variable.get("ipea_dw_pg_environment"),
            'DBT_POSTGRES_PORT': Variable.get("ipea_dw_pg_port"),
            'DBT_POSTGRES_DATABASE': Variable.get("ipea_dw_pg_db"),
        },
        append_env=True
    )

    accepted_values_empenhos_tesouro_ne_ccor_ano_emissao__2000__2001__2002__2003__2004__2005__2006__2007__2008__2009__2010__2011__2012__2013__2014__2015__2016__2017__2018__2019__2020__2021__2022__2023__2024_task = BashOperator(
        task_id='test_accepted_values_empenhos_tesouro_ne_ccor_ano_emissao__2000__2001__2002__2003__2004__2005__2006__2007__2008__2009__2010__2011__2012__2013__2014__2015__2016__2017__2018__2019__2020__2021__2022__2023__2024',
        bash_command='rm -r /tmp/dbt_test_accepted_values_empenhos_tesouro_ne_ccor_ano_emissao__2000__2001__2002__2003__2004__2005__2006__2007__2008__2009__2010__2011__2012__2013__2014__2015__2016__2017__2018__2019__2020__2021__2022__2023__2024 || true \
&& cp -r /opt/airflow/dags-config/repo/plugins/dbt /tmp/dbt_test_accepted_values_empenhos_tesouro_ne_ccor_ano_emissao__2000__2001__2002__2003__2004__2005__2006__2007__2008__2009__2010__2011__2012__2013__2014__2015__2016__2017__2018__2019__2020__2021__2022__2023__2024 \
&& cd /tmp/dbt_test_accepted_values_empenhos_tesouro_ne_ccor_ano_emissao__2000__2001__2002__2003__2004__2005__2006__2007__2008__2009__2010__2011__2012__2013__2014__2015__2016__2017__2018__2019__2020__2021__2022__2023__2024 \
&& dbt deps && dbt test --select accepted_values_empenhos_tesouro_ne_ccor_ano_emissao__2000__2001__2002__2003__2004__2005__2006__2007__2008__2009__2010__2011__2012__2013__2014__2015__2016__2017__2018__2019__2020__2021__2022__2023__2024 \
&& rm -r /tmp/dbt_test_accepted_values_empenhos_tesouro_ne_ccor_ano_emissao__2000__2001__2002__2003__2004__2005__2006__2007__2008__2009__2010__2011__2012__2013__2014__2015__2016__2017__2018__2019__2020__2021__2022__2023__2024',
        env={
            'DBT_POSTGRES_HOST': Variable.get("ipea_dw_pg_host"),
            'DBT_POSTGRES_USER': Variable.get("ipea_dw_pg_user"),
            'DBT_POSTGRES_PASSWORD': Variable.get("ipea_dw_pg_password"),
            'DBT_POSTGRES_ENVIRONMENT': Variable.get("ipea_dw_pg_environment"),
            'DBT_POSTGRES_PORT': Variable.get("ipea_dw_pg_port"),
            'DBT_POSTGRES_DATABASE': Variable.get("ipea_dw_pg_db"),
        },
        append_env=True
    )

    accepted_values_empenhos_tesouro_ne_ccor_ano_emissao_1__2000__2001__2002__2003__2004__2005__2006__2007__2008__2009__2010__2011__2012__2013__2014__2015__2016__2017__2018__2019__2020__2021__2022__2023__2024_task = BashOperator(
        task_id='test_accepted_values_empenhos_tesouro_ne_ccor_ano_emissao_1__2000__2001__2002__2003__2004__2005__2006__2007__2008__2009__2010__2011__2012__2013__2014__2015__2016__2017__2018__2019__2020__2021__2022__2023__2024',
        bash_command='rm -r /tmp/dbt_test_accepted_values_empenhos_tesouro_ne_ccor_ano_emissao_1__2000__2001__2002__2003__2004__2005__2006__2007__2008__2009__2010__2011__2012__2013__2014__2015__2016__2017__2018__2019__2020__2021__2022__2023__2024 || true \
&& cp -r /opt/airflow/dags-config/repo/plugins/dbt /tmp/dbt_test_accepted_values_empenhos_tesouro_ne_ccor_ano_emissao_1__2000__2001__2002__2003__2004__2005__2006__2007__2008__2009__2010__2011__2012__2013__2014__2015__2016__2017__2018__2019__2020__2021__2022__2023__2024 \
&& cd /tmp/dbt_test_accepted_values_empenhos_tesouro_ne_ccor_ano_emissao_1__2000__2001__2002__2003__2004__2005__2006__2007__2008__2009__2010__2011__2012__2013__2014__2015__2016__2017__2018__2019__2020__2021__2022__2023__2024 \
&& dbt deps && dbt test --select accepted_values_empenhos_tesouro_ne_ccor_ano_emissao_1__2000__2001__2002__2003__2004__2005__2006__2007__2008__2009__2010__2011__2012__2013__2014__2015__2016__2017__2018__2019__2020__2021__2022__2023__2024 \
&& rm -r /tmp/dbt_test_accepted_values_empenhos_tesouro_ne_ccor_ano_emissao_1__2000__2001__2002__2003__2004__2005__2006__2007__2008__2009__2010__2011__2012__2013__2014__2015__2016__2017__2018__2019__2020__2021__2022__2023__2024',
        env={
            'DBT_POSTGRES_HOST': Variable.get("ipea_dw_pg_host"),
            'DBT_POSTGRES_USER': Variable.get("ipea_dw_pg_user"),
            'DBT_POSTGRES_PASSWORD': Variable.get("ipea_dw_pg_password"),
            'DBT_POSTGRES_ENVIRONMENT': Variable.get("ipea_dw_pg_environment"),
            'DBT_POSTGRES_PORT': Variable.get("ipea_dw_pg_port"),
            'DBT_POSTGRES_DATABASE': Variable.get("ipea_dw_pg_db"),
        },
        append_env=True
    )

    not_null_empenhos_tesouro_despesas_empenhadas_saldo_task = BashOperator(
        task_id='test_not_null_empenhos_tesouro_despesas_empenhadas_saldo',
        bash_command='rm -r /tmp/dbt_test_not_null_empenhos_tesouro_despesas_empenhadas_saldo || true \
&& cp -r /opt/airflow/dags-config/repo/plugins/dbt /tmp/dbt_test_not_null_empenhos_tesouro_despesas_empenhadas_saldo \
&& cd /tmp/dbt_test_not_null_empenhos_tesouro_despesas_empenhadas_saldo \
&& dbt deps && dbt test --select not_null_empenhos_tesouro_despesas_empenhadas_saldo \
&& rm -r /tmp/dbt_test_not_null_empenhos_tesouro_despesas_empenhadas_saldo',
        env={
            'DBT_POSTGRES_HOST': Variable.get("ipea_dw_pg_host"),
            'DBT_POSTGRES_USER': Variable.get("ipea_dw_pg_user"),
            'DBT_POSTGRES_PASSWORD': Variable.get("ipea_dw_pg_password"),
            'DBT_POSTGRES_ENVIRONMENT': Variable.get("ipea_dw_pg_environment"),
            'DBT_POSTGRES_PORT': Variable.get("ipea_dw_pg_port"),
            'DBT_POSTGRES_DATABASE': Variable.get("ipea_dw_pg_db"),
        },
        append_env=True
    )

    not_null_empenhos_tesouro_despesas_empenhadas_movim_liquido_task = BashOperator(
        task_id='test_not_null_empenhos_tesouro_despesas_empenhadas_movim_liquido',
        bash_command='rm -r /tmp/dbt_test_not_null_empenhos_tesouro_despesas_empenhadas_movim_liquido || true \
&& cp -r /opt/airflow/dags-config/repo/plugins/dbt /tmp/dbt_test_not_null_empenhos_tesouro_despesas_empenhadas_movim_liquido \
&& cd /tmp/dbt_test_not_null_empenhos_tesouro_despesas_empenhadas_movim_liquido \
&& dbt deps && dbt test --select not_null_empenhos_tesouro_despesas_empenhadas_movim_liquido \
&& rm -r /tmp/dbt_test_not_null_empenhos_tesouro_despesas_empenhadas_movim_liquido',
        env={
            'DBT_POSTGRES_HOST': Variable.get("ipea_dw_pg_host"),
            'DBT_POSTGRES_USER': Variable.get("ipea_dw_pg_user"),
            'DBT_POSTGRES_PASSWORD': Variable.get("ipea_dw_pg_password"),
            'DBT_POSTGRES_ENVIRONMENT': Variable.get("ipea_dw_pg_environment"),
            'DBT_POSTGRES_PORT': Variable.get("ipea_dw_pg_port"),
            'DBT_POSTGRES_DATABASE': Variable.get("ipea_dw_pg_db"),
        },
        append_env=True
    )

    empenhos_tesouro_task >> not_null_empenhos_tesouro_ne_ccor_task >> end_task
    empenhos_tesouro_task >> not_null_empenhos_tesouro_ne_num_processo_task >> end_task
    empenhos_tesouro_task >> not_null_empenhos_tesouro_ne_ccor_descricao_task >> end_task
    empenhos_tesouro_task >> not_null_empenhos_tesouro_doc_observacao_task >> end_task
    empenhos_tesouro_task >> not_null_empenhos_tesouro_natureza_despesa_1_task >> end_task
    empenhos_tesouro_task >> not_null_empenhos_tesouro_natureza_despesa_detalhada_task >> end_task
    empenhos_tesouro_task >> not_null_empenhos_tesouro_natureza_despesa_detalhada_1_task >> end_task
    empenhos_tesouro_task >> not_null_empenhos_tesouro_ne_ccor_favorecido_task >> end_task
    empenhos_tesouro_task >> not_null_empenhos_tesouro_ne_ccor_favorecido_1_task >> end_task
    empenhos_tesouro_task >> accepted_values_empenhos_tesouro_ne_ccor_ano_emissao__2000__2001__2002__2003__2004__2005__2006__2007__2008__2009__2010__2011__2012__2013__2014__2015__2016__2017__2018__2019__2020__2021__2022__2023__2024_task >> end_task
    empenhos_tesouro_task >> accepted_values_empenhos_tesouro_ne_ccor_ano_emissao_1__2000__2001__2002__2003__2004__2005__2006__2007__2008__2009__2010__2011__2012__2013__2014__2015__2016__2017__2018__2019__2020__2021__2022__2023__2024_task >> end_task
    empenhos_tesouro_task >> not_null_empenhos_tesouro_despesas_empenhadas_saldo_task >> end_task
    empenhos_tesouro_task >> not_null_empenhos_tesouro_despesas_empenhadas_movim_liquido_task >> end_task
