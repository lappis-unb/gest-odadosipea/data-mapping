# fmt: off
# ruff: noqa

from airflow import DAG
from airflow.datasets import Dataset
from airflow.models import Variable
from airflow.operators.bash_operator import BashOperator
from airflow.operators.empty import EmptyOperator
from airflow.utils.dates import days_ago

from datetime import timedelta

default_args = {
    'owner': 'DBT-Genrated',
    'retries': 5,
    'retry_delay': timedelta(minutes=10),
    'pool': 'dbt_pool'
}

with DAG(
    "run_model__silver_contratos_empenhos",
    default_args=default_args,
    schedule=[Dataset('empenhos_tesouro_model')],
    start_date=days_ago(1),
    tags=["dbt", "model"],
    max_active_runs=1,
    on_success_callback=None,
) as dag:

    end_task = EmptyOperator(
        task_id="end",
        outlets=[Dataset("silver_contratos_empenhos_model")],
    )

    silver_contratos_empenhos_task = BashOperator(
        task_id='run_silver_contratos_empenhos',
        bash_command='rm -r /tmp/dbt_run_silver_contratos_empenhos || true \
&& cp -r /opt/airflow/dags-config/repo/plugins/dbt /tmp/dbt_run_silver_contratos_empenhos \
&& cd /tmp/dbt_run_silver_contratos_empenhos \
&& dbt deps && dbt run --select silver_contratos_empenhos \
&& rm -r /tmp/dbt_run_silver_contratos_empenhos',
        env={
            'DBT_POSTGRES_HOST': Variable.get("ipea_dw_pg_host"),
            'DBT_POSTGRES_USER': Variable.get("ipea_dw_pg_user"),
            'DBT_POSTGRES_PASSWORD': Variable.get("ipea_dw_pg_password"),
            'DBT_POSTGRES_ENVIRONMENT': Variable.get("ipea_dw_pg_environment"),
            'DBT_POSTGRES_PORT': Variable.get("ipea_dw_pg_port"),
            'DBT_POSTGRES_DATABASE': Variable.get("ipea_dw_pg_db"),
        },
        append_env=True
    )

    not_null_silver_contratos_empenhos_ne_ccor_task = BashOperator(
        task_id='test_not_null_silver_contratos_empenhos_ne_ccor',
        bash_command='rm -r /tmp/dbt_test_not_null_silver_contratos_empenhos_ne_ccor || true \
&& cp -r /opt/airflow/dags-config/repo/plugins/dbt /tmp/dbt_test_not_null_silver_contratos_empenhos_ne_ccor \
&& cd /tmp/dbt_test_not_null_silver_contratos_empenhos_ne_ccor \
&& dbt deps && dbt test --select not_null_silver_contratos_empenhos_ne_ccor \
&& rm -r /tmp/dbt_test_not_null_silver_contratos_empenhos_ne_ccor',
        env={
            'DBT_POSTGRES_HOST': Variable.get("ipea_dw_pg_host"),
            'DBT_POSTGRES_USER': Variable.get("ipea_dw_pg_user"),
            'DBT_POSTGRES_PASSWORD': Variable.get("ipea_dw_pg_password"),
            'DBT_POSTGRES_ENVIRONMENT': Variable.get("ipea_dw_pg_environment"),
            'DBT_POSTGRES_PORT': Variable.get("ipea_dw_pg_port"),
            'DBT_POSTGRES_DATABASE': Variable.get("ipea_dw_pg_db"),
        },
        append_env=True
    )

    not_null_silver_contratos_empenhos_ne_informacao_complementar_task = BashOperator(
        task_id='test_not_null_silver_contratos_empenhos_ne_informacao_complementar',
        bash_command='rm -r /tmp/dbt_test_not_null_silver_contratos_empenhos_ne_informacao_complementar || true \
&& cp -r /opt/airflow/dags-config/repo/plugins/dbt /tmp/dbt_test_not_null_silver_contratos_empenhos_ne_informacao_complementar \
&& cd /tmp/dbt_test_not_null_silver_contratos_empenhos_ne_informacao_complementar \
&& dbt deps && dbt test --select not_null_silver_contratos_empenhos_ne_informacao_complementar \
&& rm -r /tmp/dbt_test_not_null_silver_contratos_empenhos_ne_informacao_complementar',
        env={
            'DBT_POSTGRES_HOST': Variable.get("ipea_dw_pg_host"),
            'DBT_POSTGRES_USER': Variable.get("ipea_dw_pg_user"),
            'DBT_POSTGRES_PASSWORD': Variable.get("ipea_dw_pg_password"),
            'DBT_POSTGRES_ENVIRONMENT': Variable.get("ipea_dw_pg_environment"),
            'DBT_POSTGRES_PORT': Variable.get("ipea_dw_pg_port"),
            'DBT_POSTGRES_DATABASE': Variable.get("ipea_dw_pg_db"),
        },
        append_env=True
    )

    not_null_silver_contratos_empenhos_ne_num_processo_task = BashOperator(
        task_id='test_not_null_silver_contratos_empenhos_ne_num_processo',
        bash_command='rm -r /tmp/dbt_test_not_null_silver_contratos_empenhos_ne_num_processo || true \
&& cp -r /opt/airflow/dags-config/repo/plugins/dbt /tmp/dbt_test_not_null_silver_contratos_empenhos_ne_num_processo \
&& cd /tmp/dbt_test_not_null_silver_contratos_empenhos_ne_num_processo \
&& dbt deps && dbt test --select not_null_silver_contratos_empenhos_ne_num_processo \
&& rm -r /tmp/dbt_test_not_null_silver_contratos_empenhos_ne_num_processo',
        env={
            'DBT_POSTGRES_HOST': Variable.get("ipea_dw_pg_host"),
            'DBT_POSTGRES_USER': Variable.get("ipea_dw_pg_user"),
            'DBT_POSTGRES_PASSWORD': Variable.get("ipea_dw_pg_password"),
            'DBT_POSTGRES_ENVIRONMENT': Variable.get("ipea_dw_pg_environment"),
            'DBT_POSTGRES_PORT': Variable.get("ipea_dw_pg_port"),
            'DBT_POSTGRES_DATABASE': Variable.get("ipea_dw_pg_db"),
        },
        append_env=True
    )

    not_null_silver_contratos_empenhos_ne_ccor_descricao_task = BashOperator(
        task_id='test_not_null_silver_contratos_empenhos_ne_ccor_descricao',
        bash_command='rm -r /tmp/dbt_test_not_null_silver_contratos_empenhos_ne_ccor_descricao || true \
&& cp -r /opt/airflow/dags-config/repo/plugins/dbt /tmp/dbt_test_not_null_silver_contratos_empenhos_ne_ccor_descricao \
&& cd /tmp/dbt_test_not_null_silver_contratos_empenhos_ne_ccor_descricao \
&& dbt deps && dbt test --select not_null_silver_contratos_empenhos_ne_ccor_descricao \
&& rm -r /tmp/dbt_test_not_null_silver_contratos_empenhos_ne_ccor_descricao',
        env={
            'DBT_POSTGRES_HOST': Variable.get("ipea_dw_pg_host"),
            'DBT_POSTGRES_USER': Variable.get("ipea_dw_pg_user"),
            'DBT_POSTGRES_PASSWORD': Variable.get("ipea_dw_pg_password"),
            'DBT_POSTGRES_ENVIRONMENT': Variable.get("ipea_dw_pg_environment"),
            'DBT_POSTGRES_PORT': Variable.get("ipea_dw_pg_port"),
            'DBT_POSTGRES_DATABASE': Variable.get("ipea_dw_pg_db"),
        },
        append_env=True
    )

    not_null_silver_contratos_empenhos_doc_observacao_task = BashOperator(
        task_id='test_not_null_silver_contratos_empenhos_doc_observacao',
        bash_command='rm -r /tmp/dbt_test_not_null_silver_contratos_empenhos_doc_observacao || true \
&& cp -r /opt/airflow/dags-config/repo/plugins/dbt /tmp/dbt_test_not_null_silver_contratos_empenhos_doc_observacao \
&& cd /tmp/dbt_test_not_null_silver_contratos_empenhos_doc_observacao \
&& dbt deps && dbt test --select not_null_silver_contratos_empenhos_doc_observacao \
&& rm -r /tmp/dbt_test_not_null_silver_contratos_empenhos_doc_observacao',
        env={
            'DBT_POSTGRES_HOST': Variable.get("ipea_dw_pg_host"),
            'DBT_POSTGRES_USER': Variable.get("ipea_dw_pg_user"),
            'DBT_POSTGRES_PASSWORD': Variable.get("ipea_dw_pg_password"),
            'DBT_POSTGRES_ENVIRONMENT': Variable.get("ipea_dw_pg_environment"),
            'DBT_POSTGRES_PORT': Variable.get("ipea_dw_pg_port"),
            'DBT_POSTGRES_DATABASE': Variable.get("ipea_dw_pg_db"),
        },
        append_env=True
    )

    not_null_silver_contratos_empenhos_natureza_despesa_task = BashOperator(
        task_id='test_not_null_silver_contratos_empenhos_natureza_despesa',
        bash_command='rm -r /tmp/dbt_test_not_null_silver_contratos_empenhos_natureza_despesa || true \
&& cp -r /opt/airflow/dags-config/repo/plugins/dbt /tmp/dbt_test_not_null_silver_contratos_empenhos_natureza_despesa \
&& cd /tmp/dbt_test_not_null_silver_contratos_empenhos_natureza_despesa \
&& dbt deps && dbt test --select not_null_silver_contratos_empenhos_natureza_despesa \
&& rm -r /tmp/dbt_test_not_null_silver_contratos_empenhos_natureza_despesa',
        env={
            'DBT_POSTGRES_HOST': Variable.get("ipea_dw_pg_host"),
            'DBT_POSTGRES_USER': Variable.get("ipea_dw_pg_user"),
            'DBT_POSTGRES_PASSWORD': Variable.get("ipea_dw_pg_password"),
            'DBT_POSTGRES_ENVIRONMENT': Variable.get("ipea_dw_pg_environment"),
            'DBT_POSTGRES_PORT': Variable.get("ipea_dw_pg_port"),
            'DBT_POSTGRES_DATABASE': Variable.get("ipea_dw_pg_db"),
        },
        append_env=True
    )

    not_null_silver_contratos_empenhos_natureza_despesa_1_task = BashOperator(
        task_id='test_not_null_silver_contratos_empenhos_natureza_despesa_1',
        bash_command='rm -r /tmp/dbt_test_not_null_silver_contratos_empenhos_natureza_despesa_1 || true \
&& cp -r /opt/airflow/dags-config/repo/plugins/dbt /tmp/dbt_test_not_null_silver_contratos_empenhos_natureza_despesa_1 \
&& cd /tmp/dbt_test_not_null_silver_contratos_empenhos_natureza_despesa_1 \
&& dbt deps && dbt test --select not_null_silver_contratos_empenhos_natureza_despesa_1 \
&& rm -r /tmp/dbt_test_not_null_silver_contratos_empenhos_natureza_despesa_1',
        env={
            'DBT_POSTGRES_HOST': Variable.get("ipea_dw_pg_host"),
            'DBT_POSTGRES_USER': Variable.get("ipea_dw_pg_user"),
            'DBT_POSTGRES_PASSWORD': Variable.get("ipea_dw_pg_password"),
            'DBT_POSTGRES_ENVIRONMENT': Variable.get("ipea_dw_pg_environment"),
            'DBT_POSTGRES_PORT': Variable.get("ipea_dw_pg_port"),
            'DBT_POSTGRES_DATABASE': Variable.get("ipea_dw_pg_db"),
        },
        append_env=True
    )

    not_null_silver_contratos_empenhos_natureza_despesa_detalhada_task = BashOperator(
        task_id='test_not_null_silver_contratos_empenhos_natureza_despesa_detalhada',
        bash_command='rm -r /tmp/dbt_test_not_null_silver_contratos_empenhos_natureza_despesa_detalhada || true \
&& cp -r /opt/airflow/dags-config/repo/plugins/dbt /tmp/dbt_test_not_null_silver_contratos_empenhos_natureza_despesa_detalhada \
&& cd /tmp/dbt_test_not_null_silver_contratos_empenhos_natureza_despesa_detalhada \
&& dbt deps && dbt test --select not_null_silver_contratos_empenhos_natureza_despesa_detalhada \
&& rm -r /tmp/dbt_test_not_null_silver_contratos_empenhos_natureza_despesa_detalhada',
        env={
            'DBT_POSTGRES_HOST': Variable.get("ipea_dw_pg_host"),
            'DBT_POSTGRES_USER': Variable.get("ipea_dw_pg_user"),
            'DBT_POSTGRES_PASSWORD': Variable.get("ipea_dw_pg_password"),
            'DBT_POSTGRES_ENVIRONMENT': Variable.get("ipea_dw_pg_environment"),
            'DBT_POSTGRES_PORT': Variable.get("ipea_dw_pg_port"),
            'DBT_POSTGRES_DATABASE': Variable.get("ipea_dw_pg_db"),
        },
        append_env=True
    )

    not_null_silver_contratos_empenhos_natureza_despesa_detalhada_1_task = BashOperator(
        task_id='test_not_null_silver_contratos_empenhos_natureza_despesa_detalhada_1',
        bash_command='rm -r /tmp/dbt_test_not_null_silver_contratos_empenhos_natureza_despesa_detalhada_1 || true \
&& cp -r /opt/airflow/dags-config/repo/plugins/dbt /tmp/dbt_test_not_null_silver_contratos_empenhos_natureza_despesa_detalhada_1 \
&& cd /tmp/dbt_test_not_null_silver_contratos_empenhos_natureza_despesa_detalhada_1 \
&& dbt deps && dbt test --select not_null_silver_contratos_empenhos_natureza_despesa_detalhada_1 \
&& rm -r /tmp/dbt_test_not_null_silver_contratos_empenhos_natureza_despesa_detalhada_1',
        env={
            'DBT_POSTGRES_HOST': Variable.get("ipea_dw_pg_host"),
            'DBT_POSTGRES_USER': Variable.get("ipea_dw_pg_user"),
            'DBT_POSTGRES_PASSWORD': Variable.get("ipea_dw_pg_password"),
            'DBT_POSTGRES_ENVIRONMENT': Variable.get("ipea_dw_pg_environment"),
            'DBT_POSTGRES_PORT': Variable.get("ipea_dw_pg_port"),
            'DBT_POSTGRES_DATABASE': Variable.get("ipea_dw_pg_db"),
        },
        append_env=True
    )

    not_null_silver_contratos_empenhos_ne_ccor_favorecido_task = BashOperator(
        task_id='test_not_null_silver_contratos_empenhos_ne_ccor_favorecido',
        bash_command='rm -r /tmp/dbt_test_not_null_silver_contratos_empenhos_ne_ccor_favorecido || true \
&& cp -r /opt/airflow/dags-config/repo/plugins/dbt /tmp/dbt_test_not_null_silver_contratos_empenhos_ne_ccor_favorecido \
&& cd /tmp/dbt_test_not_null_silver_contratos_empenhos_ne_ccor_favorecido \
&& dbt deps && dbt test --select not_null_silver_contratos_empenhos_ne_ccor_favorecido \
&& rm -r /tmp/dbt_test_not_null_silver_contratos_empenhos_ne_ccor_favorecido',
        env={
            'DBT_POSTGRES_HOST': Variable.get("ipea_dw_pg_host"),
            'DBT_POSTGRES_USER': Variable.get("ipea_dw_pg_user"),
            'DBT_POSTGRES_PASSWORD': Variable.get("ipea_dw_pg_password"),
            'DBT_POSTGRES_ENVIRONMENT': Variable.get("ipea_dw_pg_environment"),
            'DBT_POSTGRES_PORT': Variable.get("ipea_dw_pg_port"),
            'DBT_POSTGRES_DATABASE': Variable.get("ipea_dw_pg_db"),
        },
        append_env=True
    )

    not_null_silver_contratos_empenhos_ne_ccor_favorecido_1_task = BashOperator(
        task_id='test_not_null_silver_contratos_empenhos_ne_ccor_favorecido_1',
        bash_command='rm -r /tmp/dbt_test_not_null_silver_contratos_empenhos_ne_ccor_favorecido_1 || true \
&& cp -r /opt/airflow/dags-config/repo/plugins/dbt /tmp/dbt_test_not_null_silver_contratos_empenhos_ne_ccor_favorecido_1 \
&& cd /tmp/dbt_test_not_null_silver_contratos_empenhos_ne_ccor_favorecido_1 \
&& dbt deps && dbt test --select not_null_silver_contratos_empenhos_ne_ccor_favorecido_1 \
&& rm -r /tmp/dbt_test_not_null_silver_contratos_empenhos_ne_ccor_favorecido_1',
        env={
            'DBT_POSTGRES_HOST': Variable.get("ipea_dw_pg_host"),
            'DBT_POSTGRES_USER': Variable.get("ipea_dw_pg_user"),
            'DBT_POSTGRES_PASSWORD': Variable.get("ipea_dw_pg_password"),
            'DBT_POSTGRES_ENVIRONMENT': Variable.get("ipea_dw_pg_environment"),
            'DBT_POSTGRES_PORT': Variable.get("ipea_dw_pg_port"),
            'DBT_POSTGRES_DATABASE': Variable.get("ipea_dw_pg_db"),
        },
        append_env=True
    )

    not_null_silver_contratos_empenhos_ne_ccor_ano_emissao_task = BashOperator(
        task_id='test_not_null_silver_contratos_empenhos_ne_ccor_ano_emissao',
        bash_command='rm -r /tmp/dbt_test_not_null_silver_contratos_empenhos_ne_ccor_ano_emissao || true \
&& cp -r /opt/airflow/dags-config/repo/plugins/dbt /tmp/dbt_test_not_null_silver_contratos_empenhos_ne_ccor_ano_emissao \
&& cd /tmp/dbt_test_not_null_silver_contratos_empenhos_ne_ccor_ano_emissao \
&& dbt deps && dbt test --select not_null_silver_contratos_empenhos_ne_ccor_ano_emissao \
&& rm -r /tmp/dbt_test_not_null_silver_contratos_empenhos_ne_ccor_ano_emissao',
        env={
            'DBT_POSTGRES_HOST': Variable.get("ipea_dw_pg_host"),
            'DBT_POSTGRES_USER': Variable.get("ipea_dw_pg_user"),
            'DBT_POSTGRES_PASSWORD': Variable.get("ipea_dw_pg_password"),
            'DBT_POSTGRES_ENVIRONMENT': Variable.get("ipea_dw_pg_environment"),
            'DBT_POSTGRES_PORT': Variable.get("ipea_dw_pg_port"),
            'DBT_POSTGRES_DATABASE': Variable.get("ipea_dw_pg_db"),
        },
        append_env=True
    )

    not_null_silver_contratos_empenhos_ne_ccor_ano_emissao_1_task = BashOperator(
        task_id='test_not_null_silver_contratos_empenhos_ne_ccor_ano_emissao_1',
        bash_command='rm -r /tmp/dbt_test_not_null_silver_contratos_empenhos_ne_ccor_ano_emissao_1 || true \
&& cp -r /opt/airflow/dags-config/repo/plugins/dbt /tmp/dbt_test_not_null_silver_contratos_empenhos_ne_ccor_ano_emissao_1 \
&& cd /tmp/dbt_test_not_null_silver_contratos_empenhos_ne_ccor_ano_emissao_1 \
&& dbt deps && dbt test --select not_null_silver_contratos_empenhos_ne_ccor_ano_emissao_1 \
&& rm -r /tmp/dbt_test_not_null_silver_contratos_empenhos_ne_ccor_ano_emissao_1',
        env={
            'DBT_POSTGRES_HOST': Variable.get("ipea_dw_pg_host"),
            'DBT_POSTGRES_USER': Variable.get("ipea_dw_pg_user"),
            'DBT_POSTGRES_PASSWORD': Variable.get("ipea_dw_pg_password"),
            'DBT_POSTGRES_ENVIRONMENT': Variable.get("ipea_dw_pg_environment"),
            'DBT_POSTGRES_PORT': Variable.get("ipea_dw_pg_port"),
            'DBT_POSTGRES_DATABASE': Variable.get("ipea_dw_pg_db"),
        },
        append_env=True
    )

    not_null_silver_contratos_empenhos_despesas_empenhadas_saldo_task = BashOperator(
        task_id='test_not_null_silver_contratos_empenhos_despesas_empenhadas_saldo',
        bash_command='rm -r /tmp/dbt_test_not_null_silver_contratos_empenhos_despesas_empenhadas_saldo || true \
&& cp -r /opt/airflow/dags-config/repo/plugins/dbt /tmp/dbt_test_not_null_silver_contratos_empenhos_despesas_empenhadas_saldo \
&& cd /tmp/dbt_test_not_null_silver_contratos_empenhos_despesas_empenhadas_saldo \
&& dbt deps && dbt test --select not_null_silver_contratos_empenhos_despesas_empenhadas_saldo \
&& rm -r /tmp/dbt_test_not_null_silver_contratos_empenhos_despesas_empenhadas_saldo',
        env={
            'DBT_POSTGRES_HOST': Variable.get("ipea_dw_pg_host"),
            'DBT_POSTGRES_USER': Variable.get("ipea_dw_pg_user"),
            'DBT_POSTGRES_PASSWORD': Variable.get("ipea_dw_pg_password"),
            'DBT_POSTGRES_ENVIRONMENT': Variable.get("ipea_dw_pg_environment"),
            'DBT_POSTGRES_PORT': Variable.get("ipea_dw_pg_port"),
            'DBT_POSTGRES_DATABASE': Variable.get("ipea_dw_pg_db"),
        },
        append_env=True
    )

    not_null_silver_contratos_empenhos_despesas_empenhadas_movim_liquido_task = BashOperator(
        task_id='test_not_null_silver_contratos_empenhos_despesas_empenhadas_movim_liquido',
        bash_command='rm -r /tmp/dbt_test_not_null_silver_contratos_empenhos_despesas_empenhadas_movim_liquido || true \
&& cp -r /opt/airflow/dags-config/repo/plugins/dbt /tmp/dbt_test_not_null_silver_contratos_empenhos_despesas_empenhadas_movim_liquido \
&& cd /tmp/dbt_test_not_null_silver_contratos_empenhos_despesas_empenhadas_movim_liquido \
&& dbt deps && dbt test --select not_null_silver_contratos_empenhos_despesas_empenhadas_movim_liquido \
&& rm -r /tmp/dbt_test_not_null_silver_contratos_empenhos_despesas_empenhadas_movim_liquido',
        env={
            'DBT_POSTGRES_HOST': Variable.get("ipea_dw_pg_host"),
            'DBT_POSTGRES_USER': Variable.get("ipea_dw_pg_user"),
            'DBT_POSTGRES_PASSWORD': Variable.get("ipea_dw_pg_password"),
            'DBT_POSTGRES_ENVIRONMENT': Variable.get("ipea_dw_pg_environment"),
            'DBT_POSTGRES_PORT': Variable.get("ipea_dw_pg_port"),
            'DBT_POSTGRES_DATABASE': Variable.get("ipea_dw_pg_db"),
        },
        append_env=True
    )

    not_null_silver_contratos_empenhos_despesas_liquidadas_saldo_task = BashOperator(
        task_id='test_not_null_silver_contratos_empenhos_despesas_liquidadas_saldo',
        bash_command='rm -r /tmp/dbt_test_not_null_silver_contratos_empenhos_despesas_liquidadas_saldo || true \
&& cp -r /opt/airflow/dags-config/repo/plugins/dbt /tmp/dbt_test_not_null_silver_contratos_empenhos_despesas_liquidadas_saldo \
&& cd /tmp/dbt_test_not_null_silver_contratos_empenhos_despesas_liquidadas_saldo \
&& dbt deps && dbt test --select not_null_silver_contratos_empenhos_despesas_liquidadas_saldo \
&& rm -r /tmp/dbt_test_not_null_silver_contratos_empenhos_despesas_liquidadas_saldo',
        env={
            'DBT_POSTGRES_HOST': Variable.get("ipea_dw_pg_host"),
            'DBT_POSTGRES_USER': Variable.get("ipea_dw_pg_user"),
            'DBT_POSTGRES_PASSWORD': Variable.get("ipea_dw_pg_password"),
            'DBT_POSTGRES_ENVIRONMENT': Variable.get("ipea_dw_pg_environment"),
            'DBT_POSTGRES_PORT': Variable.get("ipea_dw_pg_port"),
            'DBT_POSTGRES_DATABASE': Variable.get("ipea_dw_pg_db"),
        },
        append_env=True
    )

    not_null_silver_contratos_empenhos_despesas_liquidadas_movim_liquido_task = BashOperator(
        task_id='test_not_null_silver_contratos_empenhos_despesas_liquidadas_movim_liquido',
        bash_command='rm -r /tmp/dbt_test_not_null_silver_contratos_empenhos_despesas_liquidadas_movim_liquido || true \
&& cp -r /opt/airflow/dags-config/repo/plugins/dbt /tmp/dbt_test_not_null_silver_contratos_empenhos_despesas_liquidadas_movim_liquido \
&& cd /tmp/dbt_test_not_null_silver_contratos_empenhos_despesas_liquidadas_movim_liquido \
&& dbt deps && dbt test --select not_null_silver_contratos_empenhos_despesas_liquidadas_movim_liquido \
&& rm -r /tmp/dbt_test_not_null_silver_contratos_empenhos_despesas_liquidadas_movim_liquido',
        env={
            'DBT_POSTGRES_HOST': Variable.get("ipea_dw_pg_host"),
            'DBT_POSTGRES_USER': Variable.get("ipea_dw_pg_user"),
            'DBT_POSTGRES_PASSWORD': Variable.get("ipea_dw_pg_password"),
            'DBT_POSTGRES_ENVIRONMENT': Variable.get("ipea_dw_pg_environment"),
            'DBT_POSTGRES_PORT': Variable.get("ipea_dw_pg_port"),
            'DBT_POSTGRES_DATABASE': Variable.get("ipea_dw_pg_db"),
        },
        append_env=True
    )

    not_null_silver_contratos_empenhos_despesas_pagas_saldo_task = BashOperator(
        task_id='test_not_null_silver_contratos_empenhos_despesas_pagas_saldo',
        bash_command='rm -r /tmp/dbt_test_not_null_silver_contratos_empenhos_despesas_pagas_saldo || true \
&& cp -r /opt/airflow/dags-config/repo/plugins/dbt /tmp/dbt_test_not_null_silver_contratos_empenhos_despesas_pagas_saldo \
&& cd /tmp/dbt_test_not_null_silver_contratos_empenhos_despesas_pagas_saldo \
&& dbt deps && dbt test --select not_null_silver_contratos_empenhos_despesas_pagas_saldo \
&& rm -r /tmp/dbt_test_not_null_silver_contratos_empenhos_despesas_pagas_saldo',
        env={
            'DBT_POSTGRES_HOST': Variable.get("ipea_dw_pg_host"),
            'DBT_POSTGRES_USER': Variable.get("ipea_dw_pg_user"),
            'DBT_POSTGRES_PASSWORD': Variable.get("ipea_dw_pg_password"),
            'DBT_POSTGRES_ENVIRONMENT': Variable.get("ipea_dw_pg_environment"),
            'DBT_POSTGRES_PORT': Variable.get("ipea_dw_pg_port"),
            'DBT_POSTGRES_DATABASE': Variable.get("ipea_dw_pg_db"),
        },
        append_env=True
    )

    not_null_silver_contratos_empenhos_despesas_pagas_movim_liquido_task = BashOperator(
        task_id='test_not_null_silver_contratos_empenhos_despesas_pagas_movim_liquido',
        bash_command='rm -r /tmp/dbt_test_not_null_silver_contratos_empenhos_despesas_pagas_movim_liquido || true \
&& cp -r /opt/airflow/dags-config/repo/plugins/dbt /tmp/dbt_test_not_null_silver_contratos_empenhos_despesas_pagas_movim_liquido \
&& cd /tmp/dbt_test_not_null_silver_contratos_empenhos_despesas_pagas_movim_liquido \
&& dbt deps && dbt test --select not_null_silver_contratos_empenhos_despesas_pagas_movim_liquido \
&& rm -r /tmp/dbt_test_not_null_silver_contratos_empenhos_despesas_pagas_movim_liquido',
        env={
            'DBT_POSTGRES_HOST': Variable.get("ipea_dw_pg_host"),
            'DBT_POSTGRES_USER': Variable.get("ipea_dw_pg_user"),
            'DBT_POSTGRES_PASSWORD': Variable.get("ipea_dw_pg_password"),
            'DBT_POSTGRES_ENVIRONMENT': Variable.get("ipea_dw_pg_environment"),
            'DBT_POSTGRES_PORT': Variable.get("ipea_dw_pg_port"),
            'DBT_POSTGRES_DATABASE': Variable.get("ipea_dw_pg_db"),
        },
        append_env=True
    )

    not_null_silver_contratos_empenhos_contrato_id_task = BashOperator(
        task_id='test_not_null_silver_contratos_empenhos_contrato_id',
        bash_command='rm -r /tmp/dbt_test_not_null_silver_contratos_empenhos_contrato_id || true \
&& cp -r /opt/airflow/dags-config/repo/plugins/dbt /tmp/dbt_test_not_null_silver_contratos_empenhos_contrato_id \
&& cd /tmp/dbt_test_not_null_silver_contratos_empenhos_contrato_id \
&& dbt deps && dbt test --select not_null_silver_contratos_empenhos_contrato_id \
&& rm -r /tmp/dbt_test_not_null_silver_contratos_empenhos_contrato_id',
        env={
            'DBT_POSTGRES_HOST': Variable.get("ipea_dw_pg_host"),
            'DBT_POSTGRES_USER': Variable.get("ipea_dw_pg_user"),
            'DBT_POSTGRES_PASSWORD': Variable.get("ipea_dw_pg_password"),
            'DBT_POSTGRES_ENVIRONMENT': Variable.get("ipea_dw_pg_environment"),
            'DBT_POSTGRES_PORT': Variable.get("ipea_dw_pg_port"),
            'DBT_POSTGRES_DATABASE': Variable.get("ipea_dw_pg_db"),
        },
        append_env=True
    )

    not_null_silver_contratos_empenhos_receita_despesa_task = BashOperator(
        task_id='test_not_null_silver_contratos_empenhos_receita_despesa',
        bash_command='rm -r /tmp/dbt_test_not_null_silver_contratos_empenhos_receita_despesa || true \
&& cp -r /opt/airflow/dags-config/repo/plugins/dbt /tmp/dbt_test_not_null_silver_contratos_empenhos_receita_despesa \
&& cd /tmp/dbt_test_not_null_silver_contratos_empenhos_receita_despesa \
&& dbt deps && dbt test --select not_null_silver_contratos_empenhos_receita_despesa \
&& rm -r /tmp/dbt_test_not_null_silver_contratos_empenhos_receita_despesa',
        env={
            'DBT_POSTGRES_HOST': Variable.get("ipea_dw_pg_host"),
            'DBT_POSTGRES_USER': Variable.get("ipea_dw_pg_user"),
            'DBT_POSTGRES_PASSWORD': Variable.get("ipea_dw_pg_password"),
            'DBT_POSTGRES_ENVIRONMENT': Variable.get("ipea_dw_pg_environment"),
            'DBT_POSTGRES_PORT': Variable.get("ipea_dw_pg_port"),
            'DBT_POSTGRES_DATABASE': Variable.get("ipea_dw_pg_db"),
        },
        append_env=True
    )

    not_null_silver_contratos_empenhos_numero_task = BashOperator(
        task_id='test_not_null_silver_contratos_empenhos_numero',
        bash_command='rm -r /tmp/dbt_test_not_null_silver_contratos_empenhos_numero || true \
&& cp -r /opt/airflow/dags-config/repo/plugins/dbt /tmp/dbt_test_not_null_silver_contratos_empenhos_numero \
&& cd /tmp/dbt_test_not_null_silver_contratos_empenhos_numero \
&& dbt deps && dbt test --select not_null_silver_contratos_empenhos_numero \
&& rm -r /tmp/dbt_test_not_null_silver_contratos_empenhos_numero',
        env={
            'DBT_POSTGRES_HOST': Variable.get("ipea_dw_pg_host"),
            'DBT_POSTGRES_USER': Variable.get("ipea_dw_pg_user"),
            'DBT_POSTGRES_PASSWORD': Variable.get("ipea_dw_pg_password"),
            'DBT_POSTGRES_ENVIRONMENT': Variable.get("ipea_dw_pg_environment"),
            'DBT_POSTGRES_PORT': Variable.get("ipea_dw_pg_port"),
            'DBT_POSTGRES_DATABASE': Variable.get("ipea_dw_pg_db"),
        },
        append_env=True
    )

    not_null_silver_contratos_empenhos_contratante_orgao_origem_codigo_task = BashOperator(
        task_id='test_not_null_silver_contratos_empenhos_contratante_orgao_origem_codigo',
        bash_command='rm -r /tmp/dbt_test_not_null_silver_contratos_empenhos_contratante_orgao_origem_codigo || true \
&& cp -r /opt/airflow/dags-config/repo/plugins/dbt /tmp/dbt_test_not_null_silver_contratos_empenhos_contratante_orgao_origem_codigo \
&& cd /tmp/dbt_test_not_null_silver_contratos_empenhos_contratante_orgao_origem_codigo \
&& dbt deps && dbt test --select not_null_silver_contratos_empenhos_contratante_orgao_origem_codigo \
&& rm -r /tmp/dbt_test_not_null_silver_contratos_empenhos_contratante_orgao_origem_codigo',
        env={
            'DBT_POSTGRES_HOST': Variable.get("ipea_dw_pg_host"),
            'DBT_POSTGRES_USER': Variable.get("ipea_dw_pg_user"),
            'DBT_POSTGRES_PASSWORD': Variable.get("ipea_dw_pg_password"),
            'DBT_POSTGRES_ENVIRONMENT': Variable.get("ipea_dw_pg_environment"),
            'DBT_POSTGRES_PORT': Variable.get("ipea_dw_pg_port"),
            'DBT_POSTGRES_DATABASE': Variable.get("ipea_dw_pg_db"),
        },
        append_env=True
    )

    not_null_silver_contratos_empenhos_contratante_orgao_origem_nome_task = BashOperator(
        task_id='test_not_null_silver_contratos_empenhos_contratante_orgao_origem_nome',
        bash_command='rm -r /tmp/dbt_test_not_null_silver_contratos_empenhos_contratante_orgao_origem_nome || true \
&& cp -r /opt/airflow/dags-config/repo/plugins/dbt /tmp/dbt_test_not_null_silver_contratos_empenhos_contratante_orgao_origem_nome \
&& cd /tmp/dbt_test_not_null_silver_contratos_empenhos_contratante_orgao_origem_nome \
&& dbt deps && dbt test --select not_null_silver_contratos_empenhos_contratante_orgao_origem_nome \
&& rm -r /tmp/dbt_test_not_null_silver_contratos_empenhos_contratante_orgao_origem_nome',
        env={
            'DBT_POSTGRES_HOST': Variable.get("ipea_dw_pg_host"),
            'DBT_POSTGRES_USER': Variable.get("ipea_dw_pg_user"),
            'DBT_POSTGRES_PASSWORD': Variable.get("ipea_dw_pg_password"),
            'DBT_POSTGRES_ENVIRONMENT': Variable.get("ipea_dw_pg_environment"),
            'DBT_POSTGRES_PORT': Variable.get("ipea_dw_pg_port"),
            'DBT_POSTGRES_DATABASE': Variable.get("ipea_dw_pg_db"),
        },
        append_env=True
    )

    not_null_silver_contratos_empenhos_contratante_orgao_origem_unidade_gestora_origem_codigo_task = BashOperator(
        task_id='test_not_null_silver_contratos_empenhos_contratante_orgao_origem_unidade_gestora_origem_codigo',
        bash_command='rm -r /tmp/dbt_test_not_null_silver_contratos_empenhos_contratante_orgao_origem_unidade_gestora_origem_codigo || true \
&& cp -r /opt/airflow/dags-config/repo/plugins/dbt /tmp/dbt_test_not_null_silver_contratos_empenhos_contratante_orgao_origem_unidade_gestora_origem_codigo \
&& cd /tmp/dbt_test_not_null_silver_contratos_empenhos_contratante_orgao_origem_unidade_gestora_origem_codigo \
&& dbt deps && dbt test --select not_null_silver_contratos_empenhos_contratante_orgao_origem_unidade_gestora_origem_codigo \
&& rm -r /tmp/dbt_test_not_null_silver_contratos_empenhos_contratante_orgao_origem_unidade_gestora_origem_codigo',
        env={
            'DBT_POSTGRES_HOST': Variable.get("ipea_dw_pg_host"),
            'DBT_POSTGRES_USER': Variable.get("ipea_dw_pg_user"),
            'DBT_POSTGRES_PASSWORD': Variable.get("ipea_dw_pg_password"),
            'DBT_POSTGRES_ENVIRONMENT': Variable.get("ipea_dw_pg_environment"),
            'DBT_POSTGRES_PORT': Variable.get("ipea_dw_pg_port"),
            'DBT_POSTGRES_DATABASE': Variable.get("ipea_dw_pg_db"),
        },
        append_env=True
    )

    not_null_silver_contratos_empenhos_contratante_orgao_origem_unidade_gestora_origem_nome_resumido_task = BashOperator(
        task_id='test_not_null_silver_contratos_empenhos_contratante_orgao_origem_unidade_gestora_origem_nome_resumido',
        bash_command='rm -r /tmp/dbt_test_not_null_silver_contratos_empenhos_contratante_orgao_origem_unidade_gestora_origem_nome_resumido || true \
&& cp -r /opt/airflow/dags-config/repo/plugins/dbt /tmp/dbt_test_not_null_silver_contratos_empenhos_contratante_orgao_origem_unidade_gestora_origem_nome_resumido \
&& cd /tmp/dbt_test_not_null_silver_contratos_empenhos_contratante_orgao_origem_unidade_gestora_origem_nome_resumido \
&& dbt deps && dbt test --select not_null_silver_contratos_empenhos_contratante_orgao_origem_unidade_gestora_origem_nome_resumido \
&& rm -r /tmp/dbt_test_not_null_silver_contratos_empenhos_contratante_orgao_origem_unidade_gestora_origem_nome_resumido',
        env={
            'DBT_POSTGRES_HOST': Variable.get("ipea_dw_pg_host"),
            'DBT_POSTGRES_USER': Variable.get("ipea_dw_pg_user"),
            'DBT_POSTGRES_PASSWORD': Variable.get("ipea_dw_pg_password"),
            'DBT_POSTGRES_ENVIRONMENT': Variable.get("ipea_dw_pg_environment"),
            'DBT_POSTGRES_PORT': Variable.get("ipea_dw_pg_port"),
            'DBT_POSTGRES_DATABASE': Variable.get("ipea_dw_pg_db"),
        },
        append_env=True
    )

    not_null_silver_contratos_empenhos_contratante_orgao_origem_unidade_gestora_origem_nome_task = BashOperator(
        task_id='test_not_null_silver_contratos_empenhos_contratante_orgao_origem_unidade_gestora_origem_nome',
        bash_command='rm -r /tmp/dbt_test_not_null_silver_contratos_empenhos_contratante_orgao_origem_unidade_gestora_origem_nome || true \
&& cp -r /opt/airflow/dags-config/repo/plugins/dbt /tmp/dbt_test_not_null_silver_contratos_empenhos_contratante_orgao_origem_unidade_gestora_origem_nome \
&& cd /tmp/dbt_test_not_null_silver_contratos_empenhos_contratante_orgao_origem_unidade_gestora_origem_nome \
&& dbt deps && dbt test --select not_null_silver_contratos_empenhos_contratante_orgao_origem_unidade_gestora_origem_nome \
&& rm -r /tmp/dbt_test_not_null_silver_contratos_empenhos_contratante_orgao_origem_unidade_gestora_origem_nome',
        env={
            'DBT_POSTGRES_HOST': Variable.get("ipea_dw_pg_host"),
            'DBT_POSTGRES_USER': Variable.get("ipea_dw_pg_user"),
            'DBT_POSTGRES_PASSWORD': Variable.get("ipea_dw_pg_password"),
            'DBT_POSTGRES_ENVIRONMENT': Variable.get("ipea_dw_pg_environment"),
            'DBT_POSTGRES_PORT': Variable.get("ipea_dw_pg_port"),
            'DBT_POSTGRES_DATABASE': Variable.get("ipea_dw_pg_db"),
        },
        append_env=True
    )

    not_null_silver_contratos_empenhos_contratante_orgao_origem_unidade_gestora_origem_sisg_task = BashOperator(
        task_id='test_not_null_silver_contratos_empenhos_contratante_orgao_origem_unidade_gestora_origem_sisg',
        bash_command='rm -r /tmp/dbt_test_not_null_silver_contratos_empenhos_contratante_orgao_origem_unidade_gestora_origem_sisg || true \
&& cp -r /opt/airflow/dags-config/repo/plugins/dbt /tmp/dbt_test_not_null_silver_contratos_empenhos_contratante_orgao_origem_unidade_gestora_origem_sisg \
&& cd /tmp/dbt_test_not_null_silver_contratos_empenhos_contratante_orgao_origem_unidade_gestora_origem_sisg \
&& dbt deps && dbt test --select not_null_silver_contratos_empenhos_contratante_orgao_origem_unidade_gestora_origem_sisg \
&& rm -r /tmp/dbt_test_not_null_silver_contratos_empenhos_contratante_orgao_origem_unidade_gestora_origem_sisg',
        env={
            'DBT_POSTGRES_HOST': Variable.get("ipea_dw_pg_host"),
            'DBT_POSTGRES_USER': Variable.get("ipea_dw_pg_user"),
            'DBT_POSTGRES_PASSWORD': Variable.get("ipea_dw_pg_password"),
            'DBT_POSTGRES_ENVIRONMENT': Variable.get("ipea_dw_pg_environment"),
            'DBT_POSTGRES_PORT': Variable.get("ipea_dw_pg_port"),
            'DBT_POSTGRES_DATABASE': Variable.get("ipea_dw_pg_db"),
        },
        append_env=True
    )

    not_null_silver_contratos_empenhos_contratante_orgao_origem_unidade_gestora_origem_utiliza_siafi_task = BashOperator(
        task_id='test_not_null_silver_contratos_empenhos_contratante_orgao_origem_unidade_gestora_origem_utiliza_siafi',
        bash_command='rm -r /tmp/dbt_test_not_null_silver_contratos_empenhos_contratante_orgao_origem_unidade_gestora_origem_utiliza_siafi || true \
&& cp -r /opt/airflow/dags-config/repo/plugins/dbt /tmp/dbt_test_not_null_silver_contratos_empenhos_contratante_orgao_origem_unidade_gestora_origem_utiliza_siafi \
&& cd /tmp/dbt_test_not_null_silver_contratos_empenhos_contratante_orgao_origem_unidade_gestora_origem_utiliza_siafi \
&& dbt deps && dbt test --select not_null_silver_contratos_empenhos_contratante_orgao_origem_unidade_gestora_origem_utiliza_siafi \
&& rm -r /tmp/dbt_test_not_null_silver_contratos_empenhos_contratante_orgao_origem_unidade_gestora_origem_utiliza_siafi',
        env={
            'DBT_POSTGRES_HOST': Variable.get("ipea_dw_pg_host"),
            'DBT_POSTGRES_USER': Variable.get("ipea_dw_pg_user"),
            'DBT_POSTGRES_PASSWORD': Variable.get("ipea_dw_pg_password"),
            'DBT_POSTGRES_ENVIRONMENT': Variable.get("ipea_dw_pg_environment"),
            'DBT_POSTGRES_PORT': Variable.get("ipea_dw_pg_port"),
            'DBT_POSTGRES_DATABASE': Variable.get("ipea_dw_pg_db"),
        },
        append_env=True
    )

    not_null_silver_contratos_empenhos_contratante_orgao_origem_unidade_gestora_origem_utiliza_antecip_task = BashOperator(
        task_id='test_not_null_silver_contratos_empenhos_contratante_orgao_origem_unidade_gestora_origem_utiliza_antecip',
        bash_command='rm -r /tmp/dbt_test_not_null_silver_contratos_empenhos_contratante_orgao_origem_unidade_gestora_origem_utiliza_antecip || true \
&& cp -r /opt/airflow/dags-config/repo/plugins/dbt /tmp/dbt_test_not_null_silver_contratos_empenhos_contratante_orgao_origem_unidade_gestora_origem_utiliza_antecip \
&& cd /tmp/dbt_test_not_null_silver_contratos_empenhos_contratante_orgao_origem_unidade_gestora_origem_utiliza_antecip \
&& dbt deps && dbt test --select not_null_silver_contratos_empenhos_contratante_orgao_origem_unidade_gestora_origem_utiliza_antecip \
&& rm -r /tmp/dbt_test_not_null_silver_contratos_empenhos_contratante_orgao_origem_unidade_gestora_origem_utiliza_antecip',
        env={
            'DBT_POSTGRES_HOST': Variable.get("ipea_dw_pg_host"),
            'DBT_POSTGRES_USER': Variable.get("ipea_dw_pg_user"),
            'DBT_POSTGRES_PASSWORD': Variable.get("ipea_dw_pg_password"),
            'DBT_POSTGRES_ENVIRONMENT': Variable.get("ipea_dw_pg_environment"),
            'DBT_POSTGRES_PORT': Variable.get("ipea_dw_pg_port"),
            'DBT_POSTGRES_DATABASE': Variable.get("ipea_dw_pg_db"),
        },
        append_env=True
    )

    not_null_silver_contratos_empenhos_contratante_orgao_codigo_task = BashOperator(
        task_id='test_not_null_silver_contratos_empenhos_contratante_orgao_codigo',
        bash_command='rm -r /tmp/dbt_test_not_null_silver_contratos_empenhos_contratante_orgao_codigo || true \
&& cp -r /opt/airflow/dags-config/repo/plugins/dbt /tmp/dbt_test_not_null_silver_contratos_empenhos_contratante_orgao_codigo \
&& cd /tmp/dbt_test_not_null_silver_contratos_empenhos_contratante_orgao_codigo \
&& dbt deps && dbt test --select not_null_silver_contratos_empenhos_contratante_orgao_codigo \
&& rm -r /tmp/dbt_test_not_null_silver_contratos_empenhos_contratante_orgao_codigo',
        env={
            'DBT_POSTGRES_HOST': Variable.get("ipea_dw_pg_host"),
            'DBT_POSTGRES_USER': Variable.get("ipea_dw_pg_user"),
            'DBT_POSTGRES_PASSWORD': Variable.get("ipea_dw_pg_password"),
            'DBT_POSTGRES_ENVIRONMENT': Variable.get("ipea_dw_pg_environment"),
            'DBT_POSTGRES_PORT': Variable.get("ipea_dw_pg_port"),
            'DBT_POSTGRES_DATABASE': Variable.get("ipea_dw_pg_db"),
        },
        append_env=True
    )

    not_null_silver_contratos_empenhos_contratante_orgao_nome_task = BashOperator(
        task_id='test_not_null_silver_contratos_empenhos_contratante_orgao_nome',
        bash_command='rm -r /tmp/dbt_test_not_null_silver_contratos_empenhos_contratante_orgao_nome || true \
&& cp -r /opt/airflow/dags-config/repo/plugins/dbt /tmp/dbt_test_not_null_silver_contratos_empenhos_contratante_orgao_nome \
&& cd /tmp/dbt_test_not_null_silver_contratos_empenhos_contratante_orgao_nome \
&& dbt deps && dbt test --select not_null_silver_contratos_empenhos_contratante_orgao_nome \
&& rm -r /tmp/dbt_test_not_null_silver_contratos_empenhos_contratante_orgao_nome',
        env={
            'DBT_POSTGRES_HOST': Variable.get("ipea_dw_pg_host"),
            'DBT_POSTGRES_USER': Variable.get("ipea_dw_pg_user"),
            'DBT_POSTGRES_PASSWORD': Variable.get("ipea_dw_pg_password"),
            'DBT_POSTGRES_ENVIRONMENT': Variable.get("ipea_dw_pg_environment"),
            'DBT_POSTGRES_PORT': Variable.get("ipea_dw_pg_port"),
            'DBT_POSTGRES_DATABASE': Variable.get("ipea_dw_pg_db"),
        },
        append_env=True
    )

    not_null_silver_contratos_empenhos_contratante_orgao_unidade_gestora_codigo_task = BashOperator(
        task_id='test_not_null_silver_contratos_empenhos_contratante_orgao_unidade_gestora_codigo',
        bash_command='rm -r /tmp/dbt_test_not_null_silver_contratos_empenhos_contratante_orgao_unidade_gestora_codigo || true \
&& cp -r /opt/airflow/dags-config/repo/plugins/dbt /tmp/dbt_test_not_null_silver_contratos_empenhos_contratante_orgao_unidade_gestora_codigo \
&& cd /tmp/dbt_test_not_null_silver_contratos_empenhos_contratante_orgao_unidade_gestora_codigo \
&& dbt deps && dbt test --select not_null_silver_contratos_empenhos_contratante_orgao_unidade_gestora_codigo \
&& rm -r /tmp/dbt_test_not_null_silver_contratos_empenhos_contratante_orgao_unidade_gestora_codigo',
        env={
            'DBT_POSTGRES_HOST': Variable.get("ipea_dw_pg_host"),
            'DBT_POSTGRES_USER': Variable.get("ipea_dw_pg_user"),
            'DBT_POSTGRES_PASSWORD': Variable.get("ipea_dw_pg_password"),
            'DBT_POSTGRES_ENVIRONMENT': Variable.get("ipea_dw_pg_environment"),
            'DBT_POSTGRES_PORT': Variable.get("ipea_dw_pg_port"),
            'DBT_POSTGRES_DATABASE': Variable.get("ipea_dw_pg_db"),
        },
        append_env=True
    )

    not_null_silver_contratos_empenhos_contratante_orgao_unidade_gestora_nome_resumido_task = BashOperator(
        task_id='test_not_null_silver_contratos_empenhos_contratante_orgao_unidade_gestora_nome_resumido',
        bash_command='rm -r /tmp/dbt_test_not_null_silver_contratos_empenhos_contratante_orgao_unidade_gestora_nome_resumido || true \
&& cp -r /opt/airflow/dags-config/repo/plugins/dbt /tmp/dbt_test_not_null_silver_contratos_empenhos_contratante_orgao_unidade_gestora_nome_resumido \
&& cd /tmp/dbt_test_not_null_silver_contratos_empenhos_contratante_orgao_unidade_gestora_nome_resumido \
&& dbt deps && dbt test --select not_null_silver_contratos_empenhos_contratante_orgao_unidade_gestora_nome_resumido \
&& rm -r /tmp/dbt_test_not_null_silver_contratos_empenhos_contratante_orgao_unidade_gestora_nome_resumido',
        env={
            'DBT_POSTGRES_HOST': Variable.get("ipea_dw_pg_host"),
            'DBT_POSTGRES_USER': Variable.get("ipea_dw_pg_user"),
            'DBT_POSTGRES_PASSWORD': Variable.get("ipea_dw_pg_password"),
            'DBT_POSTGRES_ENVIRONMENT': Variable.get("ipea_dw_pg_environment"),
            'DBT_POSTGRES_PORT': Variable.get("ipea_dw_pg_port"),
            'DBT_POSTGRES_DATABASE': Variable.get("ipea_dw_pg_db"),
        },
        append_env=True
    )

    not_null_silver_contratos_empenhos_contratante_orgao_unidade_gestora_nome_task = BashOperator(
        task_id='test_not_null_silver_contratos_empenhos_contratante_orgao_unidade_gestora_nome',
        bash_command='rm -r /tmp/dbt_test_not_null_silver_contratos_empenhos_contratante_orgao_unidade_gestora_nome || true \
&& cp -r /opt/airflow/dags-config/repo/plugins/dbt /tmp/dbt_test_not_null_silver_contratos_empenhos_contratante_orgao_unidade_gestora_nome \
&& cd /tmp/dbt_test_not_null_silver_contratos_empenhos_contratante_orgao_unidade_gestora_nome \
&& dbt deps && dbt test --select not_null_silver_contratos_empenhos_contratante_orgao_unidade_gestora_nome \
&& rm -r /tmp/dbt_test_not_null_silver_contratos_empenhos_contratante_orgao_unidade_gestora_nome',
        env={
            'DBT_POSTGRES_HOST': Variable.get("ipea_dw_pg_host"),
            'DBT_POSTGRES_USER': Variable.get("ipea_dw_pg_user"),
            'DBT_POSTGRES_PASSWORD': Variable.get("ipea_dw_pg_password"),
            'DBT_POSTGRES_ENVIRONMENT': Variable.get("ipea_dw_pg_environment"),
            'DBT_POSTGRES_PORT': Variable.get("ipea_dw_pg_port"),
            'DBT_POSTGRES_DATABASE': Variable.get("ipea_dw_pg_db"),
        },
        append_env=True
    )

    not_null_silver_contratos_empenhos_contratante_orgao_unidade_gestora_sisg_task = BashOperator(
        task_id='test_not_null_silver_contratos_empenhos_contratante_orgao_unidade_gestora_sisg',
        bash_command='rm -r /tmp/dbt_test_not_null_silver_contratos_empenhos_contratante_orgao_unidade_gestora_sisg || true \
&& cp -r /opt/airflow/dags-config/repo/plugins/dbt /tmp/dbt_test_not_null_silver_contratos_empenhos_contratante_orgao_unidade_gestora_sisg \
&& cd /tmp/dbt_test_not_null_silver_contratos_empenhos_contratante_orgao_unidade_gestora_sisg \
&& dbt deps && dbt test --select not_null_silver_contratos_empenhos_contratante_orgao_unidade_gestora_sisg \
&& rm -r /tmp/dbt_test_not_null_silver_contratos_empenhos_contratante_orgao_unidade_gestora_sisg',
        env={
            'DBT_POSTGRES_HOST': Variable.get("ipea_dw_pg_host"),
            'DBT_POSTGRES_USER': Variable.get("ipea_dw_pg_user"),
            'DBT_POSTGRES_PASSWORD': Variable.get("ipea_dw_pg_password"),
            'DBT_POSTGRES_ENVIRONMENT': Variable.get("ipea_dw_pg_environment"),
            'DBT_POSTGRES_PORT': Variable.get("ipea_dw_pg_port"),
            'DBT_POSTGRES_DATABASE': Variable.get("ipea_dw_pg_db"),
        },
        append_env=True
    )

    not_null_silver_contratos_empenhos_contratante_orgao_unidade_gestora_utiliza_siafi_task = BashOperator(
        task_id='test_not_null_silver_contratos_empenhos_contratante_orgao_unidade_gestora_utiliza_siafi',
        bash_command='rm -r /tmp/dbt_test_not_null_silver_contratos_empenhos_contratante_orgao_unidade_gestora_utiliza_siafi || true \
&& cp -r /opt/airflow/dags-config/repo/plugins/dbt /tmp/dbt_test_not_null_silver_contratos_empenhos_contratante_orgao_unidade_gestora_utiliza_siafi \
&& cd /tmp/dbt_test_not_null_silver_contratos_empenhos_contratante_orgao_unidade_gestora_utiliza_siafi \
&& dbt deps && dbt test --select not_null_silver_contratos_empenhos_contratante_orgao_unidade_gestora_utiliza_siafi \
&& rm -r /tmp/dbt_test_not_null_silver_contratos_empenhos_contratante_orgao_unidade_gestora_utiliza_siafi',
        env={
            'DBT_POSTGRES_HOST': Variable.get("ipea_dw_pg_host"),
            'DBT_POSTGRES_USER': Variable.get("ipea_dw_pg_user"),
            'DBT_POSTGRES_PASSWORD': Variable.get("ipea_dw_pg_password"),
            'DBT_POSTGRES_ENVIRONMENT': Variable.get("ipea_dw_pg_environment"),
            'DBT_POSTGRES_PORT': Variable.get("ipea_dw_pg_port"),
            'DBT_POSTGRES_DATABASE': Variable.get("ipea_dw_pg_db"),
        },
        append_env=True
    )

    not_null_silver_contratos_empenhos_contratante_orgao_unidade_gestora_utiliza_antecipagov_task = BashOperator(
        task_id='test_not_null_silver_contratos_empenhos_contratante_orgao_unidade_gestora_utiliza_antecipagov',
        bash_command='rm -r /tmp/dbt_test_not_null_silver_contratos_empenhos_contratante_orgao_unidade_gestora_utiliza_antecipagov || true \
&& cp -r /opt/airflow/dags-config/repo/plugins/dbt /tmp/dbt_test_not_null_silver_contratos_empenhos_contratante_orgao_unidade_gestora_utiliza_antecipagov \
&& cd /tmp/dbt_test_not_null_silver_contratos_empenhos_contratante_orgao_unidade_gestora_utiliza_antecipagov \
&& dbt deps && dbt test --select not_null_silver_contratos_empenhos_contratante_orgao_unidade_gestora_utiliza_antecipagov \
&& rm -r /tmp/dbt_test_not_null_silver_contratos_empenhos_contratante_orgao_unidade_gestora_utiliza_antecipagov',
        env={
            'DBT_POSTGRES_HOST': Variable.get("ipea_dw_pg_host"),
            'DBT_POSTGRES_USER': Variable.get("ipea_dw_pg_user"),
            'DBT_POSTGRES_PASSWORD': Variable.get("ipea_dw_pg_password"),
            'DBT_POSTGRES_ENVIRONMENT': Variable.get("ipea_dw_pg_environment"),
            'DBT_POSTGRES_PORT': Variable.get("ipea_dw_pg_port"),
            'DBT_POSTGRES_DATABASE': Variable.get("ipea_dw_pg_db"),
        },
        append_env=True
    )

    not_null_silver_contratos_empenhos_fornecedor_tipo_task = BashOperator(
        task_id='test_not_null_silver_contratos_empenhos_fornecedor_tipo',
        bash_command='rm -r /tmp/dbt_test_not_null_silver_contratos_empenhos_fornecedor_tipo || true \
&& cp -r /opt/airflow/dags-config/repo/plugins/dbt /tmp/dbt_test_not_null_silver_contratos_empenhos_fornecedor_tipo \
&& cd /tmp/dbt_test_not_null_silver_contratos_empenhos_fornecedor_tipo \
&& dbt deps && dbt test --select not_null_silver_contratos_empenhos_fornecedor_tipo \
&& rm -r /tmp/dbt_test_not_null_silver_contratos_empenhos_fornecedor_tipo',
        env={
            'DBT_POSTGRES_HOST': Variable.get("ipea_dw_pg_host"),
            'DBT_POSTGRES_USER': Variable.get("ipea_dw_pg_user"),
            'DBT_POSTGRES_PASSWORD': Variable.get("ipea_dw_pg_password"),
            'DBT_POSTGRES_ENVIRONMENT': Variable.get("ipea_dw_pg_environment"),
            'DBT_POSTGRES_PORT': Variable.get("ipea_dw_pg_port"),
            'DBT_POSTGRES_DATABASE': Variable.get("ipea_dw_pg_db"),
        },
        append_env=True
    )

    not_null_silver_contratos_empenhos_fornecedor_cnpj_cpf_idgener_task = BashOperator(
        task_id='test_not_null_silver_contratos_empenhos_fornecedor_cnpj_cpf_idgener',
        bash_command='rm -r /tmp/dbt_test_not_null_silver_contratos_empenhos_fornecedor_cnpj_cpf_idgener || true \
&& cp -r /opt/airflow/dags-config/repo/plugins/dbt /tmp/dbt_test_not_null_silver_contratos_empenhos_fornecedor_cnpj_cpf_idgener \
&& cd /tmp/dbt_test_not_null_silver_contratos_empenhos_fornecedor_cnpj_cpf_idgener \
&& dbt deps && dbt test --select not_null_silver_contratos_empenhos_fornecedor_cnpj_cpf_idgener \
&& rm -r /tmp/dbt_test_not_null_silver_contratos_empenhos_fornecedor_cnpj_cpf_idgener',
        env={
            'DBT_POSTGRES_HOST': Variable.get("ipea_dw_pg_host"),
            'DBT_POSTGRES_USER': Variable.get("ipea_dw_pg_user"),
            'DBT_POSTGRES_PASSWORD': Variable.get("ipea_dw_pg_password"),
            'DBT_POSTGRES_ENVIRONMENT': Variable.get("ipea_dw_pg_environment"),
            'DBT_POSTGRES_PORT': Variable.get("ipea_dw_pg_port"),
            'DBT_POSTGRES_DATABASE': Variable.get("ipea_dw_pg_db"),
        },
        append_env=True
    )

    not_null_silver_contratos_empenhos_fornecedor_nome_task = BashOperator(
        task_id='test_not_null_silver_contratos_empenhos_fornecedor_nome',
        bash_command='rm -r /tmp/dbt_test_not_null_silver_contratos_empenhos_fornecedor_nome || true \
&& cp -r /opt/airflow/dags-config/repo/plugins/dbt /tmp/dbt_test_not_null_silver_contratos_empenhos_fornecedor_nome \
&& cd /tmp/dbt_test_not_null_silver_contratos_empenhos_fornecedor_nome \
&& dbt deps && dbt test --select not_null_silver_contratos_empenhos_fornecedor_nome \
&& rm -r /tmp/dbt_test_not_null_silver_contratos_empenhos_fornecedor_nome',
        env={
            'DBT_POSTGRES_HOST': Variable.get("ipea_dw_pg_host"),
            'DBT_POSTGRES_USER': Variable.get("ipea_dw_pg_user"),
            'DBT_POSTGRES_PASSWORD': Variable.get("ipea_dw_pg_password"),
            'DBT_POSTGRES_ENVIRONMENT': Variable.get("ipea_dw_pg_environment"),
            'DBT_POSTGRES_PORT': Variable.get("ipea_dw_pg_port"),
            'DBT_POSTGRES_DATABASE': Variable.get("ipea_dw_pg_db"),
        },
        append_env=True
    )

    not_null_silver_contratos_empenhos_codigo_tipo_task = BashOperator(
        task_id='test_not_null_silver_contratos_empenhos_codigo_tipo',
        bash_command='rm -r /tmp/dbt_test_not_null_silver_contratos_empenhos_codigo_tipo || true \
&& cp -r /opt/airflow/dags-config/repo/plugins/dbt /tmp/dbt_test_not_null_silver_contratos_empenhos_codigo_tipo \
&& cd /tmp/dbt_test_not_null_silver_contratos_empenhos_codigo_tipo \
&& dbt deps && dbt test --select not_null_silver_contratos_empenhos_codigo_tipo \
&& rm -r /tmp/dbt_test_not_null_silver_contratos_empenhos_codigo_tipo',
        env={
            'DBT_POSTGRES_HOST': Variable.get("ipea_dw_pg_host"),
            'DBT_POSTGRES_USER': Variable.get("ipea_dw_pg_user"),
            'DBT_POSTGRES_PASSWORD': Variable.get("ipea_dw_pg_password"),
            'DBT_POSTGRES_ENVIRONMENT': Variable.get("ipea_dw_pg_environment"),
            'DBT_POSTGRES_PORT': Variable.get("ipea_dw_pg_port"),
            'DBT_POSTGRES_DATABASE': Variable.get("ipea_dw_pg_db"),
        },
        append_env=True
    )

    not_null_silver_contratos_empenhos_tipo_task = BashOperator(
        task_id='test_not_null_silver_contratos_empenhos_tipo',
        bash_command='rm -r /tmp/dbt_test_not_null_silver_contratos_empenhos_tipo || true \
&& cp -r /opt/airflow/dags-config/repo/plugins/dbt /tmp/dbt_test_not_null_silver_contratos_empenhos_tipo \
&& cd /tmp/dbt_test_not_null_silver_contratos_empenhos_tipo \
&& dbt deps && dbt test --select not_null_silver_contratos_empenhos_tipo \
&& rm -r /tmp/dbt_test_not_null_silver_contratos_empenhos_tipo',
        env={
            'DBT_POSTGRES_HOST': Variable.get("ipea_dw_pg_host"),
            'DBT_POSTGRES_USER': Variable.get("ipea_dw_pg_user"),
            'DBT_POSTGRES_PASSWORD': Variable.get("ipea_dw_pg_password"),
            'DBT_POSTGRES_ENVIRONMENT': Variable.get("ipea_dw_pg_environment"),
            'DBT_POSTGRES_PORT': Variable.get("ipea_dw_pg_port"),
            'DBT_POSTGRES_DATABASE': Variable.get("ipea_dw_pg_db"),
        },
        append_env=True
    )

    not_null_silver_contratos_empenhos_subtipo_task = BashOperator(
        task_id='test_not_null_silver_contratos_empenhos_subtipo',
        bash_command='rm -r /tmp/dbt_test_not_null_silver_contratos_empenhos_subtipo || true \
&& cp -r /opt/airflow/dags-config/repo/plugins/dbt /tmp/dbt_test_not_null_silver_contratos_empenhos_subtipo \
&& cd /tmp/dbt_test_not_null_silver_contratos_empenhos_subtipo \
&& dbt deps && dbt test --select not_null_silver_contratos_empenhos_subtipo \
&& rm -r /tmp/dbt_test_not_null_silver_contratos_empenhos_subtipo',
        env={
            'DBT_POSTGRES_HOST': Variable.get("ipea_dw_pg_host"),
            'DBT_POSTGRES_USER': Variable.get("ipea_dw_pg_user"),
            'DBT_POSTGRES_PASSWORD': Variable.get("ipea_dw_pg_password"),
            'DBT_POSTGRES_ENVIRONMENT': Variable.get("ipea_dw_pg_environment"),
            'DBT_POSTGRES_PORT': Variable.get("ipea_dw_pg_port"),
            'DBT_POSTGRES_DATABASE': Variable.get("ipea_dw_pg_db"),
        },
        append_env=True
    )

    not_null_silver_contratos_empenhos_prorrogavel_task = BashOperator(
        task_id='test_not_null_silver_contratos_empenhos_prorrogavel',
        bash_command='rm -r /tmp/dbt_test_not_null_silver_contratos_empenhos_prorrogavel || true \
&& cp -r /opt/airflow/dags-config/repo/plugins/dbt /tmp/dbt_test_not_null_silver_contratos_empenhos_prorrogavel \
&& cd /tmp/dbt_test_not_null_silver_contratos_empenhos_prorrogavel \
&& dbt deps && dbt test --select not_null_silver_contratos_empenhos_prorrogavel \
&& rm -r /tmp/dbt_test_not_null_silver_contratos_empenhos_prorrogavel',
        env={
            'DBT_POSTGRES_HOST': Variable.get("ipea_dw_pg_host"),
            'DBT_POSTGRES_USER': Variable.get("ipea_dw_pg_user"),
            'DBT_POSTGRES_PASSWORD': Variable.get("ipea_dw_pg_password"),
            'DBT_POSTGRES_ENVIRONMENT': Variable.get("ipea_dw_pg_environment"),
            'DBT_POSTGRES_PORT': Variable.get("ipea_dw_pg_port"),
            'DBT_POSTGRES_DATABASE': Variable.get("ipea_dw_pg_db"),
        },
        append_env=True
    )

    not_null_silver_contratos_empenhos_situacao_task = BashOperator(
        task_id='test_not_null_silver_contratos_empenhos_situacao',
        bash_command='rm -r /tmp/dbt_test_not_null_silver_contratos_empenhos_situacao || true \
&& cp -r /opt/airflow/dags-config/repo/plugins/dbt /tmp/dbt_test_not_null_silver_contratos_empenhos_situacao \
&& cd /tmp/dbt_test_not_null_silver_contratos_empenhos_situacao \
&& dbt deps && dbt test --select not_null_silver_contratos_empenhos_situacao \
&& rm -r /tmp/dbt_test_not_null_silver_contratos_empenhos_situacao',
        env={
            'DBT_POSTGRES_HOST': Variable.get("ipea_dw_pg_host"),
            'DBT_POSTGRES_USER': Variable.get("ipea_dw_pg_user"),
            'DBT_POSTGRES_PASSWORD': Variable.get("ipea_dw_pg_password"),
            'DBT_POSTGRES_ENVIRONMENT': Variable.get("ipea_dw_pg_environment"),
            'DBT_POSTGRES_PORT': Variable.get("ipea_dw_pg_port"),
            'DBT_POSTGRES_DATABASE': Variable.get("ipea_dw_pg_db"),
        },
        append_env=True
    )

    not_null_silver_contratos_empenhos_justificativa_inativo_task = BashOperator(
        task_id='test_not_null_silver_contratos_empenhos_justificativa_inativo',
        bash_command='rm -r /tmp/dbt_test_not_null_silver_contratos_empenhos_justificativa_inativo || true \
&& cp -r /opt/airflow/dags-config/repo/plugins/dbt /tmp/dbt_test_not_null_silver_contratos_empenhos_justificativa_inativo \
&& cd /tmp/dbt_test_not_null_silver_contratos_empenhos_justificativa_inativo \
&& dbt deps && dbt test --select not_null_silver_contratos_empenhos_justificativa_inativo \
&& rm -r /tmp/dbt_test_not_null_silver_contratos_empenhos_justificativa_inativo',
        env={
            'DBT_POSTGRES_HOST': Variable.get("ipea_dw_pg_host"),
            'DBT_POSTGRES_USER': Variable.get("ipea_dw_pg_user"),
            'DBT_POSTGRES_PASSWORD': Variable.get("ipea_dw_pg_password"),
            'DBT_POSTGRES_ENVIRONMENT': Variable.get("ipea_dw_pg_environment"),
            'DBT_POSTGRES_PORT': Variable.get("ipea_dw_pg_port"),
            'DBT_POSTGRES_DATABASE': Variable.get("ipea_dw_pg_db"),
        },
        append_env=True
    )

    not_null_silver_contratos_empenhos_categoria_task = BashOperator(
        task_id='test_not_null_silver_contratos_empenhos_categoria',
        bash_command='rm -r /tmp/dbt_test_not_null_silver_contratos_empenhos_categoria || true \
&& cp -r /opt/airflow/dags-config/repo/plugins/dbt /tmp/dbt_test_not_null_silver_contratos_empenhos_categoria \
&& cd /tmp/dbt_test_not_null_silver_contratos_empenhos_categoria \
&& dbt deps && dbt test --select not_null_silver_contratos_empenhos_categoria \
&& rm -r /tmp/dbt_test_not_null_silver_contratos_empenhos_categoria',
        env={
            'DBT_POSTGRES_HOST': Variable.get("ipea_dw_pg_host"),
            'DBT_POSTGRES_USER': Variable.get("ipea_dw_pg_user"),
            'DBT_POSTGRES_PASSWORD': Variable.get("ipea_dw_pg_password"),
            'DBT_POSTGRES_ENVIRONMENT': Variable.get("ipea_dw_pg_environment"),
            'DBT_POSTGRES_PORT': Variable.get("ipea_dw_pg_port"),
            'DBT_POSTGRES_DATABASE': Variable.get("ipea_dw_pg_db"),
        },
        append_env=True
    )

    not_null_silver_contratos_empenhos_subcategoria_task = BashOperator(
        task_id='test_not_null_silver_contratos_empenhos_subcategoria',
        bash_command='rm -r /tmp/dbt_test_not_null_silver_contratos_empenhos_subcategoria || true \
&& cp -r /opt/airflow/dags-config/repo/plugins/dbt /tmp/dbt_test_not_null_silver_contratos_empenhos_subcategoria \
&& cd /tmp/dbt_test_not_null_silver_contratos_empenhos_subcategoria \
&& dbt deps && dbt test --select not_null_silver_contratos_empenhos_subcategoria \
&& rm -r /tmp/dbt_test_not_null_silver_contratos_empenhos_subcategoria',
        env={
            'DBT_POSTGRES_HOST': Variable.get("ipea_dw_pg_host"),
            'DBT_POSTGRES_USER': Variable.get("ipea_dw_pg_user"),
            'DBT_POSTGRES_PASSWORD': Variable.get("ipea_dw_pg_password"),
            'DBT_POSTGRES_ENVIRONMENT': Variable.get("ipea_dw_pg_environment"),
            'DBT_POSTGRES_PORT': Variable.get("ipea_dw_pg_port"),
            'DBT_POSTGRES_DATABASE': Variable.get("ipea_dw_pg_db"),
        },
        append_env=True
    )

    not_null_silver_contratos_empenhos_unidades_requisitantes_task = BashOperator(
        task_id='test_not_null_silver_contratos_empenhos_unidades_requisitantes',
        bash_command='rm -r /tmp/dbt_test_not_null_silver_contratos_empenhos_unidades_requisitantes || true \
&& cp -r /opt/airflow/dags-config/repo/plugins/dbt /tmp/dbt_test_not_null_silver_contratos_empenhos_unidades_requisitantes \
&& cd /tmp/dbt_test_not_null_silver_contratos_empenhos_unidades_requisitantes \
&& dbt deps && dbt test --select not_null_silver_contratos_empenhos_unidades_requisitantes \
&& rm -r /tmp/dbt_test_not_null_silver_contratos_empenhos_unidades_requisitantes',
        env={
            'DBT_POSTGRES_HOST': Variable.get("ipea_dw_pg_host"),
            'DBT_POSTGRES_USER': Variable.get("ipea_dw_pg_user"),
            'DBT_POSTGRES_PASSWORD': Variable.get("ipea_dw_pg_password"),
            'DBT_POSTGRES_ENVIRONMENT': Variable.get("ipea_dw_pg_environment"),
            'DBT_POSTGRES_PORT': Variable.get("ipea_dw_pg_port"),
            'DBT_POSTGRES_DATABASE': Variable.get("ipea_dw_pg_db"),
        },
        append_env=True
    )

    not_null_silver_contratos_empenhos_processo_task = BashOperator(
        task_id='test_not_null_silver_contratos_empenhos_processo',
        bash_command='rm -r /tmp/dbt_test_not_null_silver_contratos_empenhos_processo || true \
&& cp -r /opt/airflow/dags-config/repo/plugins/dbt /tmp/dbt_test_not_null_silver_contratos_empenhos_processo \
&& cd /tmp/dbt_test_not_null_silver_contratos_empenhos_processo \
&& dbt deps && dbt test --select not_null_silver_contratos_empenhos_processo \
&& rm -r /tmp/dbt_test_not_null_silver_contratos_empenhos_processo',
        env={
            'DBT_POSTGRES_HOST': Variable.get("ipea_dw_pg_host"),
            'DBT_POSTGRES_USER': Variable.get("ipea_dw_pg_user"),
            'DBT_POSTGRES_PASSWORD': Variable.get("ipea_dw_pg_password"),
            'DBT_POSTGRES_ENVIRONMENT': Variable.get("ipea_dw_pg_environment"),
            'DBT_POSTGRES_PORT': Variable.get("ipea_dw_pg_port"),
            'DBT_POSTGRES_DATABASE': Variable.get("ipea_dw_pg_db"),
        },
        append_env=True
    )

    not_null_silver_contratos_empenhos_objeto_task = BashOperator(
        task_id='test_not_null_silver_contratos_empenhos_objeto',
        bash_command='rm -r /tmp/dbt_test_not_null_silver_contratos_empenhos_objeto || true \
&& cp -r /opt/airflow/dags-config/repo/plugins/dbt /tmp/dbt_test_not_null_silver_contratos_empenhos_objeto \
&& cd /tmp/dbt_test_not_null_silver_contratos_empenhos_objeto \
&& dbt deps && dbt test --select not_null_silver_contratos_empenhos_objeto \
&& rm -r /tmp/dbt_test_not_null_silver_contratos_empenhos_objeto',
        env={
            'DBT_POSTGRES_HOST': Variable.get("ipea_dw_pg_host"),
            'DBT_POSTGRES_USER': Variable.get("ipea_dw_pg_user"),
            'DBT_POSTGRES_PASSWORD': Variable.get("ipea_dw_pg_password"),
            'DBT_POSTGRES_ENVIRONMENT': Variable.get("ipea_dw_pg_environment"),
            'DBT_POSTGRES_PORT': Variable.get("ipea_dw_pg_port"),
            'DBT_POSTGRES_DATABASE': Variable.get("ipea_dw_pg_db"),
        },
        append_env=True
    )

    not_null_silver_contratos_empenhos_amparo_legal_task = BashOperator(
        task_id='test_not_null_silver_contratos_empenhos_amparo_legal',
        bash_command='rm -r /tmp/dbt_test_not_null_silver_contratos_empenhos_amparo_legal || true \
&& cp -r /opt/airflow/dags-config/repo/plugins/dbt /tmp/dbt_test_not_null_silver_contratos_empenhos_amparo_legal \
&& cd /tmp/dbt_test_not_null_silver_contratos_empenhos_amparo_legal \
&& dbt deps && dbt test --select not_null_silver_contratos_empenhos_amparo_legal \
&& rm -r /tmp/dbt_test_not_null_silver_contratos_empenhos_amparo_legal',
        env={
            'DBT_POSTGRES_HOST': Variable.get("ipea_dw_pg_host"),
            'DBT_POSTGRES_USER': Variable.get("ipea_dw_pg_user"),
            'DBT_POSTGRES_PASSWORD': Variable.get("ipea_dw_pg_password"),
            'DBT_POSTGRES_ENVIRONMENT': Variable.get("ipea_dw_pg_environment"),
            'DBT_POSTGRES_PORT': Variable.get("ipea_dw_pg_port"),
            'DBT_POSTGRES_DATABASE': Variable.get("ipea_dw_pg_db"),
        },
        append_env=True
    )

    not_null_silver_contratos_empenhos_informacao_complementar_task = BashOperator(
        task_id='test_not_null_silver_contratos_empenhos_informacao_complementar',
        bash_command='rm -r /tmp/dbt_test_not_null_silver_contratos_empenhos_informacao_complementar || true \
&& cp -r /opt/airflow/dags-config/repo/plugins/dbt /tmp/dbt_test_not_null_silver_contratos_empenhos_informacao_complementar \
&& cd /tmp/dbt_test_not_null_silver_contratos_empenhos_informacao_complementar \
&& dbt deps && dbt test --select not_null_silver_contratos_empenhos_informacao_complementar \
&& rm -r /tmp/dbt_test_not_null_silver_contratos_empenhos_informacao_complementar',
        env={
            'DBT_POSTGRES_HOST': Variable.get("ipea_dw_pg_host"),
            'DBT_POSTGRES_USER': Variable.get("ipea_dw_pg_user"),
            'DBT_POSTGRES_PASSWORD': Variable.get("ipea_dw_pg_password"),
            'DBT_POSTGRES_ENVIRONMENT': Variable.get("ipea_dw_pg_environment"),
            'DBT_POSTGRES_PORT': Variable.get("ipea_dw_pg_port"),
            'DBT_POSTGRES_DATABASE': Variable.get("ipea_dw_pg_db"),
        },
        append_env=True
    )

    not_null_silver_contratos_empenhos_codigo_modalidade_task = BashOperator(
        task_id='test_not_null_silver_contratos_empenhos_codigo_modalidade',
        bash_command='rm -r /tmp/dbt_test_not_null_silver_contratos_empenhos_codigo_modalidade || true \
&& cp -r /opt/airflow/dags-config/repo/plugins/dbt /tmp/dbt_test_not_null_silver_contratos_empenhos_codigo_modalidade \
&& cd /tmp/dbt_test_not_null_silver_contratos_empenhos_codigo_modalidade \
&& dbt deps && dbt test --select not_null_silver_contratos_empenhos_codigo_modalidade \
&& rm -r /tmp/dbt_test_not_null_silver_contratos_empenhos_codigo_modalidade',
        env={
            'DBT_POSTGRES_HOST': Variable.get("ipea_dw_pg_host"),
            'DBT_POSTGRES_USER': Variable.get("ipea_dw_pg_user"),
            'DBT_POSTGRES_PASSWORD': Variable.get("ipea_dw_pg_password"),
            'DBT_POSTGRES_ENVIRONMENT': Variable.get("ipea_dw_pg_environment"),
            'DBT_POSTGRES_PORT': Variable.get("ipea_dw_pg_port"),
            'DBT_POSTGRES_DATABASE': Variable.get("ipea_dw_pg_db"),
        },
        append_env=True
    )

    not_null_silver_contratos_empenhos_modalidade_task = BashOperator(
        task_id='test_not_null_silver_contratos_empenhos_modalidade',
        bash_command='rm -r /tmp/dbt_test_not_null_silver_contratos_empenhos_modalidade || true \
&& cp -r /opt/airflow/dags-config/repo/plugins/dbt /tmp/dbt_test_not_null_silver_contratos_empenhos_modalidade \
&& cd /tmp/dbt_test_not_null_silver_contratos_empenhos_modalidade \
&& dbt deps && dbt test --select not_null_silver_contratos_empenhos_modalidade \
&& rm -r /tmp/dbt_test_not_null_silver_contratos_empenhos_modalidade',
        env={
            'DBT_POSTGRES_HOST': Variable.get("ipea_dw_pg_host"),
            'DBT_POSTGRES_USER': Variable.get("ipea_dw_pg_user"),
            'DBT_POSTGRES_PASSWORD': Variable.get("ipea_dw_pg_password"),
            'DBT_POSTGRES_ENVIRONMENT': Variable.get("ipea_dw_pg_environment"),
            'DBT_POSTGRES_PORT': Variable.get("ipea_dw_pg_port"),
            'DBT_POSTGRES_DATABASE': Variable.get("ipea_dw_pg_db"),
        },
        append_env=True
    )

    not_null_silver_contratos_empenhos_unidade_compra_task = BashOperator(
        task_id='test_not_null_silver_contratos_empenhos_unidade_compra',
        bash_command='rm -r /tmp/dbt_test_not_null_silver_contratos_empenhos_unidade_compra || true \
&& cp -r /opt/airflow/dags-config/repo/plugins/dbt /tmp/dbt_test_not_null_silver_contratos_empenhos_unidade_compra \
&& cd /tmp/dbt_test_not_null_silver_contratos_empenhos_unidade_compra \
&& dbt deps && dbt test --select not_null_silver_contratos_empenhos_unidade_compra \
&& rm -r /tmp/dbt_test_not_null_silver_contratos_empenhos_unidade_compra',
        env={
            'DBT_POSTGRES_HOST': Variable.get("ipea_dw_pg_host"),
            'DBT_POSTGRES_USER': Variable.get("ipea_dw_pg_user"),
            'DBT_POSTGRES_PASSWORD': Variable.get("ipea_dw_pg_password"),
            'DBT_POSTGRES_ENVIRONMENT': Variable.get("ipea_dw_pg_environment"),
            'DBT_POSTGRES_PORT': Variable.get("ipea_dw_pg_port"),
            'DBT_POSTGRES_DATABASE': Variable.get("ipea_dw_pg_db"),
        },
        append_env=True
    )

    not_null_silver_contratos_empenhos_licitacao_numero_task = BashOperator(
        task_id='test_not_null_silver_contratos_empenhos_licitacao_numero',
        bash_command='rm -r /tmp/dbt_test_not_null_silver_contratos_empenhos_licitacao_numero || true \
&& cp -r /opt/airflow/dags-config/repo/plugins/dbt /tmp/dbt_test_not_null_silver_contratos_empenhos_licitacao_numero \
&& cd /tmp/dbt_test_not_null_silver_contratos_empenhos_licitacao_numero \
&& dbt deps && dbt test --select not_null_silver_contratos_empenhos_licitacao_numero \
&& rm -r /tmp/dbt_test_not_null_silver_contratos_empenhos_licitacao_numero',
        env={
            'DBT_POSTGRES_HOST': Variable.get("ipea_dw_pg_host"),
            'DBT_POSTGRES_USER': Variable.get("ipea_dw_pg_user"),
            'DBT_POSTGRES_PASSWORD': Variable.get("ipea_dw_pg_password"),
            'DBT_POSTGRES_ENVIRONMENT': Variable.get("ipea_dw_pg_environment"),
            'DBT_POSTGRES_PORT': Variable.get("ipea_dw_pg_port"),
            'DBT_POSTGRES_DATABASE': Variable.get("ipea_dw_pg_db"),
        },
        append_env=True
    )

    not_null_silver_contratos_empenhos_sistema_origem_licitacao_task = BashOperator(
        task_id='test_not_null_silver_contratos_empenhos_sistema_origem_licitacao',
        bash_command='rm -r /tmp/dbt_test_not_null_silver_contratos_empenhos_sistema_origem_licitacao || true \
&& cp -r /opt/airflow/dags-config/repo/plugins/dbt /tmp/dbt_test_not_null_silver_contratos_empenhos_sistema_origem_licitacao \
&& cd /tmp/dbt_test_not_null_silver_contratos_empenhos_sistema_origem_licitacao \
&& dbt deps && dbt test --select not_null_silver_contratos_empenhos_sistema_origem_licitacao \
&& rm -r /tmp/dbt_test_not_null_silver_contratos_empenhos_sistema_origem_licitacao',
        env={
            'DBT_POSTGRES_HOST': Variable.get("ipea_dw_pg_host"),
            'DBT_POSTGRES_USER': Variable.get("ipea_dw_pg_user"),
            'DBT_POSTGRES_PASSWORD': Variable.get("ipea_dw_pg_password"),
            'DBT_POSTGRES_ENVIRONMENT': Variable.get("ipea_dw_pg_environment"),
            'DBT_POSTGRES_PORT': Variable.get("ipea_dw_pg_port"),
            'DBT_POSTGRES_DATABASE': Variable.get("ipea_dw_pg_db"),
        },
        append_env=True
    )

    not_null_silver_contratos_empenhos_data_assinatura_task = BashOperator(
        task_id='test_not_null_silver_contratos_empenhos_data_assinatura',
        bash_command='rm -r /tmp/dbt_test_not_null_silver_contratos_empenhos_data_assinatura || true \
&& cp -r /opt/airflow/dags-config/repo/plugins/dbt /tmp/dbt_test_not_null_silver_contratos_empenhos_data_assinatura \
&& cd /tmp/dbt_test_not_null_silver_contratos_empenhos_data_assinatura \
&& dbt deps && dbt test --select not_null_silver_contratos_empenhos_data_assinatura \
&& rm -r /tmp/dbt_test_not_null_silver_contratos_empenhos_data_assinatura',
        env={
            'DBT_POSTGRES_HOST': Variable.get("ipea_dw_pg_host"),
            'DBT_POSTGRES_USER': Variable.get("ipea_dw_pg_user"),
            'DBT_POSTGRES_PASSWORD': Variable.get("ipea_dw_pg_password"),
            'DBT_POSTGRES_ENVIRONMENT': Variable.get("ipea_dw_pg_environment"),
            'DBT_POSTGRES_PORT': Variable.get("ipea_dw_pg_port"),
            'DBT_POSTGRES_DATABASE': Variable.get("ipea_dw_pg_db"),
        },
        append_env=True
    )

    not_null_silver_contratos_empenhos_data_publicacao_task = BashOperator(
        task_id='test_not_null_silver_contratos_empenhos_data_publicacao',
        bash_command='rm -r /tmp/dbt_test_not_null_silver_contratos_empenhos_data_publicacao || true \
&& cp -r /opt/airflow/dags-config/repo/plugins/dbt /tmp/dbt_test_not_null_silver_contratos_empenhos_data_publicacao \
&& cd /tmp/dbt_test_not_null_silver_contratos_empenhos_data_publicacao \
&& dbt deps && dbt test --select not_null_silver_contratos_empenhos_data_publicacao \
&& rm -r /tmp/dbt_test_not_null_silver_contratos_empenhos_data_publicacao',
        env={
            'DBT_POSTGRES_HOST': Variable.get("ipea_dw_pg_host"),
            'DBT_POSTGRES_USER': Variable.get("ipea_dw_pg_user"),
            'DBT_POSTGRES_PASSWORD': Variable.get("ipea_dw_pg_password"),
            'DBT_POSTGRES_ENVIRONMENT': Variable.get("ipea_dw_pg_environment"),
            'DBT_POSTGRES_PORT': Variable.get("ipea_dw_pg_port"),
            'DBT_POSTGRES_DATABASE': Variable.get("ipea_dw_pg_db"),
        },
        append_env=True
    )

    not_null_silver_contratos_empenhos_data_proposta_comercial_task = BashOperator(
        task_id='test_not_null_silver_contratos_empenhos_data_proposta_comercial',
        bash_command='rm -r /tmp/dbt_test_not_null_silver_contratos_empenhos_data_proposta_comercial || true \
&& cp -r /opt/airflow/dags-config/repo/plugins/dbt /tmp/dbt_test_not_null_silver_contratos_empenhos_data_proposta_comercial \
&& cd /tmp/dbt_test_not_null_silver_contratos_empenhos_data_proposta_comercial \
&& dbt deps && dbt test --select not_null_silver_contratos_empenhos_data_proposta_comercial \
&& rm -r /tmp/dbt_test_not_null_silver_contratos_empenhos_data_proposta_comercial',
        env={
            'DBT_POSTGRES_HOST': Variable.get("ipea_dw_pg_host"),
            'DBT_POSTGRES_USER': Variable.get("ipea_dw_pg_user"),
            'DBT_POSTGRES_PASSWORD': Variable.get("ipea_dw_pg_password"),
            'DBT_POSTGRES_ENVIRONMENT': Variable.get("ipea_dw_pg_environment"),
            'DBT_POSTGRES_PORT': Variable.get("ipea_dw_pg_port"),
            'DBT_POSTGRES_DATABASE': Variable.get("ipea_dw_pg_db"),
        },
        append_env=True
    )

    not_null_silver_contratos_empenhos_vigencia_inicio_task = BashOperator(
        task_id='test_not_null_silver_contratos_empenhos_vigencia_inicio',
        bash_command='rm -r /tmp/dbt_test_not_null_silver_contratos_empenhos_vigencia_inicio || true \
&& cp -r /opt/airflow/dags-config/repo/plugins/dbt /tmp/dbt_test_not_null_silver_contratos_empenhos_vigencia_inicio \
&& cd /tmp/dbt_test_not_null_silver_contratos_empenhos_vigencia_inicio \
&& dbt deps && dbt test --select not_null_silver_contratos_empenhos_vigencia_inicio \
&& rm -r /tmp/dbt_test_not_null_silver_contratos_empenhos_vigencia_inicio',
        env={
            'DBT_POSTGRES_HOST': Variable.get("ipea_dw_pg_host"),
            'DBT_POSTGRES_USER': Variable.get("ipea_dw_pg_user"),
            'DBT_POSTGRES_PASSWORD': Variable.get("ipea_dw_pg_password"),
            'DBT_POSTGRES_ENVIRONMENT': Variable.get("ipea_dw_pg_environment"),
            'DBT_POSTGRES_PORT': Variable.get("ipea_dw_pg_port"),
            'DBT_POSTGRES_DATABASE': Variable.get("ipea_dw_pg_db"),
        },
        append_env=True
    )

    not_null_silver_contratos_empenhos_vigencia_fim_task = BashOperator(
        task_id='test_not_null_silver_contratos_empenhos_vigencia_fim',
        bash_command='rm -r /tmp/dbt_test_not_null_silver_contratos_empenhos_vigencia_fim || true \
&& cp -r /opt/airflow/dags-config/repo/plugins/dbt /tmp/dbt_test_not_null_silver_contratos_empenhos_vigencia_fim \
&& cd /tmp/dbt_test_not_null_silver_contratos_empenhos_vigencia_fim \
&& dbt deps && dbt test --select not_null_silver_contratos_empenhos_vigencia_fim \
&& rm -r /tmp/dbt_test_not_null_silver_contratos_empenhos_vigencia_fim',
        env={
            'DBT_POSTGRES_HOST': Variable.get("ipea_dw_pg_host"),
            'DBT_POSTGRES_USER': Variable.get("ipea_dw_pg_user"),
            'DBT_POSTGRES_PASSWORD': Variable.get("ipea_dw_pg_password"),
            'DBT_POSTGRES_ENVIRONMENT': Variable.get("ipea_dw_pg_environment"),
            'DBT_POSTGRES_PORT': Variable.get("ipea_dw_pg_port"),
            'DBT_POSTGRES_DATABASE': Variable.get("ipea_dw_pg_db"),
        },
        append_env=True
    )

    not_null_silver_contratos_empenhos_valor_inicial_task = BashOperator(
        task_id='test_not_null_silver_contratos_empenhos_valor_inicial',
        bash_command='rm -r /tmp/dbt_test_not_null_silver_contratos_empenhos_valor_inicial || true \
&& cp -r /opt/airflow/dags-config/repo/plugins/dbt /tmp/dbt_test_not_null_silver_contratos_empenhos_valor_inicial \
&& cd /tmp/dbt_test_not_null_silver_contratos_empenhos_valor_inicial \
&& dbt deps && dbt test --select not_null_silver_contratos_empenhos_valor_inicial \
&& rm -r /tmp/dbt_test_not_null_silver_contratos_empenhos_valor_inicial',
        env={
            'DBT_POSTGRES_HOST': Variable.get("ipea_dw_pg_host"),
            'DBT_POSTGRES_USER': Variable.get("ipea_dw_pg_user"),
            'DBT_POSTGRES_PASSWORD': Variable.get("ipea_dw_pg_password"),
            'DBT_POSTGRES_ENVIRONMENT': Variable.get("ipea_dw_pg_environment"),
            'DBT_POSTGRES_PORT': Variable.get("ipea_dw_pg_port"),
            'DBT_POSTGRES_DATABASE': Variable.get("ipea_dw_pg_db"),
        },
        append_env=True
    )

    not_null_silver_contratos_empenhos_valor_global_task = BashOperator(
        task_id='test_not_null_silver_contratos_empenhos_valor_global',
        bash_command='rm -r /tmp/dbt_test_not_null_silver_contratos_empenhos_valor_global || true \
&& cp -r /opt/airflow/dags-config/repo/plugins/dbt /tmp/dbt_test_not_null_silver_contratos_empenhos_valor_global \
&& cd /tmp/dbt_test_not_null_silver_contratos_empenhos_valor_global \
&& dbt deps && dbt test --select not_null_silver_contratos_empenhos_valor_global \
&& rm -r /tmp/dbt_test_not_null_silver_contratos_empenhos_valor_global',
        env={
            'DBT_POSTGRES_HOST': Variable.get("ipea_dw_pg_host"),
            'DBT_POSTGRES_USER': Variable.get("ipea_dw_pg_user"),
            'DBT_POSTGRES_PASSWORD': Variable.get("ipea_dw_pg_password"),
            'DBT_POSTGRES_ENVIRONMENT': Variable.get("ipea_dw_pg_environment"),
            'DBT_POSTGRES_PORT': Variable.get("ipea_dw_pg_port"),
            'DBT_POSTGRES_DATABASE': Variable.get("ipea_dw_pg_db"),
        },
        append_env=True
    )

    not_null_silver_contratos_empenhos_num_parcelas_task = BashOperator(
        task_id='test_not_null_silver_contratos_empenhos_num_parcelas',
        bash_command='rm -r /tmp/dbt_test_not_null_silver_contratos_empenhos_num_parcelas || true \
&& cp -r /opt/airflow/dags-config/repo/plugins/dbt /tmp/dbt_test_not_null_silver_contratos_empenhos_num_parcelas \
&& cd /tmp/dbt_test_not_null_silver_contratos_empenhos_num_parcelas \
&& dbt deps && dbt test --select not_null_silver_contratos_empenhos_num_parcelas \
&& rm -r /tmp/dbt_test_not_null_silver_contratos_empenhos_num_parcelas',
        env={
            'DBT_POSTGRES_HOST': Variable.get("ipea_dw_pg_host"),
            'DBT_POSTGRES_USER': Variable.get("ipea_dw_pg_user"),
            'DBT_POSTGRES_PASSWORD': Variable.get("ipea_dw_pg_password"),
            'DBT_POSTGRES_ENVIRONMENT': Variable.get("ipea_dw_pg_environment"),
            'DBT_POSTGRES_PORT': Variable.get("ipea_dw_pg_port"),
            'DBT_POSTGRES_DATABASE': Variable.get("ipea_dw_pg_db"),
        },
        append_env=True
    )

    not_null_silver_contratos_empenhos_valor_parcela_task = BashOperator(
        task_id='test_not_null_silver_contratos_empenhos_valor_parcela',
        bash_command='rm -r /tmp/dbt_test_not_null_silver_contratos_empenhos_valor_parcela || true \
&& cp -r /opt/airflow/dags-config/repo/plugins/dbt /tmp/dbt_test_not_null_silver_contratos_empenhos_valor_parcela \
&& cd /tmp/dbt_test_not_null_silver_contratos_empenhos_valor_parcela \
&& dbt deps && dbt test --select not_null_silver_contratos_empenhos_valor_parcela \
&& rm -r /tmp/dbt_test_not_null_silver_contratos_empenhos_valor_parcela',
        env={
            'DBT_POSTGRES_HOST': Variable.get("ipea_dw_pg_host"),
            'DBT_POSTGRES_USER': Variable.get("ipea_dw_pg_user"),
            'DBT_POSTGRES_PASSWORD': Variable.get("ipea_dw_pg_password"),
            'DBT_POSTGRES_ENVIRONMENT': Variable.get("ipea_dw_pg_environment"),
            'DBT_POSTGRES_PORT': Variable.get("ipea_dw_pg_port"),
            'DBT_POSTGRES_DATABASE': Variable.get("ipea_dw_pg_db"),
        },
        append_env=True
    )

    not_null_silver_contratos_empenhos_valor_acumulado_task = BashOperator(
        task_id='test_not_null_silver_contratos_empenhos_valor_acumulado',
        bash_command='rm -r /tmp/dbt_test_not_null_silver_contratos_empenhos_valor_acumulado || true \
&& cp -r /opt/airflow/dags-config/repo/plugins/dbt /tmp/dbt_test_not_null_silver_contratos_empenhos_valor_acumulado \
&& cd /tmp/dbt_test_not_null_silver_contratos_empenhos_valor_acumulado \
&& dbt deps && dbt test --select not_null_silver_contratos_empenhos_valor_acumulado \
&& rm -r /tmp/dbt_test_not_null_silver_contratos_empenhos_valor_acumulado',
        env={
            'DBT_POSTGRES_HOST': Variable.get("ipea_dw_pg_host"),
            'DBT_POSTGRES_USER': Variable.get("ipea_dw_pg_user"),
            'DBT_POSTGRES_PASSWORD': Variable.get("ipea_dw_pg_password"),
            'DBT_POSTGRES_ENVIRONMENT': Variable.get("ipea_dw_pg_environment"),
            'DBT_POSTGRES_PORT': Variable.get("ipea_dw_pg_port"),
            'DBT_POSTGRES_DATABASE': Variable.get("ipea_dw_pg_db"),
        },
        append_env=True
    )

    not_null_silver_contratos_empenhos_links_historico_task = BashOperator(
        task_id='test_not_null_silver_contratos_empenhos_links_historico',
        bash_command='rm -r /tmp/dbt_test_not_null_silver_contratos_empenhos_links_historico || true \
&& cp -r /opt/airflow/dags-config/repo/plugins/dbt /tmp/dbt_test_not_null_silver_contratos_empenhos_links_historico \
&& cd /tmp/dbt_test_not_null_silver_contratos_empenhos_links_historico \
&& dbt deps && dbt test --select not_null_silver_contratos_empenhos_links_historico \
&& rm -r /tmp/dbt_test_not_null_silver_contratos_empenhos_links_historico',
        env={
            'DBT_POSTGRES_HOST': Variable.get("ipea_dw_pg_host"),
            'DBT_POSTGRES_USER': Variable.get("ipea_dw_pg_user"),
            'DBT_POSTGRES_PASSWORD': Variable.get("ipea_dw_pg_password"),
            'DBT_POSTGRES_ENVIRONMENT': Variable.get("ipea_dw_pg_environment"),
            'DBT_POSTGRES_PORT': Variable.get("ipea_dw_pg_port"),
            'DBT_POSTGRES_DATABASE': Variable.get("ipea_dw_pg_db"),
        },
        append_env=True
    )

    not_null_silver_contratos_empenhos_links_empenhos_task = BashOperator(
        task_id='test_not_null_silver_contratos_empenhos_links_empenhos',
        bash_command='rm -r /tmp/dbt_test_not_null_silver_contratos_empenhos_links_empenhos || true \
&& cp -r /opt/airflow/dags-config/repo/plugins/dbt /tmp/dbt_test_not_null_silver_contratos_empenhos_links_empenhos \
&& cd /tmp/dbt_test_not_null_silver_contratos_empenhos_links_empenhos \
&& dbt deps && dbt test --select not_null_silver_contratos_empenhos_links_empenhos \
&& rm -r /tmp/dbt_test_not_null_silver_contratos_empenhos_links_empenhos',
        env={
            'DBT_POSTGRES_HOST': Variable.get("ipea_dw_pg_host"),
            'DBT_POSTGRES_USER': Variable.get("ipea_dw_pg_user"),
            'DBT_POSTGRES_PASSWORD': Variable.get("ipea_dw_pg_password"),
            'DBT_POSTGRES_ENVIRONMENT': Variable.get("ipea_dw_pg_environment"),
            'DBT_POSTGRES_PORT': Variable.get("ipea_dw_pg_port"),
            'DBT_POSTGRES_DATABASE': Variable.get("ipea_dw_pg_db"),
        },
        append_env=True
    )

    not_null_silver_contratos_empenhos_links_cronograma_task = BashOperator(
        task_id='test_not_null_silver_contratos_empenhos_links_cronograma',
        bash_command='rm -r /tmp/dbt_test_not_null_silver_contratos_empenhos_links_cronograma || true \
&& cp -r /opt/airflow/dags-config/repo/plugins/dbt /tmp/dbt_test_not_null_silver_contratos_empenhos_links_cronograma \
&& cd /tmp/dbt_test_not_null_silver_contratos_empenhos_links_cronograma \
&& dbt deps && dbt test --select not_null_silver_contratos_empenhos_links_cronograma \
&& rm -r /tmp/dbt_test_not_null_silver_contratos_empenhos_links_cronograma',
        env={
            'DBT_POSTGRES_HOST': Variable.get("ipea_dw_pg_host"),
            'DBT_POSTGRES_USER': Variable.get("ipea_dw_pg_user"),
            'DBT_POSTGRES_PASSWORD': Variable.get("ipea_dw_pg_password"),
            'DBT_POSTGRES_ENVIRONMENT': Variable.get("ipea_dw_pg_environment"),
            'DBT_POSTGRES_PORT': Variable.get("ipea_dw_pg_port"),
            'DBT_POSTGRES_DATABASE': Variable.get("ipea_dw_pg_db"),
        },
        append_env=True
    )

    not_null_silver_contratos_empenhos_links_garantias_task = BashOperator(
        task_id='test_not_null_silver_contratos_empenhos_links_garantias',
        bash_command='rm -r /tmp/dbt_test_not_null_silver_contratos_empenhos_links_garantias || true \
&& cp -r /opt/airflow/dags-config/repo/plugins/dbt /tmp/dbt_test_not_null_silver_contratos_empenhos_links_garantias \
&& cd /tmp/dbt_test_not_null_silver_contratos_empenhos_links_garantias \
&& dbt deps && dbt test --select not_null_silver_contratos_empenhos_links_garantias \
&& rm -r /tmp/dbt_test_not_null_silver_contratos_empenhos_links_garantias',
        env={
            'DBT_POSTGRES_HOST': Variable.get("ipea_dw_pg_host"),
            'DBT_POSTGRES_USER': Variable.get("ipea_dw_pg_user"),
            'DBT_POSTGRES_PASSWORD': Variable.get("ipea_dw_pg_password"),
            'DBT_POSTGRES_ENVIRONMENT': Variable.get("ipea_dw_pg_environment"),
            'DBT_POSTGRES_PORT': Variable.get("ipea_dw_pg_port"),
            'DBT_POSTGRES_DATABASE': Variable.get("ipea_dw_pg_db"),
        },
        append_env=True
    )

    not_null_silver_contratos_empenhos_links_itens_task = BashOperator(
        task_id='test_not_null_silver_contratos_empenhos_links_itens',
        bash_command='rm -r /tmp/dbt_test_not_null_silver_contratos_empenhos_links_itens || true \
&& cp -r /opt/airflow/dags-config/repo/plugins/dbt /tmp/dbt_test_not_null_silver_contratos_empenhos_links_itens \
&& cd /tmp/dbt_test_not_null_silver_contratos_empenhos_links_itens \
&& dbt deps && dbt test --select not_null_silver_contratos_empenhos_links_itens \
&& rm -r /tmp/dbt_test_not_null_silver_contratos_empenhos_links_itens',
        env={
            'DBT_POSTGRES_HOST': Variable.get("ipea_dw_pg_host"),
            'DBT_POSTGRES_USER': Variable.get("ipea_dw_pg_user"),
            'DBT_POSTGRES_PASSWORD': Variable.get("ipea_dw_pg_password"),
            'DBT_POSTGRES_ENVIRONMENT': Variable.get("ipea_dw_pg_environment"),
            'DBT_POSTGRES_PORT': Variable.get("ipea_dw_pg_port"),
            'DBT_POSTGRES_DATABASE': Variable.get("ipea_dw_pg_db"),
        },
        append_env=True
    )

    not_null_silver_contratos_empenhos_links_prepostos_task = BashOperator(
        task_id='test_not_null_silver_contratos_empenhos_links_prepostos',
        bash_command='rm -r /tmp/dbt_test_not_null_silver_contratos_empenhos_links_prepostos || true \
&& cp -r /opt/airflow/dags-config/repo/plugins/dbt /tmp/dbt_test_not_null_silver_contratos_empenhos_links_prepostos \
&& cd /tmp/dbt_test_not_null_silver_contratos_empenhos_links_prepostos \
&& dbt deps && dbt test --select not_null_silver_contratos_empenhos_links_prepostos \
&& rm -r /tmp/dbt_test_not_null_silver_contratos_empenhos_links_prepostos',
        env={
            'DBT_POSTGRES_HOST': Variable.get("ipea_dw_pg_host"),
            'DBT_POSTGRES_USER': Variable.get("ipea_dw_pg_user"),
            'DBT_POSTGRES_PASSWORD': Variable.get("ipea_dw_pg_password"),
            'DBT_POSTGRES_ENVIRONMENT': Variable.get("ipea_dw_pg_environment"),
            'DBT_POSTGRES_PORT': Variable.get("ipea_dw_pg_port"),
            'DBT_POSTGRES_DATABASE': Variable.get("ipea_dw_pg_db"),
        },
        append_env=True
    )

    not_null_silver_contratos_empenhos_links_responsaveis_task = BashOperator(
        task_id='test_not_null_silver_contratos_empenhos_links_responsaveis',
        bash_command='rm -r /tmp/dbt_test_not_null_silver_contratos_empenhos_links_responsaveis || true \
&& cp -r /opt/airflow/dags-config/repo/plugins/dbt /tmp/dbt_test_not_null_silver_contratos_empenhos_links_responsaveis \
&& cd /tmp/dbt_test_not_null_silver_contratos_empenhos_links_responsaveis \
&& dbt deps && dbt test --select not_null_silver_contratos_empenhos_links_responsaveis \
&& rm -r /tmp/dbt_test_not_null_silver_contratos_empenhos_links_responsaveis',
        env={
            'DBT_POSTGRES_HOST': Variable.get("ipea_dw_pg_host"),
            'DBT_POSTGRES_USER': Variable.get("ipea_dw_pg_user"),
            'DBT_POSTGRES_PASSWORD': Variable.get("ipea_dw_pg_password"),
            'DBT_POSTGRES_ENVIRONMENT': Variable.get("ipea_dw_pg_environment"),
            'DBT_POSTGRES_PORT': Variable.get("ipea_dw_pg_port"),
            'DBT_POSTGRES_DATABASE': Variable.get("ipea_dw_pg_db"),
        },
        append_env=True
    )

    not_null_silver_contratos_empenhos_links_despesas_acessorias_task = BashOperator(
        task_id='test_not_null_silver_contratos_empenhos_links_despesas_acessorias',
        bash_command='rm -r /tmp/dbt_test_not_null_silver_contratos_empenhos_links_despesas_acessorias || true \
&& cp -r /opt/airflow/dags-config/repo/plugins/dbt /tmp/dbt_test_not_null_silver_contratos_empenhos_links_despesas_acessorias \
&& cd /tmp/dbt_test_not_null_silver_contratos_empenhos_links_despesas_acessorias \
&& dbt deps && dbt test --select not_null_silver_contratos_empenhos_links_despesas_acessorias \
&& rm -r /tmp/dbt_test_not_null_silver_contratos_empenhos_links_despesas_acessorias',
        env={
            'DBT_POSTGRES_HOST': Variable.get("ipea_dw_pg_host"),
            'DBT_POSTGRES_USER': Variable.get("ipea_dw_pg_user"),
            'DBT_POSTGRES_PASSWORD': Variable.get("ipea_dw_pg_password"),
            'DBT_POSTGRES_ENVIRONMENT': Variable.get("ipea_dw_pg_environment"),
            'DBT_POSTGRES_PORT': Variable.get("ipea_dw_pg_port"),
            'DBT_POSTGRES_DATABASE': Variable.get("ipea_dw_pg_db"),
        },
        append_env=True
    )

    not_null_silver_contratos_empenhos_links_faturas_task = BashOperator(
        task_id='test_not_null_silver_contratos_empenhos_links_faturas',
        bash_command='rm -r /tmp/dbt_test_not_null_silver_contratos_empenhos_links_faturas || true \
&& cp -r /opt/airflow/dags-config/repo/plugins/dbt /tmp/dbt_test_not_null_silver_contratos_empenhos_links_faturas \
&& cd /tmp/dbt_test_not_null_silver_contratos_empenhos_links_faturas \
&& dbt deps && dbt test --select not_null_silver_contratos_empenhos_links_faturas \
&& rm -r /tmp/dbt_test_not_null_silver_contratos_empenhos_links_faturas',
        env={
            'DBT_POSTGRES_HOST': Variable.get("ipea_dw_pg_host"),
            'DBT_POSTGRES_USER': Variable.get("ipea_dw_pg_user"),
            'DBT_POSTGRES_PASSWORD': Variable.get("ipea_dw_pg_password"),
            'DBT_POSTGRES_ENVIRONMENT': Variable.get("ipea_dw_pg_environment"),
            'DBT_POSTGRES_PORT': Variable.get("ipea_dw_pg_port"),
            'DBT_POSTGRES_DATABASE': Variable.get("ipea_dw_pg_db"),
        },
        append_env=True
    )

    not_null_silver_contratos_empenhos_links_ocorrencias_task = BashOperator(
        task_id='test_not_null_silver_contratos_empenhos_links_ocorrencias',
        bash_command='rm -r /tmp/dbt_test_not_null_silver_contratos_empenhos_links_ocorrencias || true \
&& cp -r /opt/airflow/dags-config/repo/plugins/dbt /tmp/dbt_test_not_null_silver_contratos_empenhos_links_ocorrencias \
&& cd /tmp/dbt_test_not_null_silver_contratos_empenhos_links_ocorrencias \
&& dbt deps && dbt test --select not_null_silver_contratos_empenhos_links_ocorrencias \
&& rm -r /tmp/dbt_test_not_null_silver_contratos_empenhos_links_ocorrencias',
        env={
            'DBT_POSTGRES_HOST': Variable.get("ipea_dw_pg_host"),
            'DBT_POSTGRES_USER': Variable.get("ipea_dw_pg_user"),
            'DBT_POSTGRES_PASSWORD': Variable.get("ipea_dw_pg_password"),
            'DBT_POSTGRES_ENVIRONMENT': Variable.get("ipea_dw_pg_environment"),
            'DBT_POSTGRES_PORT': Variable.get("ipea_dw_pg_port"),
            'DBT_POSTGRES_DATABASE': Variable.get("ipea_dw_pg_db"),
        },
        append_env=True
    )

    not_null_silver_contratos_empenhos_links_terceirizados_task = BashOperator(
        task_id='test_not_null_silver_contratos_empenhos_links_terceirizados',
        bash_command='rm -r /tmp/dbt_test_not_null_silver_contratos_empenhos_links_terceirizados || true \
&& cp -r /opt/airflow/dags-config/repo/plugins/dbt /tmp/dbt_test_not_null_silver_contratos_empenhos_links_terceirizados \
&& cd /tmp/dbt_test_not_null_silver_contratos_empenhos_links_terceirizados \
&& dbt deps && dbt test --select not_null_silver_contratos_empenhos_links_terceirizados \
&& rm -r /tmp/dbt_test_not_null_silver_contratos_empenhos_links_terceirizados',
        env={
            'DBT_POSTGRES_HOST': Variable.get("ipea_dw_pg_host"),
            'DBT_POSTGRES_USER': Variable.get("ipea_dw_pg_user"),
            'DBT_POSTGRES_PASSWORD': Variable.get("ipea_dw_pg_password"),
            'DBT_POSTGRES_ENVIRONMENT': Variable.get("ipea_dw_pg_environment"),
            'DBT_POSTGRES_PORT': Variable.get("ipea_dw_pg_port"),
            'DBT_POSTGRES_DATABASE': Variable.get("ipea_dw_pg_db"),
        },
        append_env=True
    )

    not_null_silver_contratos_empenhos_links_arquivos_task = BashOperator(
        task_id='test_not_null_silver_contratos_empenhos_links_arquivos',
        bash_command='rm -r /tmp/dbt_test_not_null_silver_contratos_empenhos_links_arquivos || true \
&& cp -r /opt/airflow/dags-config/repo/plugins/dbt /tmp/dbt_test_not_null_silver_contratos_empenhos_links_arquivos \
&& cd /tmp/dbt_test_not_null_silver_contratos_empenhos_links_arquivos \
&& dbt deps && dbt test --select not_null_silver_contratos_empenhos_links_arquivos \
&& rm -r /tmp/dbt_test_not_null_silver_contratos_empenhos_links_arquivos',
        env={
            'DBT_POSTGRES_HOST': Variable.get("ipea_dw_pg_host"),
            'DBT_POSTGRES_USER': Variable.get("ipea_dw_pg_user"),
            'DBT_POSTGRES_PASSWORD': Variable.get("ipea_dw_pg_password"),
            'DBT_POSTGRES_ENVIRONMENT': Variable.get("ipea_dw_pg_environment"),
            'DBT_POSTGRES_PORT': Variable.get("ipea_dw_pg_port"),
            'DBT_POSTGRES_DATABASE': Variable.get("ipea_dw_pg_db"),
        },
        append_env=True
    )

    not_null_silver_contratos_empenhos_origem_task = BashOperator(
        task_id='test_not_null_silver_contratos_empenhos_origem',
        bash_command='rm -r /tmp/dbt_test_not_null_silver_contratos_empenhos_origem || true \
&& cp -r /opt/airflow/dags-config/repo/plugins/dbt /tmp/dbt_test_not_null_silver_contratos_empenhos_origem \
&& cd /tmp/dbt_test_not_null_silver_contratos_empenhos_origem \
&& dbt deps && dbt test --select not_null_silver_contratos_empenhos_origem \
&& rm -r /tmp/dbt_test_not_null_silver_contratos_empenhos_origem',
        env={
            'DBT_POSTGRES_HOST': Variable.get("ipea_dw_pg_host"),
            'DBT_POSTGRES_USER': Variable.get("ipea_dw_pg_user"),
            'DBT_POSTGRES_PASSWORD': Variable.get("ipea_dw_pg_password"),
            'DBT_POSTGRES_ENVIRONMENT': Variable.get("ipea_dw_pg_environment"),
            'DBT_POSTGRES_PORT': Variable.get("ipea_dw_pg_port"),
            'DBT_POSTGRES_DATABASE': Variable.get("ipea_dw_pg_db"),
        },
        append_env=True
    )

    silver_contratos_empenhos_task >> not_null_silver_contratos_empenhos_ne_ccor_task >> end_task
    silver_contratos_empenhos_task >> not_null_silver_contratos_empenhos_ne_informacao_complementar_task >> end_task
    silver_contratos_empenhos_task >> not_null_silver_contratos_empenhos_ne_num_processo_task >> end_task
    silver_contratos_empenhos_task >> not_null_silver_contratos_empenhos_ne_ccor_descricao_task >> end_task
    silver_contratos_empenhos_task >> not_null_silver_contratos_empenhos_doc_observacao_task >> end_task
    silver_contratos_empenhos_task >> not_null_silver_contratos_empenhos_natureza_despesa_task >> end_task
    silver_contratos_empenhos_task >> not_null_silver_contratos_empenhos_natureza_despesa_1_task >> end_task
    silver_contratos_empenhos_task >> not_null_silver_contratos_empenhos_natureza_despesa_detalhada_task >> end_task
    silver_contratos_empenhos_task >> not_null_silver_contratos_empenhos_natureza_despesa_detalhada_1_task >> end_task
    silver_contratos_empenhos_task >> not_null_silver_contratos_empenhos_ne_ccor_favorecido_task >> end_task
    silver_contratos_empenhos_task >> not_null_silver_contratos_empenhos_ne_ccor_favorecido_1_task >> end_task
    silver_contratos_empenhos_task >> not_null_silver_contratos_empenhos_ne_ccor_ano_emissao_task >> end_task
    silver_contratos_empenhos_task >> not_null_silver_contratos_empenhos_ne_ccor_ano_emissao_1_task >> end_task
    silver_contratos_empenhos_task >> not_null_silver_contratos_empenhos_despesas_empenhadas_saldo_task >> end_task
    silver_contratos_empenhos_task >> not_null_silver_contratos_empenhos_despesas_empenhadas_movim_liquido_task >> end_task
    silver_contratos_empenhos_task >> not_null_silver_contratos_empenhos_despesas_liquidadas_saldo_task >> end_task
    silver_contratos_empenhos_task >> not_null_silver_contratos_empenhos_despesas_liquidadas_movim_liquido_task >> end_task
    silver_contratos_empenhos_task >> not_null_silver_contratos_empenhos_despesas_pagas_saldo_task >> end_task
    silver_contratos_empenhos_task >> not_null_silver_contratos_empenhos_despesas_pagas_movim_liquido_task >> end_task
    silver_contratos_empenhos_task >> not_null_silver_contratos_empenhos_contrato_id_task >> end_task
    silver_contratos_empenhos_task >> not_null_silver_contratos_empenhos_receita_despesa_task >> end_task
    silver_contratos_empenhos_task >> not_null_silver_contratos_empenhos_numero_task >> end_task
    silver_contratos_empenhos_task >> not_null_silver_contratos_empenhos_contratante_orgao_origem_codigo_task >> end_task
    silver_contratos_empenhos_task >> not_null_silver_contratos_empenhos_contratante_orgao_origem_nome_task >> end_task
    silver_contratos_empenhos_task >> not_null_silver_contratos_empenhos_contratante_orgao_origem_unidade_gestora_origem_codigo_task >> end_task
    silver_contratos_empenhos_task >> not_null_silver_contratos_empenhos_contratante_orgao_origem_unidade_gestora_origem_nome_resumido_task >> end_task
    silver_contratos_empenhos_task >> not_null_silver_contratos_empenhos_contratante_orgao_origem_unidade_gestora_origem_nome_task >> end_task
    silver_contratos_empenhos_task >> not_null_silver_contratos_empenhos_contratante_orgao_origem_unidade_gestora_origem_sisg_task >> end_task
    silver_contratos_empenhos_task >> not_null_silver_contratos_empenhos_contratante_orgao_origem_unidade_gestora_origem_utiliza_siafi_task >> end_task
    silver_contratos_empenhos_task >> not_null_silver_contratos_empenhos_contratante_orgao_origem_unidade_gestora_origem_utiliza_antecip_task >> end_task
    silver_contratos_empenhos_task >> not_null_silver_contratos_empenhos_contratante_orgao_codigo_task >> end_task
    silver_contratos_empenhos_task >> not_null_silver_contratos_empenhos_contratante_orgao_nome_task >> end_task
    silver_contratos_empenhos_task >> not_null_silver_contratos_empenhos_contratante_orgao_unidade_gestora_codigo_task >> end_task
    silver_contratos_empenhos_task >> not_null_silver_contratos_empenhos_contratante_orgao_unidade_gestora_nome_resumido_task >> end_task
    silver_contratos_empenhos_task >> not_null_silver_contratos_empenhos_contratante_orgao_unidade_gestora_nome_task >> end_task
    silver_contratos_empenhos_task >> not_null_silver_contratos_empenhos_contratante_orgao_unidade_gestora_sisg_task >> end_task
    silver_contratos_empenhos_task >> not_null_silver_contratos_empenhos_contratante_orgao_unidade_gestora_utiliza_siafi_task >> end_task
    silver_contratos_empenhos_task >> not_null_silver_contratos_empenhos_contratante_orgao_unidade_gestora_utiliza_antecipagov_task >> end_task
    silver_contratos_empenhos_task >> not_null_silver_contratos_empenhos_fornecedor_tipo_task >> end_task
    silver_contratos_empenhos_task >> not_null_silver_contratos_empenhos_fornecedor_cnpj_cpf_idgener_task >> end_task
    silver_contratos_empenhos_task >> not_null_silver_contratos_empenhos_fornecedor_nome_task >> end_task
    silver_contratos_empenhos_task >> not_null_silver_contratos_empenhos_codigo_tipo_task >> end_task
    silver_contratos_empenhos_task >> not_null_silver_contratos_empenhos_tipo_task >> end_task
    silver_contratos_empenhos_task >> not_null_silver_contratos_empenhos_subtipo_task >> end_task
    silver_contratos_empenhos_task >> not_null_silver_contratos_empenhos_prorrogavel_task >> end_task
    silver_contratos_empenhos_task >> not_null_silver_contratos_empenhos_situacao_task >> end_task
    silver_contratos_empenhos_task >> not_null_silver_contratos_empenhos_justificativa_inativo_task >> end_task
    silver_contratos_empenhos_task >> not_null_silver_contratos_empenhos_categoria_task >> end_task
    silver_contratos_empenhos_task >> not_null_silver_contratos_empenhos_subcategoria_task >> end_task
    silver_contratos_empenhos_task >> not_null_silver_contratos_empenhos_unidades_requisitantes_task >> end_task
    silver_contratos_empenhos_task >> not_null_silver_contratos_empenhos_processo_task >> end_task
    silver_contratos_empenhos_task >> not_null_silver_contratos_empenhos_objeto_task >> end_task
    silver_contratos_empenhos_task >> not_null_silver_contratos_empenhos_amparo_legal_task >> end_task
    silver_contratos_empenhos_task >> not_null_silver_contratos_empenhos_informacao_complementar_task >> end_task
    silver_contratos_empenhos_task >> not_null_silver_contratos_empenhos_codigo_modalidade_task >> end_task
    silver_contratos_empenhos_task >> not_null_silver_contratos_empenhos_modalidade_task >> end_task
    silver_contratos_empenhos_task >> not_null_silver_contratos_empenhos_unidade_compra_task >> end_task
    silver_contratos_empenhos_task >> not_null_silver_contratos_empenhos_licitacao_numero_task >> end_task
    silver_contratos_empenhos_task >> not_null_silver_contratos_empenhos_sistema_origem_licitacao_task >> end_task
    silver_contratos_empenhos_task >> not_null_silver_contratos_empenhos_data_assinatura_task >> end_task
    silver_contratos_empenhos_task >> not_null_silver_contratos_empenhos_data_publicacao_task >> end_task
    silver_contratos_empenhos_task >> not_null_silver_contratos_empenhos_data_proposta_comercial_task >> end_task
    silver_contratos_empenhos_task >> not_null_silver_contratos_empenhos_vigencia_inicio_task >> end_task
    silver_contratos_empenhos_task >> not_null_silver_contratos_empenhos_vigencia_fim_task >> end_task
    silver_contratos_empenhos_task >> not_null_silver_contratos_empenhos_valor_inicial_task >> end_task
    silver_contratos_empenhos_task >> not_null_silver_contratos_empenhos_valor_global_task >> end_task
    silver_contratos_empenhos_task >> not_null_silver_contratos_empenhos_num_parcelas_task >> end_task
    silver_contratos_empenhos_task >> not_null_silver_contratos_empenhos_valor_parcela_task >> end_task
    silver_contratos_empenhos_task >> not_null_silver_contratos_empenhos_valor_acumulado_task >> end_task
    silver_contratos_empenhos_task >> not_null_silver_contratos_empenhos_links_historico_task >> end_task
    silver_contratos_empenhos_task >> not_null_silver_contratos_empenhos_links_empenhos_task >> end_task
    silver_contratos_empenhos_task >> not_null_silver_contratos_empenhos_links_cronograma_task >> end_task
    silver_contratos_empenhos_task >> not_null_silver_contratos_empenhos_links_garantias_task >> end_task
    silver_contratos_empenhos_task >> not_null_silver_contratos_empenhos_links_itens_task >> end_task
    silver_contratos_empenhos_task >> not_null_silver_contratos_empenhos_links_prepostos_task >> end_task
    silver_contratos_empenhos_task >> not_null_silver_contratos_empenhos_links_responsaveis_task >> end_task
    silver_contratos_empenhos_task >> not_null_silver_contratos_empenhos_links_despesas_acessorias_task >> end_task
    silver_contratos_empenhos_task >> not_null_silver_contratos_empenhos_links_faturas_task >> end_task
    silver_contratos_empenhos_task >> not_null_silver_contratos_empenhos_links_ocorrencias_task >> end_task
    silver_contratos_empenhos_task >> not_null_silver_contratos_empenhos_links_terceirizados_task >> end_task
    silver_contratos_empenhos_task >> not_null_silver_contratos_empenhos_links_arquivos_task >> end_task
    silver_contratos_empenhos_task >> not_null_silver_contratos_empenhos_origem_task >> end_task
