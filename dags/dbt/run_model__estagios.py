# fmt: off
# ruff: noqa

from airflow import DAG
from airflow.datasets import Dataset
from airflow.models import Variable
from airflow.operators.bash_operator import BashOperator
from airflow.operators.empty import EmptyOperator
from airflow.utils.dates import days_ago

from datetime import timedelta

default_args = {
    'owner': 'DBT-Genrated',
    'retries': 5,
    'retry_delay': timedelta(minutes=10),
    'pool': 'dbt_pool'
}

with DAG(
    "run_model__estagios",
    default_args=default_args,
    schedule='@daily',
    start_date=days_ago(1),
    tags=["dbt", "model"],
    max_active_runs=1,
    on_success_callback=None,
) as dag:

    end_task = EmptyOperator(
        task_id="end",
        outlets=[Dataset("estagios_model")],
    )

    estagios_task = BashOperator(
        task_id='run_estagios',
        bash_command='rm -r /tmp/dbt_run_estagios || true \
&& cp -r /opt/airflow/dags-config/repo/plugins/dbt /tmp/dbt_run_estagios \
&& cd /tmp/dbt_run_estagios \
&& dbt deps && dbt run --select estagios \
&& rm -r /tmp/dbt_run_estagios',
        env={
            'DBT_POSTGRES_HOST': Variable.get("ipea_dw_pg_host"),
            'DBT_POSTGRES_USER': Variable.get("ipea_dw_pg_user"),
            'DBT_POSTGRES_PASSWORD': Variable.get("ipea_dw_pg_password"),
            'DBT_POSTGRES_ENVIRONMENT': Variable.get("ipea_dw_pg_environment"),
            'DBT_POSTGRES_PORT': Variable.get("ipea_dw_pg_port"),
            'DBT_POSTGRES_DATABASE': Variable.get("ipea_dw_pg_db"),
        },
        append_env=True
    )

    not_null_estagios__NE_CCor__task = BashOperator(
        task_id='test_not_null_estagios__NE_CCor_',
        bash_command='rm -r /tmp/dbt_test_not_null_estagios__NE_CCor_ || true \
&& cp -r /opt/airflow/dags-config/repo/plugins/dbt /tmp/dbt_test_not_null_estagios__NE_CCor_ \
&& cd /tmp/dbt_test_not_null_estagios__NE_CCor_ \
&& dbt deps && dbt test --select not_null_estagios__NE_CCor_ \
&& rm -r /tmp/dbt_test_not_null_estagios__NE_CCor_',
        env={
            'DBT_POSTGRES_HOST': Variable.get("ipea_dw_pg_host"),
            'DBT_POSTGRES_USER': Variable.get("ipea_dw_pg_user"),
            'DBT_POSTGRES_PASSWORD': Variable.get("ipea_dw_pg_password"),
            'DBT_POSTGRES_ENVIRONMENT': Variable.get("ipea_dw_pg_environment"),
            'DBT_POSTGRES_PORT': Variable.get("ipea_dw_pg_port"),
            'DBT_POSTGRES_DATABASE': Variable.get("ipea_dw_pg_db"),
        },
        append_env=True
    )

    not_null_estagios__NE_N_m_Processo__task = BashOperator(
        task_id='test_not_null_estagios__NE_N_m_Processo_',
        bash_command='rm -r /tmp/dbt_test_not_null_estagios__NE_N_m_Processo_ || true \
&& cp -r /opt/airflow/dags-config/repo/plugins/dbt /tmp/dbt_test_not_null_estagios__NE_N_m_Processo_ \
&& cd /tmp/dbt_test_not_null_estagios__NE_N_m_Processo_ \
&& dbt deps && dbt test --select not_null_estagios__NE_N_m_Processo_ \
&& rm -r /tmp/dbt_test_not_null_estagios__NE_N_m_Processo_',
        env={
            'DBT_POSTGRES_HOST': Variable.get("ipea_dw_pg_host"),
            'DBT_POSTGRES_USER': Variable.get("ipea_dw_pg_user"),
            'DBT_POSTGRES_PASSWORD': Variable.get("ipea_dw_pg_password"),
            'DBT_POSTGRES_ENVIRONMENT': Variable.get("ipea_dw_pg_environment"),
            'DBT_POSTGRES_PORT': Variable.get("ipea_dw_pg_port"),
            'DBT_POSTGRES_DATABASE': Variable.get("ipea_dw_pg_db"),
        },
        append_env=True
    )

    not_null_estagios__Natureza_Despesa_1__task = BashOperator(
        task_id='test_not_null_estagios__Natureza_Despesa_1_',
        bash_command='rm -r /tmp/dbt_test_not_null_estagios__Natureza_Despesa_1_ || true \
&& cp -r /opt/airflow/dags-config/repo/plugins/dbt /tmp/dbt_test_not_null_estagios__Natureza_Despesa_1_ \
&& cd /tmp/dbt_test_not_null_estagios__Natureza_Despesa_1_ \
&& dbt deps && dbt test --select not_null_estagios__Natureza_Despesa_1_ \
&& rm -r /tmp/dbt_test_not_null_estagios__Natureza_Despesa_1_',
        env={
            'DBT_POSTGRES_HOST': Variable.get("ipea_dw_pg_host"),
            'DBT_POSTGRES_USER': Variable.get("ipea_dw_pg_user"),
            'DBT_POSTGRES_PASSWORD': Variable.get("ipea_dw_pg_password"),
            'DBT_POSTGRES_ENVIRONMENT': Variable.get("ipea_dw_pg_environment"),
            'DBT_POSTGRES_PORT': Variable.get("ipea_dw_pg_port"),
            'DBT_POSTGRES_DATABASE': Variable.get("ipea_dw_pg_db"),
        },
        append_env=True
    )

    not_null_estagios__Natureza_Despesa_Detalhada_1__task = BashOperator(
        task_id='test_not_null_estagios__Natureza_Despesa_Detalhada_1_',
        bash_command='rm -r /tmp/dbt_test_not_null_estagios__Natureza_Despesa_Detalhada_1_ || true \
&& cp -r /opt/airflow/dags-config/repo/plugins/dbt /tmp/dbt_test_not_null_estagios__Natureza_Despesa_Detalhada_1_ \
&& cd /tmp/dbt_test_not_null_estagios__Natureza_Despesa_Detalhada_1_ \
&& dbt deps && dbt test --select not_null_estagios__Natureza_Despesa_Detalhada_1_ \
&& rm -r /tmp/dbt_test_not_null_estagios__Natureza_Despesa_Detalhada_1_',
        env={
            'DBT_POSTGRES_HOST': Variable.get("ipea_dw_pg_host"),
            'DBT_POSTGRES_USER': Variable.get("ipea_dw_pg_user"),
            'DBT_POSTGRES_PASSWORD': Variable.get("ipea_dw_pg_password"),
            'DBT_POSTGRES_ENVIRONMENT': Variable.get("ipea_dw_pg_environment"),
            'DBT_POSTGRES_PORT': Variable.get("ipea_dw_pg_port"),
            'DBT_POSTGRES_DATABASE': Variable.get("ipea_dw_pg_db"),
        },
        append_env=True
    )

    not_null_estagios__NE_CCor_Favorecido__task = BashOperator(
        task_id='test_not_null_estagios__NE_CCor_Favorecido_',
        bash_command='rm -r /tmp/dbt_test_not_null_estagios__NE_CCor_Favorecido_ || true \
&& cp -r /opt/airflow/dags-config/repo/plugins/dbt /tmp/dbt_test_not_null_estagios__NE_CCor_Favorecido_ \
&& cd /tmp/dbt_test_not_null_estagios__NE_CCor_Favorecido_ \
&& dbt deps && dbt test --select not_null_estagios__NE_CCor_Favorecido_ \
&& rm -r /tmp/dbt_test_not_null_estagios__NE_CCor_Favorecido_',
        env={
            'DBT_POSTGRES_HOST': Variable.get("ipea_dw_pg_host"),
            'DBT_POSTGRES_USER': Variable.get("ipea_dw_pg_user"),
            'DBT_POSTGRES_PASSWORD': Variable.get("ipea_dw_pg_password"),
            'DBT_POSTGRES_ENVIRONMENT': Variable.get("ipea_dw_pg_environment"),
            'DBT_POSTGRES_PORT': Variable.get("ipea_dw_pg_port"),
            'DBT_POSTGRES_DATABASE': Variable.get("ipea_dw_pg_db"),
        },
        append_env=True
    )

    not_null_estagios_column8_task = BashOperator(
        task_id='test_not_null_estagios_column8',
        bash_command='rm -r /tmp/dbt_test_not_null_estagios_column8 || true \
&& cp -r /opt/airflow/dags-config/repo/plugins/dbt /tmp/dbt_test_not_null_estagios_column8 \
&& cd /tmp/dbt_test_not_null_estagios_column8 \
&& dbt deps && dbt test --select not_null_estagios_column8 \
&& rm -r /tmp/dbt_test_not_null_estagios_column8',
        env={
            'DBT_POSTGRES_HOST': Variable.get("ipea_dw_pg_host"),
            'DBT_POSTGRES_USER': Variable.get("ipea_dw_pg_user"),
            'DBT_POSTGRES_PASSWORD': Variable.get("ipea_dw_pg_password"),
            'DBT_POSTGRES_ENVIRONMENT': Variable.get("ipea_dw_pg_environment"),
            'DBT_POSTGRES_PORT': Variable.get("ipea_dw_pg_port"),
            'DBT_POSTGRES_DATABASE': Variable.get("ipea_dw_pg_db"),
        },
        append_env=True
    )

    not_null_estagios__NE_CCor_M_s_Emiss_o__task = BashOperator(
        task_id='test_not_null_estagios__NE_CCor_M_s_Emiss_o_',
        bash_command='rm -r /tmp/dbt_test_not_null_estagios__NE_CCor_M_s_Emiss_o_ || true \
&& cp -r /opt/airflow/dags-config/repo/plugins/dbt /tmp/dbt_test_not_null_estagios__NE_CCor_M_s_Emiss_o_ \
&& cd /tmp/dbt_test_not_null_estagios__NE_CCor_M_s_Emiss_o_ \
&& dbt deps && dbt test --select not_null_estagios__NE_CCor_M_s_Emiss_o_ \
&& rm -r /tmp/dbt_test_not_null_estagios__NE_CCor_M_s_Emiss_o_',
        env={
            'DBT_POSTGRES_HOST': Variable.get("ipea_dw_pg_host"),
            'DBT_POSTGRES_USER': Variable.get("ipea_dw_pg_user"),
            'DBT_POSTGRES_PASSWORD': Variable.get("ipea_dw_pg_password"),
            'DBT_POSTGRES_ENVIRONMENT': Variable.get("ipea_dw_pg_environment"),
            'DBT_POSTGRES_PORT': Variable.get("ipea_dw_pg_port"),
            'DBT_POSTGRES_DATABASE': Variable.get("ipea_dw_pg_db"),
        },
        append_env=True
    )

    not_null_estagios__M_s_Lan_amento__task = BashOperator(
        task_id='test_not_null_estagios__M_s_Lan_amento_',
        bash_command='rm -r /tmp/dbt_test_not_null_estagios__M_s_Lan_amento_ || true \
&& cp -r /opt/airflow/dags-config/repo/plugins/dbt /tmp/dbt_test_not_null_estagios__M_s_Lan_amento_ \
&& cd /tmp/dbt_test_not_null_estagios__M_s_Lan_amento_ \
&& dbt deps && dbt test --select not_null_estagios__M_s_Lan_amento_ \
&& rm -r /tmp/dbt_test_not_null_estagios__M_s_Lan_amento_',
        env={
            'DBT_POSTGRES_HOST': Variable.get("ipea_dw_pg_host"),
            'DBT_POSTGRES_USER': Variable.get("ipea_dw_pg_user"),
            'DBT_POSTGRES_PASSWORD': Variable.get("ipea_dw_pg_password"),
            'DBT_POSTGRES_ENVIRONMENT': Variable.get("ipea_dw_pg_environment"),
            'DBT_POSTGRES_PORT': Variable.get("ipea_dw_pg_port"),
            'DBT_POSTGRES_DATABASE': Variable.get("ipea_dw_pg_db"),
        },
        append_env=True
    )

    not_null_estagios_despesas_empenhadas_controle_empenho_saldo_moeda_origem_task = BashOperator(
        task_id='test_not_null_estagios_despesas_empenhadas_controle_empenho_saldo_moeda_origem',
        bash_command='rm -r /tmp/dbt_test_not_null_estagios_despesas_empenhadas_controle_empenho_saldo_moeda_origem || true \
&& cp -r /opt/airflow/dags-config/repo/plugins/dbt /tmp/dbt_test_not_null_estagios_despesas_empenhadas_controle_empenho_saldo_moeda_origem \
&& cd /tmp/dbt_test_not_null_estagios_despesas_empenhadas_controle_empenho_saldo_moeda_origem \
&& dbt deps && dbt test --select not_null_estagios_despesas_empenhadas_controle_empenho_saldo_moeda_origem \
&& rm -r /tmp/dbt_test_not_null_estagios_despesas_empenhadas_controle_empenho_saldo_moeda_origem',
        env={
            'DBT_POSTGRES_HOST': Variable.get("ipea_dw_pg_host"),
            'DBT_POSTGRES_USER': Variable.get("ipea_dw_pg_user"),
            'DBT_POSTGRES_PASSWORD': Variable.get("ipea_dw_pg_password"),
            'DBT_POSTGRES_ENVIRONMENT': Variable.get("ipea_dw_pg_environment"),
            'DBT_POSTGRES_PORT': Variable.get("ipea_dw_pg_port"),
            'DBT_POSTGRES_DATABASE': Variable.get("ipea_dw_pg_db"),
        },
        append_env=True
    )

    estagios_task >> not_null_estagios__NE_CCor__task >> end_task
    estagios_task >> not_null_estagios__NE_N_m_Processo__task >> end_task
    estagios_task >> not_null_estagios__Natureza_Despesa_1__task >> end_task
    estagios_task >> not_null_estagios__Natureza_Despesa_Detalhada_1__task >> end_task
    estagios_task >> not_null_estagios__NE_CCor_Favorecido__task >> end_task
    estagios_task >> not_null_estagios_column8_task >> end_task
    estagios_task >> not_null_estagios__NE_CCor_M_s_Emiss_o__task >> end_task
    estagios_task >> not_null_estagios__M_s_Lan_amento__task >> end_task
    estagios_task >> not_null_estagios_despesas_empenhadas_controle_empenho_saldo_moeda_origem_task >> end_task
