# fmt: off
# ruff: noqa

from airflow import DAG
from airflow.datasets import Dataset
from airflow.models import Variable
from airflow.operators.bash_operator import BashOperator
from airflow.operators.empty import EmptyOperator
from airflow.utils.dates import days_ago

from datetime import timedelta

default_args = {
    'owner': 'DBT-Genrated',
    'retries': 5,
    'retry_delay': timedelta(minutes=10),
    'pool': 'dbt_pool'
}

with DAG(
    "run_model__faturas",
    default_args=default_args,
    schedule='@daily',
    start_date=days_ago(1),
    tags=["dbt", "model"],
    max_active_runs=1,
    on_success_callback=None,
) as dag:

    end_task = EmptyOperator(
        task_id="end",
        outlets=[Dataset("faturas_model")],
    )

    faturas_task = BashOperator(
        task_id='run_faturas',
        bash_command='rm -r /tmp/dbt_run_faturas || true \
&& cp -r /opt/airflow/dags-config/repo/plugins/dbt /tmp/dbt_run_faturas \
&& cd /tmp/dbt_run_faturas \
&& dbt deps && dbt run --select faturas \
&& rm -r /tmp/dbt_run_faturas',
        env={
            'DBT_POSTGRES_HOST': Variable.get("ipea_dw_pg_host"),
            'DBT_POSTGRES_USER': Variable.get("ipea_dw_pg_user"),
            'DBT_POSTGRES_PASSWORD': Variable.get("ipea_dw_pg_password"),
            'DBT_POSTGRES_ENVIRONMENT': Variable.get("ipea_dw_pg_environment"),
            'DBT_POSTGRES_PORT': Variable.get("ipea_dw_pg_port"),
            'DBT_POSTGRES_DATABASE': Variable.get("ipea_dw_pg_db"),
        },
        append_env=True
    )

    not_null_faturas_id_task = BashOperator(
        task_id='test_not_null_faturas_id',
        bash_command='rm -r /tmp/dbt_test_not_null_faturas_id || true \
&& cp -r /opt/airflow/dags-config/repo/plugins/dbt /tmp/dbt_test_not_null_faturas_id \
&& cd /tmp/dbt_test_not_null_faturas_id \
&& dbt deps && dbt test --select not_null_faturas_id \
&& rm -r /tmp/dbt_test_not_null_faturas_id',
        env={
            'DBT_POSTGRES_HOST': Variable.get("ipea_dw_pg_host"),
            'DBT_POSTGRES_USER': Variable.get("ipea_dw_pg_user"),
            'DBT_POSTGRES_PASSWORD': Variable.get("ipea_dw_pg_password"),
            'DBT_POSTGRES_ENVIRONMENT': Variable.get("ipea_dw_pg_environment"),
            'DBT_POSTGRES_PORT': Variable.get("ipea_dw_pg_port"),
            'DBT_POSTGRES_DATABASE': Variable.get("ipea_dw_pg_db"),
        },
        append_env=True
    )

    unique_faturas_id_task = BashOperator(
        task_id='test_unique_faturas_id',
        bash_command='rm -r /tmp/dbt_test_unique_faturas_id || true \
&& cp -r /opt/airflow/dags-config/repo/plugins/dbt /tmp/dbt_test_unique_faturas_id \
&& cd /tmp/dbt_test_unique_faturas_id \
&& dbt deps && dbt test --select unique_faturas_id \
&& rm -r /tmp/dbt_test_unique_faturas_id',
        env={
            'DBT_POSTGRES_HOST': Variable.get("ipea_dw_pg_host"),
            'DBT_POSTGRES_USER': Variable.get("ipea_dw_pg_user"),
            'DBT_POSTGRES_PASSWORD': Variable.get("ipea_dw_pg_password"),
            'DBT_POSTGRES_ENVIRONMENT': Variable.get("ipea_dw_pg_environment"),
            'DBT_POSTGRES_PORT': Variable.get("ipea_dw_pg_port"),
            'DBT_POSTGRES_DATABASE': Variable.get("ipea_dw_pg_db"),
        },
        append_env=True
    )

    not_null_faturas_contrato_id_task = BashOperator(
        task_id='test_not_null_faturas_contrato_id',
        bash_command='rm -r /tmp/dbt_test_not_null_faturas_contrato_id || true \
&& cp -r /opt/airflow/dags-config/repo/plugins/dbt /tmp/dbt_test_not_null_faturas_contrato_id \
&& cd /tmp/dbt_test_not_null_faturas_contrato_id \
&& dbt deps && dbt test --select not_null_faturas_contrato_id \
&& rm -r /tmp/dbt_test_not_null_faturas_contrato_id',
        env={
            'DBT_POSTGRES_HOST': Variable.get("ipea_dw_pg_host"),
            'DBT_POSTGRES_USER': Variable.get("ipea_dw_pg_user"),
            'DBT_POSTGRES_PASSWORD': Variable.get("ipea_dw_pg_password"),
            'DBT_POSTGRES_ENVIRONMENT': Variable.get("ipea_dw_pg_environment"),
            'DBT_POSTGRES_PORT': Variable.get("ipea_dw_pg_port"),
            'DBT_POSTGRES_DATABASE': Variable.get("ipea_dw_pg_db"),
        },
        append_env=True
    )

    not_null_faturas_tipolistafatura_id_task = BashOperator(
        task_id='test_not_null_faturas_tipolistafatura_id',
        bash_command='rm -r /tmp/dbt_test_not_null_faturas_tipolistafatura_id || true \
&& cp -r /opt/airflow/dags-config/repo/plugins/dbt /tmp/dbt_test_not_null_faturas_tipolistafatura_id \
&& cd /tmp/dbt_test_not_null_faturas_tipolistafatura_id \
&& dbt deps && dbt test --select not_null_faturas_tipolistafatura_id \
&& rm -r /tmp/dbt_test_not_null_faturas_tipolistafatura_id',
        env={
            'DBT_POSTGRES_HOST': Variable.get("ipea_dw_pg_host"),
            'DBT_POSTGRES_USER': Variable.get("ipea_dw_pg_user"),
            'DBT_POSTGRES_PASSWORD': Variable.get("ipea_dw_pg_password"),
            'DBT_POSTGRES_ENVIRONMENT': Variable.get("ipea_dw_pg_environment"),
            'DBT_POSTGRES_PORT': Variable.get("ipea_dw_pg_port"),
            'DBT_POSTGRES_DATABASE': Variable.get("ipea_dw_pg_db"),
        },
        append_env=True
    )

    accepted_values_faturas_tipolistafatura_id__PRESTA_O_DE_SERVI_OS__FORNECIMENTO_DE_BENS__LOCA_ES__REALIZA_O_DE_OBRAS_task = BashOperator(
        task_id='test_accepted_values_faturas_tipolistafatura_id__PRESTA_O_DE_SERVI_OS__FORNECIMENTO_DE_BENS__LOCA_ES__REALIZA_O_DE_OBRAS',
        bash_command='rm -r /tmp/dbt_test_accepted_values_faturas_tipolistafatura_id__PRESTA_O_DE_SERVI_OS__FORNECIMENTO_DE_BENS__LOCA_ES__REALIZA_O_DE_OBRAS || true \
&& cp -r /opt/airflow/dags-config/repo/plugins/dbt /tmp/dbt_test_accepted_values_faturas_tipolistafatura_id__PRESTA_O_DE_SERVI_OS__FORNECIMENTO_DE_BENS__LOCA_ES__REALIZA_O_DE_OBRAS \
&& cd /tmp/dbt_test_accepted_values_faturas_tipolistafatura_id__PRESTA_O_DE_SERVI_OS__FORNECIMENTO_DE_BENS__LOCA_ES__REALIZA_O_DE_OBRAS \
&& dbt deps && dbt test --select accepted_values_faturas_tipolistafatura_id__PRESTA_O_DE_SERVI_OS__FORNECIMENTO_DE_BENS__LOCA_ES__REALIZA_O_DE_OBRAS \
&& rm -r /tmp/dbt_test_accepted_values_faturas_tipolistafatura_id__PRESTA_O_DE_SERVI_OS__FORNECIMENTO_DE_BENS__LOCA_ES__REALIZA_O_DE_OBRAS',
        env={
            'DBT_POSTGRES_HOST': Variable.get("ipea_dw_pg_host"),
            'DBT_POSTGRES_USER': Variable.get("ipea_dw_pg_user"),
            'DBT_POSTGRES_PASSWORD': Variable.get("ipea_dw_pg_password"),
            'DBT_POSTGRES_ENVIRONMENT': Variable.get("ipea_dw_pg_environment"),
            'DBT_POSTGRES_PORT': Variable.get("ipea_dw_pg_port"),
            'DBT_POSTGRES_DATABASE': Variable.get("ipea_dw_pg_db"),
        },
        append_env=True
    )

    not_null_faturas_numero_task = BashOperator(
        task_id='test_not_null_faturas_numero',
        bash_command='rm -r /tmp/dbt_test_not_null_faturas_numero || true \
&& cp -r /opt/airflow/dags-config/repo/plugins/dbt /tmp/dbt_test_not_null_faturas_numero \
&& cd /tmp/dbt_test_not_null_faturas_numero \
&& dbt deps && dbt test --select not_null_faturas_numero \
&& rm -r /tmp/dbt_test_not_null_faturas_numero',
        env={
            'DBT_POSTGRES_HOST': Variable.get("ipea_dw_pg_host"),
            'DBT_POSTGRES_USER': Variable.get("ipea_dw_pg_user"),
            'DBT_POSTGRES_PASSWORD': Variable.get("ipea_dw_pg_password"),
            'DBT_POSTGRES_ENVIRONMENT': Variable.get("ipea_dw_pg_environment"),
            'DBT_POSTGRES_PORT': Variable.get("ipea_dw_pg_port"),
            'DBT_POSTGRES_DATABASE': Variable.get("ipea_dw_pg_db"),
        },
        append_env=True
    )

    not_null_faturas_prazo_task = BashOperator(
        task_id='test_not_null_faturas_prazo',
        bash_command='rm -r /tmp/dbt_test_not_null_faturas_prazo || true \
&& cp -r /opt/airflow/dags-config/repo/plugins/dbt /tmp/dbt_test_not_null_faturas_prazo \
&& cd /tmp/dbt_test_not_null_faturas_prazo \
&& dbt deps && dbt test --select not_null_faturas_prazo \
&& rm -r /tmp/dbt_test_not_null_faturas_prazo',
        env={
            'DBT_POSTGRES_HOST': Variable.get("ipea_dw_pg_host"),
            'DBT_POSTGRES_USER': Variable.get("ipea_dw_pg_user"),
            'DBT_POSTGRES_PASSWORD': Variable.get("ipea_dw_pg_password"),
            'DBT_POSTGRES_ENVIRONMENT': Variable.get("ipea_dw_pg_environment"),
            'DBT_POSTGRES_PORT': Variable.get("ipea_dw_pg_port"),
            'DBT_POSTGRES_DATABASE': Variable.get("ipea_dw_pg_db"),
        },
        append_env=True
    )

    not_null_faturas_vencimento_task = BashOperator(
        task_id='test_not_null_faturas_vencimento',
        bash_command='rm -r /tmp/dbt_test_not_null_faturas_vencimento || true \
&& cp -r /opt/airflow/dags-config/repo/plugins/dbt /tmp/dbt_test_not_null_faturas_vencimento \
&& cd /tmp/dbt_test_not_null_faturas_vencimento \
&& dbt deps && dbt test --select not_null_faturas_vencimento \
&& rm -r /tmp/dbt_test_not_null_faturas_vencimento',
        env={
            'DBT_POSTGRES_HOST': Variable.get("ipea_dw_pg_host"),
            'DBT_POSTGRES_USER': Variable.get("ipea_dw_pg_user"),
            'DBT_POSTGRES_PASSWORD': Variable.get("ipea_dw_pg_password"),
            'DBT_POSTGRES_ENVIRONMENT': Variable.get("ipea_dw_pg_environment"),
            'DBT_POSTGRES_PORT': Variable.get("ipea_dw_pg_port"),
            'DBT_POSTGRES_DATABASE': Variable.get("ipea_dw_pg_db"),
        },
        append_env=True
    )

    not_null_faturas_valor_task = BashOperator(
        task_id='test_not_null_faturas_valor',
        bash_command='rm -r /tmp/dbt_test_not_null_faturas_valor || true \
&& cp -r /opt/airflow/dags-config/repo/plugins/dbt /tmp/dbt_test_not_null_faturas_valor \
&& cd /tmp/dbt_test_not_null_faturas_valor \
&& dbt deps && dbt test --select not_null_faturas_valor \
&& rm -r /tmp/dbt_test_not_null_faturas_valor',
        env={
            'DBT_POSTGRES_HOST': Variable.get("ipea_dw_pg_host"),
            'DBT_POSTGRES_USER': Variable.get("ipea_dw_pg_user"),
            'DBT_POSTGRES_PASSWORD': Variable.get("ipea_dw_pg_password"),
            'DBT_POSTGRES_ENVIRONMENT': Variable.get("ipea_dw_pg_environment"),
            'DBT_POSTGRES_PORT': Variable.get("ipea_dw_pg_port"),
            'DBT_POSTGRES_DATABASE': Variable.get("ipea_dw_pg_db"),
        },
        append_env=True
    )

    not_null_faturas_juros_task = BashOperator(
        task_id='test_not_null_faturas_juros',
        bash_command='rm -r /tmp/dbt_test_not_null_faturas_juros || true \
&& cp -r /opt/airflow/dags-config/repo/plugins/dbt /tmp/dbt_test_not_null_faturas_juros \
&& cd /tmp/dbt_test_not_null_faturas_juros \
&& dbt deps && dbt test --select not_null_faturas_juros \
&& rm -r /tmp/dbt_test_not_null_faturas_juros',
        env={
            'DBT_POSTGRES_HOST': Variable.get("ipea_dw_pg_host"),
            'DBT_POSTGRES_USER': Variable.get("ipea_dw_pg_user"),
            'DBT_POSTGRES_PASSWORD': Variable.get("ipea_dw_pg_password"),
            'DBT_POSTGRES_ENVIRONMENT': Variable.get("ipea_dw_pg_environment"),
            'DBT_POSTGRES_PORT': Variable.get("ipea_dw_pg_port"),
            'DBT_POSTGRES_DATABASE': Variable.get("ipea_dw_pg_db"),
        },
        append_env=True
    )

    not_null_faturas_multa_task = BashOperator(
        task_id='test_not_null_faturas_multa',
        bash_command='rm -r /tmp/dbt_test_not_null_faturas_multa || true \
&& cp -r /opt/airflow/dags-config/repo/plugins/dbt /tmp/dbt_test_not_null_faturas_multa \
&& cd /tmp/dbt_test_not_null_faturas_multa \
&& dbt deps && dbt test --select not_null_faturas_multa \
&& rm -r /tmp/dbt_test_not_null_faturas_multa',
        env={
            'DBT_POSTGRES_HOST': Variable.get("ipea_dw_pg_host"),
            'DBT_POSTGRES_USER': Variable.get("ipea_dw_pg_user"),
            'DBT_POSTGRES_PASSWORD': Variable.get("ipea_dw_pg_password"),
            'DBT_POSTGRES_ENVIRONMENT': Variable.get("ipea_dw_pg_environment"),
            'DBT_POSTGRES_PORT': Variable.get("ipea_dw_pg_port"),
            'DBT_POSTGRES_DATABASE': Variable.get("ipea_dw_pg_db"),
        },
        append_env=True
    )

    not_null_faturas_glosa_task = BashOperator(
        task_id='test_not_null_faturas_glosa',
        bash_command='rm -r /tmp/dbt_test_not_null_faturas_glosa || true \
&& cp -r /opt/airflow/dags-config/repo/plugins/dbt /tmp/dbt_test_not_null_faturas_glosa \
&& cd /tmp/dbt_test_not_null_faturas_glosa \
&& dbt deps && dbt test --select not_null_faturas_glosa \
&& rm -r /tmp/dbt_test_not_null_faturas_glosa',
        env={
            'DBT_POSTGRES_HOST': Variable.get("ipea_dw_pg_host"),
            'DBT_POSTGRES_USER': Variable.get("ipea_dw_pg_user"),
            'DBT_POSTGRES_PASSWORD': Variable.get("ipea_dw_pg_password"),
            'DBT_POSTGRES_ENVIRONMENT': Variable.get("ipea_dw_pg_environment"),
            'DBT_POSTGRES_PORT': Variable.get("ipea_dw_pg_port"),
            'DBT_POSTGRES_DATABASE': Variable.get("ipea_dw_pg_db"),
        },
        append_env=True
    )

    not_null_faturas_valorliquido_task = BashOperator(
        task_id='test_not_null_faturas_valorliquido',
        bash_command='rm -r /tmp/dbt_test_not_null_faturas_valorliquido || true \
&& cp -r /opt/airflow/dags-config/repo/plugins/dbt /tmp/dbt_test_not_null_faturas_valorliquido \
&& cd /tmp/dbt_test_not_null_faturas_valorliquido \
&& dbt deps && dbt test --select not_null_faturas_valorliquido \
&& rm -r /tmp/dbt_test_not_null_faturas_valorliquido',
        env={
            'DBT_POSTGRES_HOST': Variable.get("ipea_dw_pg_host"),
            'DBT_POSTGRES_USER': Variable.get("ipea_dw_pg_user"),
            'DBT_POSTGRES_PASSWORD': Variable.get("ipea_dw_pg_password"),
            'DBT_POSTGRES_ENVIRONMENT': Variable.get("ipea_dw_pg_environment"),
            'DBT_POSTGRES_PORT': Variable.get("ipea_dw_pg_port"),
            'DBT_POSTGRES_DATABASE': Variable.get("ipea_dw_pg_db"),
        },
        append_env=True
    )

    not_null_faturas_processo_task = BashOperator(
        task_id='test_not_null_faturas_processo',
        bash_command='rm -r /tmp/dbt_test_not_null_faturas_processo || true \
&& cp -r /opt/airflow/dags-config/repo/plugins/dbt /tmp/dbt_test_not_null_faturas_processo \
&& cd /tmp/dbt_test_not_null_faturas_processo \
&& dbt deps && dbt test --select not_null_faturas_processo \
&& rm -r /tmp/dbt_test_not_null_faturas_processo',
        env={
            'DBT_POSTGRES_HOST': Variable.get("ipea_dw_pg_host"),
            'DBT_POSTGRES_USER': Variable.get("ipea_dw_pg_user"),
            'DBT_POSTGRES_PASSWORD': Variable.get("ipea_dw_pg_password"),
            'DBT_POSTGRES_ENVIRONMENT': Variable.get("ipea_dw_pg_environment"),
            'DBT_POSTGRES_PORT': Variable.get("ipea_dw_pg_port"),
            'DBT_POSTGRES_DATABASE': Variable.get("ipea_dw_pg_db"),
        },
        append_env=True
    )

    not_null_faturas_protocolo_task = BashOperator(
        task_id='test_not_null_faturas_protocolo',
        bash_command='rm -r /tmp/dbt_test_not_null_faturas_protocolo || true \
&& cp -r /opt/airflow/dags-config/repo/plugins/dbt /tmp/dbt_test_not_null_faturas_protocolo \
&& cd /tmp/dbt_test_not_null_faturas_protocolo \
&& dbt deps && dbt test --select not_null_faturas_protocolo \
&& rm -r /tmp/dbt_test_not_null_faturas_protocolo',
        env={
            'DBT_POSTGRES_HOST': Variable.get("ipea_dw_pg_host"),
            'DBT_POSTGRES_USER': Variable.get("ipea_dw_pg_user"),
            'DBT_POSTGRES_PASSWORD': Variable.get("ipea_dw_pg_password"),
            'DBT_POSTGRES_ENVIRONMENT': Variable.get("ipea_dw_pg_environment"),
            'DBT_POSTGRES_PORT': Variable.get("ipea_dw_pg_port"),
            'DBT_POSTGRES_DATABASE': Variable.get("ipea_dw_pg_db"),
        },
        append_env=True
    )

    not_null_faturas_ateste_task = BashOperator(
        task_id='test_not_null_faturas_ateste',
        bash_command='rm -r /tmp/dbt_test_not_null_faturas_ateste || true \
&& cp -r /opt/airflow/dags-config/repo/plugins/dbt /tmp/dbt_test_not_null_faturas_ateste \
&& cd /tmp/dbt_test_not_null_faturas_ateste \
&& dbt deps && dbt test --select not_null_faturas_ateste \
&& rm -r /tmp/dbt_test_not_null_faturas_ateste',
        env={
            'DBT_POSTGRES_HOST': Variable.get("ipea_dw_pg_host"),
            'DBT_POSTGRES_USER': Variable.get("ipea_dw_pg_user"),
            'DBT_POSTGRES_PASSWORD': Variable.get("ipea_dw_pg_password"),
            'DBT_POSTGRES_ENVIRONMENT': Variable.get("ipea_dw_pg_environment"),
            'DBT_POSTGRES_PORT': Variable.get("ipea_dw_pg_port"),
            'DBT_POSTGRES_DATABASE': Variable.get("ipea_dw_pg_db"),
        },
        append_env=True
    )

    not_null_faturas_repactuacao_task = BashOperator(
        task_id='test_not_null_faturas_repactuacao',
        bash_command='rm -r /tmp/dbt_test_not_null_faturas_repactuacao || true \
&& cp -r /opt/airflow/dags-config/repo/plugins/dbt /tmp/dbt_test_not_null_faturas_repactuacao \
&& cd /tmp/dbt_test_not_null_faturas_repactuacao \
&& dbt deps && dbt test --select not_null_faturas_repactuacao \
&& rm -r /tmp/dbt_test_not_null_faturas_repactuacao',
        env={
            'DBT_POSTGRES_HOST': Variable.get("ipea_dw_pg_host"),
            'DBT_POSTGRES_USER': Variable.get("ipea_dw_pg_user"),
            'DBT_POSTGRES_PASSWORD': Variable.get("ipea_dw_pg_password"),
            'DBT_POSTGRES_ENVIRONMENT': Variable.get("ipea_dw_pg_environment"),
            'DBT_POSTGRES_PORT': Variable.get("ipea_dw_pg_port"),
            'DBT_POSTGRES_DATABASE': Variable.get("ipea_dw_pg_db"),
        },
        append_env=True
    )

    accepted_values_faturas_repactuacao__N_o__Sim_task = BashOperator(
        task_id='test_accepted_values_faturas_repactuacao__N_o__Sim',
        bash_command='rm -r /tmp/dbt_test_accepted_values_faturas_repactuacao__N_o__Sim || true \
&& cp -r /opt/airflow/dags-config/repo/plugins/dbt /tmp/dbt_test_accepted_values_faturas_repactuacao__N_o__Sim \
&& cd /tmp/dbt_test_accepted_values_faturas_repactuacao__N_o__Sim \
&& dbt deps && dbt test --select accepted_values_faturas_repactuacao__N_o__Sim \
&& rm -r /tmp/dbt_test_accepted_values_faturas_repactuacao__N_o__Sim',
        env={
            'DBT_POSTGRES_HOST': Variable.get("ipea_dw_pg_host"),
            'DBT_POSTGRES_USER': Variable.get("ipea_dw_pg_user"),
            'DBT_POSTGRES_PASSWORD': Variable.get("ipea_dw_pg_password"),
            'DBT_POSTGRES_ENVIRONMENT': Variable.get("ipea_dw_pg_environment"),
            'DBT_POSTGRES_PORT': Variable.get("ipea_dw_pg_port"),
            'DBT_POSTGRES_DATABASE': Variable.get("ipea_dw_pg_db"),
        },
        append_env=True
    )

    not_null_faturas_situacao_task = BashOperator(
        task_id='test_not_null_faturas_situacao',
        bash_command='rm -r /tmp/dbt_test_not_null_faturas_situacao || true \
&& cp -r /opt/airflow/dags-config/repo/plugins/dbt /tmp/dbt_test_not_null_faturas_situacao \
&& cd /tmp/dbt_test_not_null_faturas_situacao \
&& dbt deps && dbt test --select not_null_faturas_situacao \
&& rm -r /tmp/dbt_test_not_null_faturas_situacao',
        env={
            'DBT_POSTGRES_HOST': Variable.get("ipea_dw_pg_host"),
            'DBT_POSTGRES_USER': Variable.get("ipea_dw_pg_user"),
            'DBT_POSTGRES_PASSWORD': Variable.get("ipea_dw_pg_password"),
            'DBT_POSTGRES_ENVIRONMENT': Variable.get("ipea_dw_pg_environment"),
            'DBT_POSTGRES_PORT': Variable.get("ipea_dw_pg_port"),
            'DBT_POSTGRES_DATABASE': Variable.get("ipea_dw_pg_db"),
        },
        append_env=True
    )

    accepted_values_faturas_situacao__Pago__Pendente__Aproriacao_em_Andamento_task = BashOperator(
        task_id='test_accepted_values_faturas_situacao__Pago__Pendente__Aproriacao_em_Andamento',
        bash_command='rm -r /tmp/dbt_test_accepted_values_faturas_situacao__Pago__Pendente__Aproriacao_em_Andamento || true \
&& cp -r /opt/airflow/dags-config/repo/plugins/dbt /tmp/dbt_test_accepted_values_faturas_situacao__Pago__Pendente__Aproriacao_em_Andamento \
&& cd /tmp/dbt_test_accepted_values_faturas_situacao__Pago__Pendente__Aproriacao_em_Andamento \
&& dbt deps && dbt test --select accepted_values_faturas_situacao__Pago__Pendente__Aproriacao_em_Andamento \
&& rm -r /tmp/dbt_test_accepted_values_faturas_situacao__Pago__Pendente__Aproriacao_em_Andamento',
        env={
            'DBT_POSTGRES_HOST': Variable.get("ipea_dw_pg_host"),
            'DBT_POSTGRES_USER': Variable.get("ipea_dw_pg_user"),
            'DBT_POSTGRES_PASSWORD': Variable.get("ipea_dw_pg_password"),
            'DBT_POSTGRES_ENVIRONMENT': Variable.get("ipea_dw_pg_environment"),
            'DBT_POSTGRES_PORT': Variable.get("ipea_dw_pg_port"),
            'DBT_POSTGRES_DATABASE': Variable.get("ipea_dw_pg_db"),
        },
        append_env=True
    )

    faturas_task >> not_null_faturas_id_task >> end_task
    faturas_task >> unique_faturas_id_task >> end_task
    faturas_task >> not_null_faturas_contrato_id_task >> end_task
    faturas_task >> not_null_faturas_tipolistafatura_id_task >> end_task
    faturas_task >> accepted_values_faturas_tipolistafatura_id__PRESTA_O_DE_SERVI_OS__FORNECIMENTO_DE_BENS__LOCA_ES__REALIZA_O_DE_OBRAS_task >> end_task
    faturas_task >> not_null_faturas_numero_task >> end_task
    faturas_task >> not_null_faturas_prazo_task >> end_task
    faturas_task >> not_null_faturas_vencimento_task >> end_task
    faturas_task >> not_null_faturas_valor_task >> end_task
    faturas_task >> not_null_faturas_juros_task >> end_task
    faturas_task >> not_null_faturas_multa_task >> end_task
    faturas_task >> not_null_faturas_glosa_task >> end_task
    faturas_task >> not_null_faturas_valorliquido_task >> end_task
    faturas_task >> not_null_faturas_processo_task >> end_task
    faturas_task >> not_null_faturas_protocolo_task >> end_task
    faturas_task >> not_null_faturas_ateste_task >> end_task
    faturas_task >> not_null_faturas_repactuacao_task >> end_task
    faturas_task >> accepted_values_faturas_repactuacao__N_o__Sim_task >> end_task
    faturas_task >> not_null_faturas_situacao_task >> end_task
    faturas_task >> accepted_values_faturas_situacao__Pago__Pendente__Aproriacao_em_Andamento_task >> end_task
