# fmt: off
# ruff: noqa

from airflow import DAG
from airflow.datasets import Dataset
from airflow.models import Variable
from airflow.operators.bash_operator import BashOperator
from airflow.operators.empty import EmptyOperator
from airflow.utils.dates import days_ago

from datetime import timedelta

default_args = {
    'owner': 'DBT-Genrated',
    'retries': 5,
    'retry_delay': timedelta(minutes=10),
    'pool': 'dbt_pool'
}

with DAG(
    "run_model__contratos",
    default_args=default_args,
    schedule='@daily',
    start_date=days_ago(1),
    tags=["dbt", "model"],
    max_active_runs=1,
    on_success_callback=None,
) as dag:

    end_task = EmptyOperator(
        task_id="end",
        outlets=[Dataset("contratos_model")],
    )

    contratos_task = BashOperator(
        task_id='run_contratos',
        bash_command='rm -r /tmp/dbt_run_contratos || true \
&& cp -r /opt/airflow/dags-config/repo/plugins/dbt /tmp/dbt_run_contratos \
&& cd /tmp/dbt_run_contratos \
&& dbt deps && dbt run --select contratos \
&& rm -r /tmp/dbt_run_contratos',
        env={
            'DBT_POSTGRES_HOST': Variable.get("ipea_dw_pg_host"),
            'DBT_POSTGRES_USER': Variable.get("ipea_dw_pg_user"),
            'DBT_POSTGRES_PASSWORD': Variable.get("ipea_dw_pg_password"),
            'DBT_POSTGRES_ENVIRONMENT': Variable.get("ipea_dw_pg_environment"),
            'DBT_POSTGRES_PORT': Variable.get("ipea_dw_pg_port"),
            'DBT_POSTGRES_DATABASE': Variable.get("ipea_dw_pg_db"),
        },
        append_env=True
    )

    not_null_contratos_id_task = BashOperator(
        task_id='test_not_null_contratos_id',
        bash_command='rm -r /tmp/dbt_test_not_null_contratos_id || true \
&& cp -r /opt/airflow/dags-config/repo/plugins/dbt /tmp/dbt_test_not_null_contratos_id \
&& cd /tmp/dbt_test_not_null_contratos_id \
&& dbt deps && dbt test --select not_null_contratos_id \
&& rm -r /tmp/dbt_test_not_null_contratos_id',
        env={
            'DBT_POSTGRES_HOST': Variable.get("ipea_dw_pg_host"),
            'DBT_POSTGRES_USER': Variable.get("ipea_dw_pg_user"),
            'DBT_POSTGRES_PASSWORD': Variable.get("ipea_dw_pg_password"),
            'DBT_POSTGRES_ENVIRONMENT': Variable.get("ipea_dw_pg_environment"),
            'DBT_POSTGRES_PORT': Variable.get("ipea_dw_pg_port"),
            'DBT_POSTGRES_DATABASE': Variable.get("ipea_dw_pg_db"),
        },
        append_env=True
    )

    unique_contratos_id_task = BashOperator(
        task_id='test_unique_contratos_id',
        bash_command='rm -r /tmp/dbt_test_unique_contratos_id || true \
&& cp -r /opt/airflow/dags-config/repo/plugins/dbt /tmp/dbt_test_unique_contratos_id \
&& cd /tmp/dbt_test_unique_contratos_id \
&& dbt deps && dbt test --select unique_contratos_id \
&& rm -r /tmp/dbt_test_unique_contratos_id',
        env={
            'DBT_POSTGRES_HOST': Variable.get("ipea_dw_pg_host"),
            'DBT_POSTGRES_USER': Variable.get("ipea_dw_pg_user"),
            'DBT_POSTGRES_PASSWORD': Variable.get("ipea_dw_pg_password"),
            'DBT_POSTGRES_ENVIRONMENT': Variable.get("ipea_dw_pg_environment"),
            'DBT_POSTGRES_PORT': Variable.get("ipea_dw_pg_port"),
            'DBT_POSTGRES_DATABASE': Variable.get("ipea_dw_pg_db"),
        },
        append_env=True
    )

    not_null_contratos_receita_despesa_task = BashOperator(
        task_id='test_not_null_contratos_receita_despesa',
        bash_command='rm -r /tmp/dbt_test_not_null_contratos_receita_despesa || true \
&& cp -r /opt/airflow/dags-config/repo/plugins/dbt /tmp/dbt_test_not_null_contratos_receita_despesa \
&& cd /tmp/dbt_test_not_null_contratos_receita_despesa \
&& dbt deps && dbt test --select not_null_contratos_receita_despesa \
&& rm -r /tmp/dbt_test_not_null_contratos_receita_despesa',
        env={
            'DBT_POSTGRES_HOST': Variable.get("ipea_dw_pg_host"),
            'DBT_POSTGRES_USER': Variable.get("ipea_dw_pg_user"),
            'DBT_POSTGRES_PASSWORD': Variable.get("ipea_dw_pg_password"),
            'DBT_POSTGRES_ENVIRONMENT': Variable.get("ipea_dw_pg_environment"),
            'DBT_POSTGRES_PORT': Variable.get("ipea_dw_pg_port"),
            'DBT_POSTGRES_DATABASE': Variable.get("ipea_dw_pg_db"),
        },
        append_env=True
    )

    not_null_contratos_numero_task = BashOperator(
        task_id='test_not_null_contratos_numero',
        bash_command='rm -r /tmp/dbt_test_not_null_contratos_numero || true \
&& cp -r /opt/airflow/dags-config/repo/plugins/dbt /tmp/dbt_test_not_null_contratos_numero \
&& cd /tmp/dbt_test_not_null_contratos_numero \
&& dbt deps && dbt test --select not_null_contratos_numero \
&& rm -r /tmp/dbt_test_not_null_contratos_numero',
        env={
            'DBT_POSTGRES_HOST': Variable.get("ipea_dw_pg_host"),
            'DBT_POSTGRES_USER': Variable.get("ipea_dw_pg_user"),
            'DBT_POSTGRES_PASSWORD': Variable.get("ipea_dw_pg_password"),
            'DBT_POSTGRES_ENVIRONMENT': Variable.get("ipea_dw_pg_environment"),
            'DBT_POSTGRES_PORT': Variable.get("ipea_dw_pg_port"),
            'DBT_POSTGRES_DATABASE': Variable.get("ipea_dw_pg_db"),
        },
        append_env=True
    )

    not_null_contratos_contratante_orgao_origem_codigo_task = BashOperator(
        task_id='test_not_null_contratos_contratante_orgao_origem_codigo',
        bash_command='rm -r /tmp/dbt_test_not_null_contratos_contratante_orgao_origem_codigo || true \
&& cp -r /opt/airflow/dags-config/repo/plugins/dbt /tmp/dbt_test_not_null_contratos_contratante_orgao_origem_codigo \
&& cd /tmp/dbt_test_not_null_contratos_contratante_orgao_origem_codigo \
&& dbt deps && dbt test --select not_null_contratos_contratante_orgao_origem_codigo \
&& rm -r /tmp/dbt_test_not_null_contratos_contratante_orgao_origem_codigo',
        env={
            'DBT_POSTGRES_HOST': Variable.get("ipea_dw_pg_host"),
            'DBT_POSTGRES_USER': Variable.get("ipea_dw_pg_user"),
            'DBT_POSTGRES_PASSWORD': Variable.get("ipea_dw_pg_password"),
            'DBT_POSTGRES_ENVIRONMENT': Variable.get("ipea_dw_pg_environment"),
            'DBT_POSTGRES_PORT': Variable.get("ipea_dw_pg_port"),
            'DBT_POSTGRES_DATABASE': Variable.get("ipea_dw_pg_db"),
        },
        append_env=True
    )

    not_null_contratos_contratante_orgao_origem_nome_task = BashOperator(
        task_id='test_not_null_contratos_contratante_orgao_origem_nome',
        bash_command='rm -r /tmp/dbt_test_not_null_contratos_contratante_orgao_origem_nome || true \
&& cp -r /opt/airflow/dags-config/repo/plugins/dbt /tmp/dbt_test_not_null_contratos_contratante_orgao_origem_nome \
&& cd /tmp/dbt_test_not_null_contratos_contratante_orgao_origem_nome \
&& dbt deps && dbt test --select not_null_contratos_contratante_orgao_origem_nome \
&& rm -r /tmp/dbt_test_not_null_contratos_contratante_orgao_origem_nome',
        env={
            'DBT_POSTGRES_HOST': Variable.get("ipea_dw_pg_host"),
            'DBT_POSTGRES_USER': Variable.get("ipea_dw_pg_user"),
            'DBT_POSTGRES_PASSWORD': Variable.get("ipea_dw_pg_password"),
            'DBT_POSTGRES_ENVIRONMENT': Variable.get("ipea_dw_pg_environment"),
            'DBT_POSTGRES_PORT': Variable.get("ipea_dw_pg_port"),
            'DBT_POSTGRES_DATABASE': Variable.get("ipea_dw_pg_db"),
        },
        append_env=True
    )

    not_null_contratos_contratante_orgao_origem_unidade_gestora_origem_codigo_task = BashOperator(
        task_id='test_not_null_contratos_contratante_orgao_origem_unidade_gestora_origem_codigo',
        bash_command='rm -r /tmp/dbt_test_not_null_contratos_contratante_orgao_origem_unidade_gestora_origem_codigo || true \
&& cp -r /opt/airflow/dags-config/repo/plugins/dbt /tmp/dbt_test_not_null_contratos_contratante_orgao_origem_unidade_gestora_origem_codigo \
&& cd /tmp/dbt_test_not_null_contratos_contratante_orgao_origem_unidade_gestora_origem_codigo \
&& dbt deps && dbt test --select not_null_contratos_contratante_orgao_origem_unidade_gestora_origem_codigo \
&& rm -r /tmp/dbt_test_not_null_contratos_contratante_orgao_origem_unidade_gestora_origem_codigo',
        env={
            'DBT_POSTGRES_HOST': Variable.get("ipea_dw_pg_host"),
            'DBT_POSTGRES_USER': Variable.get("ipea_dw_pg_user"),
            'DBT_POSTGRES_PASSWORD': Variable.get("ipea_dw_pg_password"),
            'DBT_POSTGRES_ENVIRONMENT': Variable.get("ipea_dw_pg_environment"),
            'DBT_POSTGRES_PORT': Variable.get("ipea_dw_pg_port"),
            'DBT_POSTGRES_DATABASE': Variable.get("ipea_dw_pg_db"),
        },
        append_env=True
    )

    not_null_contratos_contratante_orgao_origem_unidade_gestora_origem_nome_resumido_task = BashOperator(
        task_id='test_not_null_contratos_contratante_orgao_origem_unidade_gestora_origem_nome_resumido',
        bash_command='rm -r /tmp/dbt_test_not_null_contratos_contratante_orgao_origem_unidade_gestora_origem_nome_resumido || true \
&& cp -r /opt/airflow/dags-config/repo/plugins/dbt /tmp/dbt_test_not_null_contratos_contratante_orgao_origem_unidade_gestora_origem_nome_resumido \
&& cd /tmp/dbt_test_not_null_contratos_contratante_orgao_origem_unidade_gestora_origem_nome_resumido \
&& dbt deps && dbt test --select not_null_contratos_contratante_orgao_origem_unidade_gestora_origem_nome_resumido \
&& rm -r /tmp/dbt_test_not_null_contratos_contratante_orgao_origem_unidade_gestora_origem_nome_resumido',
        env={
            'DBT_POSTGRES_HOST': Variable.get("ipea_dw_pg_host"),
            'DBT_POSTGRES_USER': Variable.get("ipea_dw_pg_user"),
            'DBT_POSTGRES_PASSWORD': Variable.get("ipea_dw_pg_password"),
            'DBT_POSTGRES_ENVIRONMENT': Variable.get("ipea_dw_pg_environment"),
            'DBT_POSTGRES_PORT': Variable.get("ipea_dw_pg_port"),
            'DBT_POSTGRES_DATABASE': Variable.get("ipea_dw_pg_db"),
        },
        append_env=True
    )

    not_null_contratos_contratante_orgao_origem_unidade_gestora_origem_nome_task = BashOperator(
        task_id='test_not_null_contratos_contratante_orgao_origem_unidade_gestora_origem_nome',
        bash_command='rm -r /tmp/dbt_test_not_null_contratos_contratante_orgao_origem_unidade_gestora_origem_nome || true \
&& cp -r /opt/airflow/dags-config/repo/plugins/dbt /tmp/dbt_test_not_null_contratos_contratante_orgao_origem_unidade_gestora_origem_nome \
&& cd /tmp/dbt_test_not_null_contratos_contratante_orgao_origem_unidade_gestora_origem_nome \
&& dbt deps && dbt test --select not_null_contratos_contratante_orgao_origem_unidade_gestora_origem_nome \
&& rm -r /tmp/dbt_test_not_null_contratos_contratante_orgao_origem_unidade_gestora_origem_nome',
        env={
            'DBT_POSTGRES_HOST': Variable.get("ipea_dw_pg_host"),
            'DBT_POSTGRES_USER': Variable.get("ipea_dw_pg_user"),
            'DBT_POSTGRES_PASSWORD': Variable.get("ipea_dw_pg_password"),
            'DBT_POSTGRES_ENVIRONMENT': Variable.get("ipea_dw_pg_environment"),
            'DBT_POSTGRES_PORT': Variable.get("ipea_dw_pg_port"),
            'DBT_POSTGRES_DATABASE': Variable.get("ipea_dw_pg_db"),
        },
        append_env=True
    )

    not_null_contratos_contratante_orgao_origem_unidade_gestora_origem_sisg_task = BashOperator(
        task_id='test_not_null_contratos_contratante_orgao_origem_unidade_gestora_origem_sisg',
        bash_command='rm -r /tmp/dbt_test_not_null_contratos_contratante_orgao_origem_unidade_gestora_origem_sisg || true \
&& cp -r /opt/airflow/dags-config/repo/plugins/dbt /tmp/dbt_test_not_null_contratos_contratante_orgao_origem_unidade_gestora_origem_sisg \
&& cd /tmp/dbt_test_not_null_contratos_contratante_orgao_origem_unidade_gestora_origem_sisg \
&& dbt deps && dbt test --select not_null_contratos_contratante_orgao_origem_unidade_gestora_origem_sisg \
&& rm -r /tmp/dbt_test_not_null_contratos_contratante_orgao_origem_unidade_gestora_origem_sisg',
        env={
            'DBT_POSTGRES_HOST': Variable.get("ipea_dw_pg_host"),
            'DBT_POSTGRES_USER': Variable.get("ipea_dw_pg_user"),
            'DBT_POSTGRES_PASSWORD': Variable.get("ipea_dw_pg_password"),
            'DBT_POSTGRES_ENVIRONMENT': Variable.get("ipea_dw_pg_environment"),
            'DBT_POSTGRES_PORT': Variable.get("ipea_dw_pg_port"),
            'DBT_POSTGRES_DATABASE': Variable.get("ipea_dw_pg_db"),
        },
        append_env=True
    )

    not_null_contratos_contratante_orgao_origem_unidade_gestora_origem_utiliza_siafi_task = BashOperator(
        task_id='test_not_null_contratos_contratante_orgao_origem_unidade_gestora_origem_utiliza_siafi',
        bash_command='rm -r /tmp/dbt_test_not_null_contratos_contratante_orgao_origem_unidade_gestora_origem_utiliza_siafi || true \
&& cp -r /opt/airflow/dags-config/repo/plugins/dbt /tmp/dbt_test_not_null_contratos_contratante_orgao_origem_unidade_gestora_origem_utiliza_siafi \
&& cd /tmp/dbt_test_not_null_contratos_contratante_orgao_origem_unidade_gestora_origem_utiliza_siafi \
&& dbt deps && dbt test --select not_null_contratos_contratante_orgao_origem_unidade_gestora_origem_utiliza_siafi \
&& rm -r /tmp/dbt_test_not_null_contratos_contratante_orgao_origem_unidade_gestora_origem_utiliza_siafi',
        env={
            'DBT_POSTGRES_HOST': Variable.get("ipea_dw_pg_host"),
            'DBT_POSTGRES_USER': Variable.get("ipea_dw_pg_user"),
            'DBT_POSTGRES_PASSWORD': Variable.get("ipea_dw_pg_password"),
            'DBT_POSTGRES_ENVIRONMENT': Variable.get("ipea_dw_pg_environment"),
            'DBT_POSTGRES_PORT': Variable.get("ipea_dw_pg_port"),
            'DBT_POSTGRES_DATABASE': Variable.get("ipea_dw_pg_db"),
        },
        append_env=True
    )

    not_null_contratos_contratante_orgao_origem_unidade_gestora_origem_utiliza_antecip_task = BashOperator(
        task_id='test_not_null_contratos_contratante_orgao_origem_unidade_gestora_origem_utiliza_antecip',
        bash_command='rm -r /tmp/dbt_test_not_null_contratos_contratante_orgao_origem_unidade_gestora_origem_utiliza_antecip || true \
&& cp -r /opt/airflow/dags-config/repo/plugins/dbt /tmp/dbt_test_not_null_contratos_contratante_orgao_origem_unidade_gestora_origem_utiliza_antecip \
&& cd /tmp/dbt_test_not_null_contratos_contratante_orgao_origem_unidade_gestora_origem_utiliza_antecip \
&& dbt deps && dbt test --select not_null_contratos_contratante_orgao_origem_unidade_gestora_origem_utiliza_antecip \
&& rm -r /tmp/dbt_test_not_null_contratos_contratante_orgao_origem_unidade_gestora_origem_utiliza_antecip',
        env={
            'DBT_POSTGRES_HOST': Variable.get("ipea_dw_pg_host"),
            'DBT_POSTGRES_USER': Variable.get("ipea_dw_pg_user"),
            'DBT_POSTGRES_PASSWORD': Variable.get("ipea_dw_pg_password"),
            'DBT_POSTGRES_ENVIRONMENT': Variable.get("ipea_dw_pg_environment"),
            'DBT_POSTGRES_PORT': Variable.get("ipea_dw_pg_port"),
            'DBT_POSTGRES_DATABASE': Variable.get("ipea_dw_pg_db"),
        },
        append_env=True
    )

    not_null_contratos_codigo_tipo_task = BashOperator(
        task_id='test_not_null_contratos_codigo_tipo',
        bash_command='rm -r /tmp/dbt_test_not_null_contratos_codigo_tipo || true \
&& cp -r /opt/airflow/dags-config/repo/plugins/dbt /tmp/dbt_test_not_null_contratos_codigo_tipo \
&& cd /tmp/dbt_test_not_null_contratos_codigo_tipo \
&& dbt deps && dbt test --select not_null_contratos_codigo_tipo \
&& rm -r /tmp/dbt_test_not_null_contratos_codigo_tipo',
        env={
            'DBT_POSTGRES_HOST': Variable.get("ipea_dw_pg_host"),
            'DBT_POSTGRES_USER': Variable.get("ipea_dw_pg_user"),
            'DBT_POSTGRES_PASSWORD': Variable.get("ipea_dw_pg_password"),
            'DBT_POSTGRES_ENVIRONMENT': Variable.get("ipea_dw_pg_environment"),
            'DBT_POSTGRES_PORT': Variable.get("ipea_dw_pg_port"),
            'DBT_POSTGRES_DATABASE': Variable.get("ipea_dw_pg_db"),
        },
        append_env=True
    )

    not_null_contratos_fornecedor_tipo_task = BashOperator(
        task_id='test_not_null_contratos_fornecedor_tipo',
        bash_command='rm -r /tmp/dbt_test_not_null_contratos_fornecedor_tipo || true \
&& cp -r /opt/airflow/dags-config/repo/plugins/dbt /tmp/dbt_test_not_null_contratos_fornecedor_tipo \
&& cd /tmp/dbt_test_not_null_contratos_fornecedor_tipo \
&& dbt deps && dbt test --select not_null_contratos_fornecedor_tipo \
&& rm -r /tmp/dbt_test_not_null_contratos_fornecedor_tipo',
        env={
            'DBT_POSTGRES_HOST': Variable.get("ipea_dw_pg_host"),
            'DBT_POSTGRES_USER': Variable.get("ipea_dw_pg_user"),
            'DBT_POSTGRES_PASSWORD': Variable.get("ipea_dw_pg_password"),
            'DBT_POSTGRES_ENVIRONMENT': Variable.get("ipea_dw_pg_environment"),
            'DBT_POSTGRES_PORT': Variable.get("ipea_dw_pg_port"),
            'DBT_POSTGRES_DATABASE': Variable.get("ipea_dw_pg_db"),
        },
        append_env=True
    )

    not_null_contratos_categoria_task = BashOperator(
        task_id='test_not_null_contratos_categoria',
        bash_command='rm -r /tmp/dbt_test_not_null_contratos_categoria || true \
&& cp -r /opt/airflow/dags-config/repo/plugins/dbt /tmp/dbt_test_not_null_contratos_categoria \
&& cd /tmp/dbt_test_not_null_contratos_categoria \
&& dbt deps && dbt test --select not_null_contratos_categoria \
&& rm -r /tmp/dbt_test_not_null_contratos_categoria',
        env={
            'DBT_POSTGRES_HOST': Variable.get("ipea_dw_pg_host"),
            'DBT_POSTGRES_USER': Variable.get("ipea_dw_pg_user"),
            'DBT_POSTGRES_PASSWORD': Variable.get("ipea_dw_pg_password"),
            'DBT_POSTGRES_ENVIRONMENT': Variable.get("ipea_dw_pg_environment"),
            'DBT_POSTGRES_PORT': Variable.get("ipea_dw_pg_port"),
            'DBT_POSTGRES_DATABASE': Variable.get("ipea_dw_pg_db"),
        },
        append_env=True
    )

    not_null_contratos_processo_task = BashOperator(
        task_id='test_not_null_contratos_processo',
        bash_command='rm -r /tmp/dbt_test_not_null_contratos_processo || true \
&& cp -r /opt/airflow/dags-config/repo/plugins/dbt /tmp/dbt_test_not_null_contratos_processo \
&& cd /tmp/dbt_test_not_null_contratos_processo \
&& dbt deps && dbt test --select not_null_contratos_processo \
&& rm -r /tmp/dbt_test_not_null_contratos_processo',
        env={
            'DBT_POSTGRES_HOST': Variable.get("ipea_dw_pg_host"),
            'DBT_POSTGRES_USER': Variable.get("ipea_dw_pg_user"),
            'DBT_POSTGRES_PASSWORD': Variable.get("ipea_dw_pg_password"),
            'DBT_POSTGRES_ENVIRONMENT': Variable.get("ipea_dw_pg_environment"),
            'DBT_POSTGRES_PORT': Variable.get("ipea_dw_pg_port"),
            'DBT_POSTGRES_DATABASE': Variable.get("ipea_dw_pg_db"),
        },
        append_env=True
    )

    not_null_contratos_objeto_task = BashOperator(
        task_id='test_not_null_contratos_objeto',
        bash_command='rm -r /tmp/dbt_test_not_null_contratos_objeto || true \
&& cp -r /opt/airflow/dags-config/repo/plugins/dbt /tmp/dbt_test_not_null_contratos_objeto \
&& cd /tmp/dbt_test_not_null_contratos_objeto \
&& dbt deps && dbt test --select not_null_contratos_objeto \
&& rm -r /tmp/dbt_test_not_null_contratos_objeto',
        env={
            'DBT_POSTGRES_HOST': Variable.get("ipea_dw_pg_host"),
            'DBT_POSTGRES_USER': Variable.get("ipea_dw_pg_user"),
            'DBT_POSTGRES_PASSWORD': Variable.get("ipea_dw_pg_password"),
            'DBT_POSTGRES_ENVIRONMENT': Variable.get("ipea_dw_pg_environment"),
            'DBT_POSTGRES_PORT': Variable.get("ipea_dw_pg_port"),
            'DBT_POSTGRES_DATABASE': Variable.get("ipea_dw_pg_db"),
        },
        append_env=True
    )

    not_null_contratos_codigo_modalidade_task = BashOperator(
        task_id='test_not_null_contratos_codigo_modalidade',
        bash_command='rm -r /tmp/dbt_test_not_null_contratos_codigo_modalidade || true \
&& cp -r /opt/airflow/dags-config/repo/plugins/dbt /tmp/dbt_test_not_null_contratos_codigo_modalidade \
&& cd /tmp/dbt_test_not_null_contratos_codigo_modalidade \
&& dbt deps && dbt test --select not_null_contratos_codigo_modalidade \
&& rm -r /tmp/dbt_test_not_null_contratos_codigo_modalidade',
        env={
            'DBT_POSTGRES_HOST': Variable.get("ipea_dw_pg_host"),
            'DBT_POSTGRES_USER': Variable.get("ipea_dw_pg_user"),
            'DBT_POSTGRES_PASSWORD': Variable.get("ipea_dw_pg_password"),
            'DBT_POSTGRES_ENVIRONMENT': Variable.get("ipea_dw_pg_environment"),
            'DBT_POSTGRES_PORT': Variable.get("ipea_dw_pg_port"),
            'DBT_POSTGRES_DATABASE': Variable.get("ipea_dw_pg_db"),
        },
        append_env=True
    )

    not_null_contratos_modalidade_task = BashOperator(
        task_id='test_not_null_contratos_modalidade',
        bash_command='rm -r /tmp/dbt_test_not_null_contratos_modalidade || true \
&& cp -r /opt/airflow/dags-config/repo/plugins/dbt /tmp/dbt_test_not_null_contratos_modalidade \
&& cd /tmp/dbt_test_not_null_contratos_modalidade \
&& dbt deps && dbt test --select not_null_contratos_modalidade \
&& rm -r /tmp/dbt_test_not_null_contratos_modalidade',
        env={
            'DBT_POSTGRES_HOST': Variable.get("ipea_dw_pg_host"),
            'DBT_POSTGRES_USER': Variable.get("ipea_dw_pg_user"),
            'DBT_POSTGRES_PASSWORD': Variable.get("ipea_dw_pg_password"),
            'DBT_POSTGRES_ENVIRONMENT': Variable.get("ipea_dw_pg_environment"),
            'DBT_POSTGRES_PORT': Variable.get("ipea_dw_pg_port"),
            'DBT_POSTGRES_DATABASE': Variable.get("ipea_dw_pg_db"),
        },
        append_env=True
    )

    not_null_contratos_data_assinatura_task = BashOperator(
        task_id='test_not_null_contratos_data_assinatura',
        bash_command='rm -r /tmp/dbt_test_not_null_contratos_data_assinatura || true \
&& cp -r /opt/airflow/dags-config/repo/plugins/dbt /tmp/dbt_test_not_null_contratos_data_assinatura \
&& cd /tmp/dbt_test_not_null_contratos_data_assinatura \
&& dbt deps && dbt test --select not_null_contratos_data_assinatura \
&& rm -r /tmp/dbt_test_not_null_contratos_data_assinatura',
        env={
            'DBT_POSTGRES_HOST': Variable.get("ipea_dw_pg_host"),
            'DBT_POSTGRES_USER': Variable.get("ipea_dw_pg_user"),
            'DBT_POSTGRES_PASSWORD': Variable.get("ipea_dw_pg_password"),
            'DBT_POSTGRES_ENVIRONMENT': Variable.get("ipea_dw_pg_environment"),
            'DBT_POSTGRES_PORT': Variable.get("ipea_dw_pg_port"),
            'DBT_POSTGRES_DATABASE': Variable.get("ipea_dw_pg_db"),
        },
        append_env=True
    )

    not_null_contratos_vigencia_inicio_task = BashOperator(
        task_id='test_not_null_contratos_vigencia_inicio',
        bash_command='rm -r /tmp/dbt_test_not_null_contratos_vigencia_inicio || true \
&& cp -r /opt/airflow/dags-config/repo/plugins/dbt /tmp/dbt_test_not_null_contratos_vigencia_inicio \
&& cd /tmp/dbt_test_not_null_contratos_vigencia_inicio \
&& dbt deps && dbt test --select not_null_contratos_vigencia_inicio \
&& rm -r /tmp/dbt_test_not_null_contratos_vigencia_inicio',
        env={
            'DBT_POSTGRES_HOST': Variable.get("ipea_dw_pg_host"),
            'DBT_POSTGRES_USER': Variable.get("ipea_dw_pg_user"),
            'DBT_POSTGRES_PASSWORD': Variable.get("ipea_dw_pg_password"),
            'DBT_POSTGRES_ENVIRONMENT': Variable.get("ipea_dw_pg_environment"),
            'DBT_POSTGRES_PORT': Variable.get("ipea_dw_pg_port"),
            'DBT_POSTGRES_DATABASE': Variable.get("ipea_dw_pg_db"),
        },
        append_env=True
    )

    not_null_contratos_vigencia_fim_task = BashOperator(
        task_id='test_not_null_contratos_vigencia_fim',
        bash_command='rm -r /tmp/dbt_test_not_null_contratos_vigencia_fim || true \
&& cp -r /opt/airflow/dags-config/repo/plugins/dbt /tmp/dbt_test_not_null_contratos_vigencia_fim \
&& cd /tmp/dbt_test_not_null_contratos_vigencia_fim \
&& dbt deps && dbt test --select not_null_contratos_vigencia_fim \
&& rm -r /tmp/dbt_test_not_null_contratos_vigencia_fim',
        env={
            'DBT_POSTGRES_HOST': Variable.get("ipea_dw_pg_host"),
            'DBT_POSTGRES_USER': Variable.get("ipea_dw_pg_user"),
            'DBT_POSTGRES_PASSWORD': Variable.get("ipea_dw_pg_password"),
            'DBT_POSTGRES_ENVIRONMENT': Variable.get("ipea_dw_pg_environment"),
            'DBT_POSTGRES_PORT': Variable.get("ipea_dw_pg_port"),
            'DBT_POSTGRES_DATABASE': Variable.get("ipea_dw_pg_db"),
        },
        append_env=True
    )

    not_null_contratos_valor_inicial_task = BashOperator(
        task_id='test_not_null_contratos_valor_inicial',
        bash_command='rm -r /tmp/dbt_test_not_null_contratos_valor_inicial || true \
&& cp -r /opt/airflow/dags-config/repo/plugins/dbt /tmp/dbt_test_not_null_contratos_valor_inicial \
&& cd /tmp/dbt_test_not_null_contratos_valor_inicial \
&& dbt deps && dbt test --select not_null_contratos_valor_inicial \
&& rm -r /tmp/dbt_test_not_null_contratos_valor_inicial',
        env={
            'DBT_POSTGRES_HOST': Variable.get("ipea_dw_pg_host"),
            'DBT_POSTGRES_USER': Variable.get("ipea_dw_pg_user"),
            'DBT_POSTGRES_PASSWORD': Variable.get("ipea_dw_pg_password"),
            'DBT_POSTGRES_ENVIRONMENT': Variable.get("ipea_dw_pg_environment"),
            'DBT_POSTGRES_PORT': Variable.get("ipea_dw_pg_port"),
            'DBT_POSTGRES_DATABASE': Variable.get("ipea_dw_pg_db"),
        },
        append_env=True
    )

    not_null_contratos_valor_global_task = BashOperator(
        task_id='test_not_null_contratos_valor_global',
        bash_command='rm -r /tmp/dbt_test_not_null_contratos_valor_global || true \
&& cp -r /opt/airflow/dags-config/repo/plugins/dbt /tmp/dbt_test_not_null_contratos_valor_global \
&& cd /tmp/dbt_test_not_null_contratos_valor_global \
&& dbt deps && dbt test --select not_null_contratos_valor_global \
&& rm -r /tmp/dbt_test_not_null_contratos_valor_global',
        env={
            'DBT_POSTGRES_HOST': Variable.get("ipea_dw_pg_host"),
            'DBT_POSTGRES_USER': Variable.get("ipea_dw_pg_user"),
            'DBT_POSTGRES_PASSWORD': Variable.get("ipea_dw_pg_password"),
            'DBT_POSTGRES_ENVIRONMENT': Variable.get("ipea_dw_pg_environment"),
            'DBT_POSTGRES_PORT': Variable.get("ipea_dw_pg_port"),
            'DBT_POSTGRES_DATABASE': Variable.get("ipea_dw_pg_db"),
        },
        append_env=True
    )

    not_null_contratos_num_parcelas_task = BashOperator(
        task_id='test_not_null_contratos_num_parcelas',
        bash_command='rm -r /tmp/dbt_test_not_null_contratos_num_parcelas || true \
&& cp -r /opt/airflow/dags-config/repo/plugins/dbt /tmp/dbt_test_not_null_contratos_num_parcelas \
&& cd /tmp/dbt_test_not_null_contratos_num_parcelas \
&& dbt deps && dbt test --select not_null_contratos_num_parcelas \
&& rm -r /tmp/dbt_test_not_null_contratos_num_parcelas',
        env={
            'DBT_POSTGRES_HOST': Variable.get("ipea_dw_pg_host"),
            'DBT_POSTGRES_USER': Variable.get("ipea_dw_pg_user"),
            'DBT_POSTGRES_PASSWORD': Variable.get("ipea_dw_pg_password"),
            'DBT_POSTGRES_ENVIRONMENT': Variable.get("ipea_dw_pg_environment"),
            'DBT_POSTGRES_PORT': Variable.get("ipea_dw_pg_port"),
            'DBT_POSTGRES_DATABASE': Variable.get("ipea_dw_pg_db"),
        },
        append_env=True
    )

    not_null_contratos_valor_parcela_task = BashOperator(
        task_id='test_not_null_contratos_valor_parcela',
        bash_command='rm -r /tmp/dbt_test_not_null_contratos_valor_parcela || true \
&& cp -r /opt/airflow/dags-config/repo/plugins/dbt /tmp/dbt_test_not_null_contratos_valor_parcela \
&& cd /tmp/dbt_test_not_null_contratos_valor_parcela \
&& dbt deps && dbt test --select not_null_contratos_valor_parcela \
&& rm -r /tmp/dbt_test_not_null_contratos_valor_parcela',
        env={
            'DBT_POSTGRES_HOST': Variable.get("ipea_dw_pg_host"),
            'DBT_POSTGRES_USER': Variable.get("ipea_dw_pg_user"),
            'DBT_POSTGRES_PASSWORD': Variable.get("ipea_dw_pg_password"),
            'DBT_POSTGRES_ENVIRONMENT': Variable.get("ipea_dw_pg_environment"),
            'DBT_POSTGRES_PORT': Variable.get("ipea_dw_pg_port"),
            'DBT_POSTGRES_DATABASE': Variable.get("ipea_dw_pg_db"),
        },
        append_env=True
    )

    not_null_contratos_valor_acumulado_task = BashOperator(
        task_id='test_not_null_contratos_valor_acumulado',
        bash_command='rm -r /tmp/dbt_test_not_null_contratos_valor_acumulado || true \
&& cp -r /opt/airflow/dags-config/repo/plugins/dbt /tmp/dbt_test_not_null_contratos_valor_acumulado \
&& cd /tmp/dbt_test_not_null_contratos_valor_acumulado \
&& dbt deps && dbt test --select not_null_contratos_valor_acumulado \
&& rm -r /tmp/dbt_test_not_null_contratos_valor_acumulado',
        env={
            'DBT_POSTGRES_HOST': Variable.get("ipea_dw_pg_host"),
            'DBT_POSTGRES_USER': Variable.get("ipea_dw_pg_user"),
            'DBT_POSTGRES_PASSWORD': Variable.get("ipea_dw_pg_password"),
            'DBT_POSTGRES_ENVIRONMENT': Variable.get("ipea_dw_pg_environment"),
            'DBT_POSTGRES_PORT': Variable.get("ipea_dw_pg_port"),
            'DBT_POSTGRES_DATABASE': Variable.get("ipea_dw_pg_db"),
        },
        append_env=True
    )

    not_null_contratos_situacao_task = BashOperator(
        task_id='test_not_null_contratos_situacao',
        bash_command='rm -r /tmp/dbt_test_not_null_contratos_situacao || true \
&& cp -r /opt/airflow/dags-config/repo/plugins/dbt /tmp/dbt_test_not_null_contratos_situacao \
&& cd /tmp/dbt_test_not_null_contratos_situacao \
&& dbt deps && dbt test --select not_null_contratos_situacao \
&& rm -r /tmp/dbt_test_not_null_contratos_situacao',
        env={
            'DBT_POSTGRES_HOST': Variable.get("ipea_dw_pg_host"),
            'DBT_POSTGRES_USER': Variable.get("ipea_dw_pg_user"),
            'DBT_POSTGRES_PASSWORD': Variable.get("ipea_dw_pg_password"),
            'DBT_POSTGRES_ENVIRONMENT': Variable.get("ipea_dw_pg_environment"),
            'DBT_POSTGRES_PORT': Variable.get("ipea_dw_pg_port"),
            'DBT_POSTGRES_DATABASE': Variable.get("ipea_dw_pg_db"),
        },
        append_env=True
    )

    contratos_task >> not_null_contratos_id_task >> end_task
    contratos_task >> unique_contratos_id_task >> end_task
    contratos_task >> not_null_contratos_receita_despesa_task >> end_task
    contratos_task >> not_null_contratos_numero_task >> end_task
    contratos_task >> not_null_contratos_contratante_orgao_origem_codigo_task >> end_task
    contratos_task >> not_null_contratos_contratante_orgao_origem_nome_task >> end_task
    contratos_task >> not_null_contratos_contratante_orgao_origem_unidade_gestora_origem_codigo_task >> end_task
    contratos_task >> not_null_contratos_contratante_orgao_origem_unidade_gestora_origem_nome_resumido_task >> end_task
    contratos_task >> not_null_contratos_contratante_orgao_origem_unidade_gestora_origem_nome_task >> end_task
    contratos_task >> not_null_contratos_contratante_orgao_origem_unidade_gestora_origem_sisg_task >> end_task
    contratos_task >> not_null_contratos_contratante_orgao_origem_unidade_gestora_origem_utiliza_siafi_task >> end_task
    contratos_task >> not_null_contratos_contratante_orgao_origem_unidade_gestora_origem_utiliza_antecip_task >> end_task
    contratos_task >> not_null_contratos_codigo_tipo_task >> end_task
    contratos_task >> not_null_contratos_fornecedor_tipo_task >> end_task
    contratos_task >> not_null_contratos_categoria_task >> end_task
    contratos_task >> not_null_contratos_processo_task >> end_task
    contratos_task >> not_null_contratos_objeto_task >> end_task
    contratos_task >> not_null_contratos_codigo_modalidade_task >> end_task
    contratos_task >> not_null_contratos_modalidade_task >> end_task
    contratos_task >> not_null_contratos_data_assinatura_task >> end_task
    contratos_task >> not_null_contratos_vigencia_inicio_task >> end_task
    contratos_task >> not_null_contratos_vigencia_fim_task >> end_task
    contratos_task >> not_null_contratos_valor_inicial_task >> end_task
    contratos_task >> not_null_contratos_valor_global_task >> end_task
    contratos_task >> not_null_contratos_num_parcelas_task >> end_task
    contratos_task >> not_null_contratos_valor_parcela_task >> end_task
    contratos_task >> not_null_contratos_valor_acumulado_task >> end_task
    contratos_task >> not_null_contratos_situacao_task >> end_task
