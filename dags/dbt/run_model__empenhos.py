# fmt: off
# ruff: noqa

from airflow import DAG
from airflow.datasets import Dataset
from airflow.models import Variable
from airflow.operators.bash_operator import BashOperator
from airflow.operators.empty import EmptyOperator
from airflow.utils.dates import days_ago

from datetime import timedelta

default_args = {
    'owner': 'DBT-Genrated',
    'retries': 5,
    'retry_delay': timedelta(minutes=10),
    'pool': 'dbt_pool'
}

with DAG(
    "run_model__empenhos",
    default_args=default_args,
    schedule='@daily',
    start_date=days_ago(1),
    tags=["dbt", "model"],
    max_active_runs=1,
    on_success_callback=None,
) as dag:

    end_task = EmptyOperator(
        task_id="end",
        outlets=[Dataset("empenhos_model")],
    )

    empenhos_task = BashOperator(
        task_id='run_empenhos',
        bash_command='rm -r /tmp/dbt_run_empenhos || true \
&& cp -r /opt/airflow/dags-config/repo/plugins/dbt /tmp/dbt_run_empenhos \
&& cd /tmp/dbt_run_empenhos \
&& dbt deps && dbt run --select empenhos \
&& rm -r /tmp/dbt_run_empenhos',
        env={
            'DBT_POSTGRES_HOST': Variable.get("ipea_dw_pg_host"),
            'DBT_POSTGRES_USER': Variable.get("ipea_dw_pg_user"),
            'DBT_POSTGRES_PASSWORD': Variable.get("ipea_dw_pg_password"),
            'DBT_POSTGRES_ENVIRONMENT': Variable.get("ipea_dw_pg_environment"),
            'DBT_POSTGRES_PORT': Variable.get("ipea_dw_pg_port"),
            'DBT_POSTGRES_DATABASE': Variable.get("ipea_dw_pg_db"),
        },
        append_env=True
    )

    row_count_match_empenhos_raw_empenhos__bronze_empenhos_task = BashOperator(
        task_id='test_row_count_match_empenhos_raw_empenhos__bronze_empenhos',
        bash_command='rm -r /tmp/dbt_test_row_count_match_empenhos_raw_empenhos__bronze_empenhos || true \
&& cp -r /opt/airflow/dags-config/repo/plugins/dbt /tmp/dbt_test_row_count_match_empenhos_raw_empenhos__bronze_empenhos \
&& cd /tmp/dbt_test_row_count_match_empenhos_raw_empenhos__bronze_empenhos \
&& dbt deps && dbt test --select row_count_match_empenhos_raw_empenhos__bronze_empenhos \
&& rm -r /tmp/dbt_test_row_count_match_empenhos_raw_empenhos__bronze_empenhos',
        env={
            'DBT_POSTGRES_HOST': Variable.get("ipea_dw_pg_host"),
            'DBT_POSTGRES_USER': Variable.get("ipea_dw_pg_user"),
            'DBT_POSTGRES_PASSWORD': Variable.get("ipea_dw_pg_password"),
            'DBT_POSTGRES_ENVIRONMENT': Variable.get("ipea_dw_pg_environment"),
            'DBT_POSTGRES_PORT': Variable.get("ipea_dw_pg_port"),
            'DBT_POSTGRES_DATABASE': Variable.get("ipea_dw_pg_db"),
        },
        append_env=True
    )

    verificacao_tipagem_empenhos_id__bronze_empenhos__text_task = BashOperator(
        task_id='test_verificacao_tipagem_empenhos_id__bronze_empenhos__text',
        bash_command='rm -r /tmp/dbt_test_verificacao_tipagem_empenhos_id__bronze_empenhos__text || true \
&& cp -r /opt/airflow/dags-config/repo/plugins/dbt /tmp/dbt_test_verificacao_tipagem_empenhos_id__bronze_empenhos__text \
&& cd /tmp/dbt_test_verificacao_tipagem_empenhos_id__bronze_empenhos__text \
&& dbt deps && dbt test --select verificacao_tipagem_empenhos_id__bronze_empenhos__text \
&& rm -r /tmp/dbt_test_verificacao_tipagem_empenhos_id__bronze_empenhos__text',
        env={
            'DBT_POSTGRES_HOST': Variable.get("ipea_dw_pg_host"),
            'DBT_POSTGRES_USER': Variable.get("ipea_dw_pg_user"),
            'DBT_POSTGRES_PASSWORD': Variable.get("ipea_dw_pg_password"),
            'DBT_POSTGRES_ENVIRONMENT': Variable.get("ipea_dw_pg_environment"),
            'DBT_POSTGRES_PORT': Variable.get("ipea_dw_pg_port"),
            'DBT_POSTGRES_DATABASE': Variable.get("ipea_dw_pg_db"),
        },
        append_env=True
    )

    verificacao_tipagem_empenhos_contrato_id__bronze_empenhos__text_task = BashOperator(
        task_id='test_verificacao_tipagem_empenhos_contrato_id__bronze_empenhos__text',
        bash_command='rm -r /tmp/dbt_test_verificacao_tipagem_empenhos_contrato_id__bronze_empenhos__text || true \
&& cp -r /opt/airflow/dags-config/repo/plugins/dbt /tmp/dbt_test_verificacao_tipagem_empenhos_contrato_id__bronze_empenhos__text \
&& cd /tmp/dbt_test_verificacao_tipagem_empenhos_contrato_id__bronze_empenhos__text \
&& dbt deps && dbt test --select verificacao_tipagem_empenhos_contrato_id__bronze_empenhos__text \
&& rm -r /tmp/dbt_test_verificacao_tipagem_empenhos_contrato_id__bronze_empenhos__text',
        env={
            'DBT_POSTGRES_HOST': Variable.get("ipea_dw_pg_host"),
            'DBT_POSTGRES_USER': Variable.get("ipea_dw_pg_user"),
            'DBT_POSTGRES_PASSWORD': Variable.get("ipea_dw_pg_password"),
            'DBT_POSTGRES_ENVIRONMENT': Variable.get("ipea_dw_pg_environment"),
            'DBT_POSTGRES_PORT': Variable.get("ipea_dw_pg_port"),
            'DBT_POSTGRES_DATABASE': Variable.get("ipea_dw_pg_db"),
        },
        append_env=True
    )

    verificacao_tipagem_empenhos_data_emissao__bronze_empenhos__date_task = BashOperator(
        task_id='test_verificacao_tipagem_empenhos_data_emissao__bronze_empenhos__date',
        bash_command='rm -r /tmp/dbt_test_verificacao_tipagem_empenhos_data_emissao__bronze_empenhos__date || true \
&& cp -r /opt/airflow/dags-config/repo/plugins/dbt /tmp/dbt_test_verificacao_tipagem_empenhos_data_emissao__bronze_empenhos__date \
&& cd /tmp/dbt_test_verificacao_tipagem_empenhos_data_emissao__bronze_empenhos__date \
&& dbt deps && dbt test --select verificacao_tipagem_empenhos_data_emissao__bronze_empenhos__date \
&& rm -r /tmp/dbt_test_verificacao_tipagem_empenhos_data_emissao__bronze_empenhos__date',
        env={
            'DBT_POSTGRES_HOST': Variable.get("ipea_dw_pg_host"),
            'DBT_POSTGRES_USER': Variable.get("ipea_dw_pg_user"),
            'DBT_POSTGRES_PASSWORD': Variable.get("ipea_dw_pg_password"),
            'DBT_POSTGRES_ENVIRONMENT': Variable.get("ipea_dw_pg_environment"),
            'DBT_POSTGRES_PORT': Variable.get("ipea_dw_pg_port"),
            'DBT_POSTGRES_DATABASE': Variable.get("ipea_dw_pg_db"),
        },
        append_env=True
    )

    verificacao_tipagem_empenhos_empenhado__bronze_empenhos__numeric_task = BashOperator(
        task_id='test_verificacao_tipagem_empenhos_empenhado__bronze_empenhos__numeric',
        bash_command='rm -r /tmp/dbt_test_verificacao_tipagem_empenhos_empenhado__bronze_empenhos__numeric || true \
&& cp -r /opt/airflow/dags-config/repo/plugins/dbt /tmp/dbt_test_verificacao_tipagem_empenhos_empenhado__bronze_empenhos__numeric \
&& cd /tmp/dbt_test_verificacao_tipagem_empenhos_empenhado__bronze_empenhos__numeric \
&& dbt deps && dbt test --select verificacao_tipagem_empenhos_empenhado__bronze_empenhos__numeric \
&& rm -r /tmp/dbt_test_verificacao_tipagem_empenhos_empenhado__bronze_empenhos__numeric',
        env={
            'DBT_POSTGRES_HOST': Variable.get("ipea_dw_pg_host"),
            'DBT_POSTGRES_USER': Variable.get("ipea_dw_pg_user"),
            'DBT_POSTGRES_PASSWORD': Variable.get("ipea_dw_pg_password"),
            'DBT_POSTGRES_ENVIRONMENT': Variable.get("ipea_dw_pg_environment"),
            'DBT_POSTGRES_PORT': Variable.get("ipea_dw_pg_port"),
            'DBT_POSTGRES_DATABASE': Variable.get("ipea_dw_pg_db"),
        },
        append_env=True
    )

    verificacao_tipagem_empenhos_aliquidar__bronze_empenhos__numeric_task = BashOperator(
        task_id='test_verificacao_tipagem_empenhos_aliquidar__bronze_empenhos__numeric',
        bash_command='rm -r /tmp/dbt_test_verificacao_tipagem_empenhos_aliquidar__bronze_empenhos__numeric || true \
&& cp -r /opt/airflow/dags-config/repo/plugins/dbt /tmp/dbt_test_verificacao_tipagem_empenhos_aliquidar__bronze_empenhos__numeric \
&& cd /tmp/dbt_test_verificacao_tipagem_empenhos_aliquidar__bronze_empenhos__numeric \
&& dbt deps && dbt test --select verificacao_tipagem_empenhos_aliquidar__bronze_empenhos__numeric \
&& rm -r /tmp/dbt_test_verificacao_tipagem_empenhos_aliquidar__bronze_empenhos__numeric',
        env={
            'DBT_POSTGRES_HOST': Variable.get("ipea_dw_pg_host"),
            'DBT_POSTGRES_USER': Variable.get("ipea_dw_pg_user"),
            'DBT_POSTGRES_PASSWORD': Variable.get("ipea_dw_pg_password"),
            'DBT_POSTGRES_ENVIRONMENT': Variable.get("ipea_dw_pg_environment"),
            'DBT_POSTGRES_PORT': Variable.get("ipea_dw_pg_port"),
            'DBT_POSTGRES_DATABASE': Variable.get("ipea_dw_pg_db"),
        },
        append_env=True
    )

    verificacao_tipagem_empenhos_pago__bronze_empenhos__numeric_task = BashOperator(
        task_id='test_verificacao_tipagem_empenhos_pago__bronze_empenhos__numeric',
        bash_command='rm -r /tmp/dbt_test_verificacao_tipagem_empenhos_pago__bronze_empenhos__numeric || true \
&& cp -r /opt/airflow/dags-config/repo/plugins/dbt /tmp/dbt_test_verificacao_tipagem_empenhos_pago__bronze_empenhos__numeric \
&& cd /tmp/dbt_test_verificacao_tipagem_empenhos_pago__bronze_empenhos__numeric \
&& dbt deps && dbt test --select verificacao_tipagem_empenhos_pago__bronze_empenhos__numeric \
&& rm -r /tmp/dbt_test_verificacao_tipagem_empenhos_pago__bronze_empenhos__numeric',
        env={
            'DBT_POSTGRES_HOST': Variable.get("ipea_dw_pg_host"),
            'DBT_POSTGRES_USER': Variable.get("ipea_dw_pg_user"),
            'DBT_POSTGRES_PASSWORD': Variable.get("ipea_dw_pg_password"),
            'DBT_POSTGRES_ENVIRONMENT': Variable.get("ipea_dw_pg_environment"),
            'DBT_POSTGRES_PORT': Variable.get("ipea_dw_pg_port"),
            'DBT_POSTGRES_DATABASE': Variable.get("ipea_dw_pg_db"),
        },
        append_env=True
    )

    verificacao_tipagem_empenhos_rpinscrito__bronze_empenhos__numeric_task = BashOperator(
        task_id='test_verificacao_tipagem_empenhos_rpinscrito__bronze_empenhos__numeric',
        bash_command='rm -r /tmp/dbt_test_verificacao_tipagem_empenhos_rpinscrito__bronze_empenhos__numeric || true \
&& cp -r /opt/airflow/dags-config/repo/plugins/dbt /tmp/dbt_test_verificacao_tipagem_empenhos_rpinscrito__bronze_empenhos__numeric \
&& cd /tmp/dbt_test_verificacao_tipagem_empenhos_rpinscrito__bronze_empenhos__numeric \
&& dbt deps && dbt test --select verificacao_tipagem_empenhos_rpinscrito__bronze_empenhos__numeric \
&& rm -r /tmp/dbt_test_verificacao_tipagem_empenhos_rpinscrito__bronze_empenhos__numeric',
        env={
            'DBT_POSTGRES_HOST': Variable.get("ipea_dw_pg_host"),
            'DBT_POSTGRES_USER': Variable.get("ipea_dw_pg_user"),
            'DBT_POSTGRES_PASSWORD': Variable.get("ipea_dw_pg_password"),
            'DBT_POSTGRES_ENVIRONMENT': Variable.get("ipea_dw_pg_environment"),
            'DBT_POSTGRES_PORT': Variable.get("ipea_dw_pg_port"),
            'DBT_POSTGRES_DATABASE': Variable.get("ipea_dw_pg_db"),
        },
        append_env=True
    )

    verificacao_tipagem_empenhos_rpaliquidar__bronze_empenhos__numeric_task = BashOperator(
        task_id='test_verificacao_tipagem_empenhos_rpaliquidar__bronze_empenhos__numeric',
        bash_command='rm -r /tmp/dbt_test_verificacao_tipagem_empenhos_rpaliquidar__bronze_empenhos__numeric || true \
&& cp -r /opt/airflow/dags-config/repo/plugins/dbt /tmp/dbt_test_verificacao_tipagem_empenhos_rpaliquidar__bronze_empenhos__numeric \
&& cd /tmp/dbt_test_verificacao_tipagem_empenhos_rpaliquidar__bronze_empenhos__numeric \
&& dbt deps && dbt test --select verificacao_tipagem_empenhos_rpaliquidar__bronze_empenhos__numeric \
&& rm -r /tmp/dbt_test_verificacao_tipagem_empenhos_rpaliquidar__bronze_empenhos__numeric',
        env={
            'DBT_POSTGRES_HOST': Variable.get("ipea_dw_pg_host"),
            'DBT_POSTGRES_USER': Variable.get("ipea_dw_pg_user"),
            'DBT_POSTGRES_PASSWORD': Variable.get("ipea_dw_pg_password"),
            'DBT_POSTGRES_ENVIRONMENT': Variable.get("ipea_dw_pg_environment"),
            'DBT_POSTGRES_PORT': Variable.get("ipea_dw_pg_port"),
            'DBT_POSTGRES_DATABASE': Variable.get("ipea_dw_pg_db"),
        },
        append_env=True
    )

    verificacao_tipagem_empenhos_rppago__bronze_empenhos__numeric_task = BashOperator(
        task_id='test_verificacao_tipagem_empenhos_rppago__bronze_empenhos__numeric',
        bash_command='rm -r /tmp/dbt_test_verificacao_tipagem_empenhos_rppago__bronze_empenhos__numeric || true \
&& cp -r /opt/airflow/dags-config/repo/plugins/dbt /tmp/dbt_test_verificacao_tipagem_empenhos_rppago__bronze_empenhos__numeric \
&& cd /tmp/dbt_test_verificacao_tipagem_empenhos_rppago__bronze_empenhos__numeric \
&& dbt deps && dbt test --select verificacao_tipagem_empenhos_rppago__bronze_empenhos__numeric \
&& rm -r /tmp/dbt_test_verificacao_tipagem_empenhos_rppago__bronze_empenhos__numeric',
        env={
            'DBT_POSTGRES_HOST': Variable.get("ipea_dw_pg_host"),
            'DBT_POSTGRES_USER': Variable.get("ipea_dw_pg_user"),
            'DBT_POSTGRES_PASSWORD': Variable.get("ipea_dw_pg_password"),
            'DBT_POSTGRES_ENVIRONMENT': Variable.get("ipea_dw_pg_environment"),
            'DBT_POSTGRES_PORT': Variable.get("ipea_dw_pg_port"),
            'DBT_POSTGRES_DATABASE': Variable.get("ipea_dw_pg_db"),
        },
        append_env=True
    )

    empenhos_task >> row_count_match_empenhos_raw_empenhos__bronze_empenhos_task >> end_task
    empenhos_task >> verificacao_tipagem_empenhos_id__bronze_empenhos__text_task >> end_task
    empenhos_task >> verificacao_tipagem_empenhos_contrato_id__bronze_empenhos__text_task >> end_task
    empenhos_task >> verificacao_tipagem_empenhos_data_emissao__bronze_empenhos__date_task >> end_task
    empenhos_task >> verificacao_tipagem_empenhos_empenhado__bronze_empenhos__numeric_task >> end_task
    empenhos_task >> verificacao_tipagem_empenhos_aliquidar__bronze_empenhos__numeric_task >> end_task
    empenhos_task >> verificacao_tipagem_empenhos_pago__bronze_empenhos__numeric_task >> end_task
    empenhos_task >> verificacao_tipagem_empenhos_rpinscrito__bronze_empenhos__numeric_task >> end_task
    empenhos_task >> verificacao_tipagem_empenhos_rpaliquidar__bronze_empenhos__numeric_task >> end_task
    empenhos_task >> verificacao_tipagem_empenhos_rppago__bronze_empenhos__numeric_task >> end_task
