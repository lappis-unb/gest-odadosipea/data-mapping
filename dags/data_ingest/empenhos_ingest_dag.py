from airflow.decorators import dag, task
from airflow.providers.postgres.hooks.postgres import PostgresHook
from api_plugins import ContratosAPI
from postgres_plugins import PostgresDB
from datetime import datetime, timedelta


def get_postgres_conn():
    hook = PostgresHook(postgres_conn_id="postgres_default")
    return hook.get_uri()


@dag(
    schedule_interval="@daily",  # Substituí schedule=[contratos_dataset] por @daily
    start_date=datetime(2023, 1, 1),
    catchup=False,
    default_args={
        "owner": "Davi",
        "retries": 1,
        "retry_delay": timedelta(minutes=5),
    },
    tags=["empenhos_api"],
)
def api_empenhos_dag():
    """DAG para buscar e armazenar empenhos de uma API no PostgreSQL."""

    @task
    def fetch_empenhos():
        api = ContratosAPI()

        postgres_conn_str = get_postgres_conn()

        db = PostgresDB(postgres_conn_str)

        contratos_ids = db.get_contratos_ids()

        for contrato_id in contratos_ids:
            try:
                empenhos = api.get_empenhos_by_contrato_id(contrato_id)

                for empenho in empenhos:
                    empenho["contrato_id"] = contrato_id

                db.insert_data(
                    empenhos,
                    "empenhos",
                    conflict_fields=["id", "contrato_id"],
                    primary_key=["id", "contrato_id"],
                )
            except Exception as e:
                print(f"Erro ao buscar empenhos para o contrato {contrato_id}: {e}")

    fetch_empenhos()


dag_instance = api_empenhos_dag()
