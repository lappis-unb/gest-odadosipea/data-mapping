from airflow.decorators import dag, task
from airflow.providers.postgres.hooks.postgres import PostgresHook
from api_plugins import ContratosAPI
from postgres_plugins import PostgresDB
from datetime import datetime, timedelta


def get_postgres_conn():
    hook = PostgresHook(postgres_conn_id="postgres_default")
    return hook.get_uri()


@dag(
    schedule_interval="@daily",
    start_date=datetime(2023, 1, 1),
    catchup=False,
    default_args={
        "owner": "Joyce",
        "retries": 1,
        "retry_delay": timedelta(minutes=5),
    },
    tags=["faturas_api"],
)
def api_faturas_dag():
    """DAG para buscar e armazenar faturas de uma API no PostgreSQL."""

    @task
    def fetch_faturas():
        api = ContratosAPI()

        postgres_conn_str = get_postgres_conn()

        db = PostgresDB(postgres_conn_str)

        contratos_ids = db.get_contratos_ids()

        for contrato_id in contratos_ids:
            try:
                faturas = api.get_faturas_by_contrato_id(contrato_id)

                db.insert_data(
                    faturas, "faturas", conflict_field="id", primary_key="id"
                )
            except Exception as e:
                print(f"Erro ao buscar faturas para o contrato {contrato_id}: {e}")

    fetch_faturas()


dag_instance = api_faturas_dag()
