from airflow.decorators import dag, task
from airflow.providers.postgres.hooks.postgres import PostgresHook
from api_plugins import ContratosAPI
from postgres_plugins import PostgresDB
from datetime import datetime, timedelta


def get_postgres_conn():
    hook = PostgresHook(postgres_conn_id="postgres_default")
    return hook.get_uri()


@dag(
    schedule_interval="@daily",
    start_date=datetime(2023, 1, 1),
    catchup=False,
    default_args={
        "owner": "Joyce",
        "retries": 1,
        "retry_delay": timedelta(minutes=5),
    },
    tags=["contratos_api"],
)
def api_contratos_dag():
    """DAG para buscar e armazenar contratos de uma API no PostgreSQL."""

    @task
    def fetch_and_store_contratos():
        api = ContratosAPI()

        postgres_conn_str = get_postgres_conn()

        db = PostgresDB(postgres_conn_str)

        ug_codes = [113601, 113602]

        for ug_code in ug_codes:
            contratos = api.get_contratos_by_ug(ug_code)
            db.insert_data(
                contratos, "contratos", conflict_fields=["id"], primary_key="id"
            )

    fetch_and_store_contratos()


dag_instance = api_contratos_dag()
