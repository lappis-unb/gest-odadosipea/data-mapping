from airflow.decorators import dag, task
from airflow.providers.postgres.hooks.postgres import PostgresHook
from api_plugins import ContratosAPI
from postgres_plugins import PostgresDB
from datetime import datetime, timedelta


def get_postgres_conn():
    hook = PostgresHook(postgres_conn_id="postgres_default")
    return hook.get_uri()


@dag(
    schedule_interval="@daily",  # Substituí o schedule=[contratos_dataset] para @daily
    start_date=datetime(2023, 1, 1),
    catchup=False,
    default_args={
        "owner": "Joyce",
        "retries": 1,
        "retry_delay": timedelta(minutes=5),
    },
    tags=["cronogramas_api"],
)
def api_cronogramas_dag():
    """DAG para buscar e armazenar cronogramas de uma API no PostgreSQL."""

    @task
    def fetch_cronogramas():
        api = ContratosAPI()

        postgres_conn_str = get_postgres_conn()

        db = PostgresDB(postgres_conn_str)

        contratos_ids = db.get_contratos_ids()

        for contrato_id in contratos_ids:
            try:
                cronograma = api.get_cronograma_by_contrato_id(contrato_id)

                db.insert_data(
                    cronograma, "cronograma", conflict_field="id", primary_key="id"
                )
            except Exception as e:
                print(f"Erro ao buscar cronograma para o contrato {contrato_id}: {e}")

    fetch_cronogramas()


dag_instance = api_cronogramas_dag()
