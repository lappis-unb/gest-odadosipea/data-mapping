from airflow.decorators import dag, task
from airflow.providers.postgres.hooks.postgres import PostgresHook
from api_plugins import EstruturaAPI
from postgres_plugins import PostgresDB
from datetime import datetime, timedelta
from airflow import Dataset

unidade_organizacional_dataset = Dataset("dataset_unidade_organizacional_postgres")


def get_postgres_conn():
    hook = PostgresHook(postgres_conn_id="postgres_default")
    return hook.get_uri()


@dag(
    schedule_interval="@daily",
    start_date=datetime(2023, 1, 1),
    catchup=False,
    default_args={
        "owner": "Joyce",
        "retries": 1,
        "retry_delay": timedelta(minutes=5),
    },
    tags=["estrutura_organizacional"],
)
def api_unidade_organizacional_dag():
    """DAG para buscar e armazenar dados da Estrutura Organizacional
    de uma API no PostgreSQL."""

    @task(outlets=[unidade_organizacional_dataset])
    def fetch_estrutura_organizacional_resumida():
        api = EstruturaAPI()

        postgres_conn_str = get_postgres_conn()

        db = PostgresDB(postgres_conn_str)

        codigoPoder = "1"
        codigoEsfera = "1"
        codigoUnidade = "7"

        try:
            estrutura_resumida = api.get_estrutura_organizacional_resumida(
                codigoPoder=codigoPoder,
                codigoEsfera=codigoEsfera,
                codigoUnidade=codigoUnidade,
            )
            db.insert_data(
                estrutura_resumida.get("unidades", []),
                "unidade_organizacional",
                conflict_field="codigoUnidade",
                primary_key="codigoUnidade",
            )
        except Exception as e:
            print(f"Erro ao buscar estrutura organizacional {codigoUnidade}: {e}")

    fetch_estrutura_organizacional_resumida()


dag_instance = api_unidade_organizacional_dag()
