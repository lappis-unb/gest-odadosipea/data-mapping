# Politíca de Segurança

## Relatando Vulnerabilidades

Se você descobrir uma vulnerabilidade de segurança, reporte-a imediatamente aos líderes do projeto.

### Informações Necessárias no Relatório:
- Descrição clara do problema e no impacto.
- Passos para reproduzir a vulnerabilidade.
- Informações sobre o ambiente em que a vulnerabilidade foi encontrada.

## Áreas críticas
- Integralidade e confidenciabilidade dos dados.
- Autenticações e controles de acesso.

Agradecemos por contribuir para a segurança e confiabilidade do nosso projeto!